# blog

Mon blog perso

Rédigé avec HUGO

Un site statique

# Pour travailler dessus

Cloner le dépôt

Faire ``hugo new post/datenomdupost.md``

Rédiger le ``.md``

Lancer ``hugo server -D`` en local pour voir ce que cela donne.


Puis

```
git add --all
git commit -m "message"
git push
```

Le dépôt utilise GIT LFS (Large File Storage), qui permet de ne pas trop se soucier de la redondance des fichiers binaires, en particuleir les images et/ou les PDF.

## Noter

### Les thèmes

Il faut noter que les thèmes ne sont pas placés en tant que submodule. En cas de mise à jour de Hugo il faut être prudent et au besoin re-télécharger et re-placer le thème.

### Git LFS

Il est préconisé d'utiliser GIT LFS (Large File Storage) afin de ne pas encombrer le Gitlab avec des fichiers binaires qui, se multipliant à chaque commit, saturent le serveur.
Git LFS permet de tracer les fichiers comme les ``.png``, ``.jpg``, ``.pdf``, etc. Il faut penser à l'installer sur la machine sur laquelle on travaille.
Une fois que les déclaration de tracking ont été faites, plus besoin d'y revenir : on continue à travailler normalement avec Git.


## Création d'un shortcode pour placer une table des matières où je veux

(Concerne ici le thème *Beautiful Hugo*, mais l'idée reste la même pour n'importe quel thème. Si le dossier ``shortcodes`` n'existe pas, il suffit de le créer).

Créer dans ``/layouts/shortcodes`` un fichier HTML qui contiendra le code. On on peut appeler ce fichier ``toctoc.html``, il contient :

```
<div class="toctoc"> 
<p class="titredelatoc">Table des matières</p>

{{ .Page.TableOfContents }}

</div>
```

Puis, dans le fichier ``\static\css\main.css`` (à la fin), intégrer le code CSS correspondant :

```
/* --- toctoc table des matieres shortcode --- */

.toctoc {
  width: 100%
  margin-left: 5%;
  margin-top: 15px;
  padding: 10px;
  word-wrap: break-word;
  font-size: 80%;
border-left : 3px solid LightGrey;

}

.toctoc ul {
list-style-type: none;
}



.titredelatoc{
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 800;
  font-size: 120%;
}
```

Enfin, dans les billets, on peut écrire le shortcode (qui est en réalité le nom du fichier HTML, sans l'extension) :

```
{{< toctoc >}}
```



