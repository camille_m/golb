---
title: "Club Vosgien et VTT : l'appropriation des communs"
date: 2018-02-05
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
description: "Les relations entre vététistes et randonneurs du Club Vosgien ne sont pas toujours faciles. Mais il y a des raisons profondes qui tiennent surtout à une conception étrange du bien commun."
tags: ["Sport", "VTT", "Communs", "Libertés"]
categories:
- Sport
- Libres propos
---


Chacun ses combats. Pour moi, c'est le logiciel libre et la culture libre, pour d'autres c'est la lutte contre les inégalités, la défense de la nature, la sauvegarde du patrimoine&hellip; Et pourtant nous agissons tous avec des méthodes communes&nbsp;: le partage et la coopération. Hélas ce n'est pas toujours le cas. Certains œuvrent pour une cause juste mais veulent s'approprier le résultat, au prétexte qu'ils seraient les dépositaires (même s'ils ne sont pas exclusifs) des moyens et, donc, de la fin. C'est le cas du Club Vosgien dont je voudrais ici toucher quelques mots à propos de sa capacité à partager et à collaborer.


## La montagne est au Club

Le Club Vosgien, c'est d'abord une association alsacienne séculaire. Fondée en 1872, le *Vogesenclub* doit d'abord être salué avec le plus grand respect pour le travail de promotion des activités de pleine nature et le balisage des sentiers sans lequel nombre de promeneurs ne pourraient bénéficier des superbes parcours sur l'ensemble du massif, et au-delà.

Ceci étant dit, j'affirme que le Club Vosgien à une tendance particulièrement détestable à s'approprier le bien commun que représente le paysage montagneux et forestier.

S'approprier&nbsp;? non, bien entendu, il ne s'agit pas de mettre des barrières et d'interdire quiconque de les franchir s'il n'est pas encarté au Club... Mais justement, le Club Vosgien, comprenez-vous, ce n'est pas l'Association des amoureux des sentiers des Vosges, ce n'est pas l'Association des randonneurs vosgiens, non. C'est un Club, à l'image du Club Alpin d'où il tire son nom d'ailleurs. Et un club, c'est plus qu'une association. Ce n'est pas uniquement le rassemblement de personnes ayant des intérêts communs. Un club est une société, c'est-à-dire un regroupement de personnes sociologiquement semblables&nbsp;: des activités communes, des pratiques sociales communes, etc. Dès lors, il n'y a qu'un pas facilement franchi entre la communauté de pratiques et le sentiment d'appartenance du territoire que l'on investit.

Voici un exemple. Au début du XX<sup>e</sup> siècle, le Club Vosgien s'est vu confier, par le Service des Eaux et Forêts le monopole du balisage des sentiers. L'explication est logique&nbsp;: fin XIX<sup>e</sup> siècle se développe dans les Vosges l'activité touristique. En 1875, par exemple à Gérardmer naît le premier office de tourisme, nommé *Comité des promenades* qui aménage des sentiers de randonnées dans la Vallées des Lacs et valorise son territoire. Sur le même modèle, les sociétés touristiques investissent le massif&nbsp;: il fallait harmoniser tout cela et c'est le Club Vosgien qui se vit confier ce rôle. Depuis ce jour, le Club Vosgien mène de front le balisage et l'entretien des sentiers.

Ainsi, fièrement annoncé sur la page d'accueil du [site internet du Club Vosgien](http://www.club-vosgien.eu)&nbsp;:

> Les signes de balisage du Club Vosgien sont déposés à l’INPI et soumis à autorisation pour leur utilisation sur tous supports de communication papier ou web.

En clair, les signes de balisage du Club Vosgien sont déposés en tant que logo de marque (ce n'est pas un brevet au sens industriel du terme), et il est donc impossible d'en avoir l'usage sans une autorisation expresse du Club Vosgien. C'est le premier paradoxe&nbsp;: un balisage harmonisé est un gage de qualité des parcours du Massif, et si je souhaite cartographier un parcours, par exemple pour le VTT ou le trail, et partager ma carte, je n'ai le droit de le faire qu'avec l'autorisation du Club Vosgien. Un comble, quand on sait à quel point ce besoin est présent.

L'implication&nbsp;? le Club Vosgien empêche ainsi le partage des parcours, ce qui aurait pourtant comme effet de valoriser encore davantage cet important travail de balisage du Club Vosgien. Si j'appartiens à une association sportive et que je souhaite créer un parcours en utilisant le balisage du Club Vosgien et partager ce parcours en imprimant plusieurs cartes ou sous format numérique, *je n'ai pas le droit de le faire*.

L'interprétation&nbsp;? Randonner avec le Club Vosgien, c'est utiliser son balisage et les cartes qui vont avec&nbsp;: la bonne vieille carte IGN&nbsp;TOP&nbsp;25 en papier ou son équivalent extrêmement onéreux sur GPS. Il ne s'agirait pas que n'importe qui vienne improviser un parcours VTT ou trail pour déranger les «&nbsp;vrais&nbsp;» randonneurs à crampons et sac à dos, n'est-ce pas&nbsp;? Parce que si tout le monde pouvait si facilement se repérer dans la forêt, et pouvait partager en ligne des cartes en utilisant, au hasard les couches Outdoors d'[Open Street Map](http://umap.openstreetmap.fr/fr/) et les indicateurs du Club Vosgien… hé bien qu'est-ce que cela changerait, au fond&nbsp;? Rien. Si ce n'est que l'usage en serait d'autant plus valorisé. Mais voilà&nbsp;: le Club Vosgien ne veut pas. Parce que ce sont *ses* sentiers, c'est *son* balisage, le fruit de *son* travail, qui lui incombe *à lui* uniquement.

C'est l'ambiance. Et cela va même jusqu'à servir d'arguments contre l'usage des VTT, comme on va le voir plus loin.

## Le cas du VTT

Les cyclistes, c'est le combat du moment pour le Club Vosgien&hellip; enfin pour être plus exact, quelques sections du Club Vosgien, minoritaires, mais dont le vacarme implique forcément l'image du Club Vosgien en général. En effet, en plus du balisage, l'entretien de ses parcours est capital pour que le balisage soit lui-même fiable. Et c'est là que le Club Vosgien a tendance à étendre sa préséance, à ceci près que cette fois on touche au territoire.

Dans les forêts vosgiennes, on trouve de tout&nbsp;:


- des quads avec des gros lards dessus,
- des motocyclistes, sportifs, mais à qui il faudrait expliquer deux trois choses,
- des files de 4x4 avec des chasseurs souvent seuls dedans (il faudrait leur expliquer ce que c'est que le co-voiturage),
- des 4x4 isolés, dont les trajectoires restent mystérieuses,
- des travailleurs forestiers parfois (parfois&nbsp;!) peu respectueux de la forêt (mais comme il manque de personnels aux Eaux et Forêts, il est difficile de faire respecter l'ordre et la loi),
- et des cyclistes.


La ligne de front actuelle qui défraye la chronique depuis au moins le printemps 2017, se situe dans le secteur de Masevaux (Haut-Rhin), mais ce n'est pas exclusif. Devinez quelle catégorie dans la liste ci-dessus fait l'objet de l'ire des membres du Club Vogien&nbsp;? je vous le donne en mille&nbsp;: les vététistes.

Pour certains membres du Club Vosgien, visiblement, les vététistes représentent l'ennemi à abattre, voire même la principale calamité du paysage Vosgien. On pourrait croire que des combats autrement plus glorieux pourraient voir le jour, comme par exemple la biodiversité végétale, la gestion de la faune, la pollution. Non. Parce que vous comprenez, ça c'est des trucs d'écolos. Ces combats sont déjà investis et puis les écolos c'est à gauche&hellip;

Ce qui  chiffonne ces gens, c'est que les sentiers avec des VTT dessus, c'est plus des sentiers de randonnées. Parce qu'un sentier, c'est forcément «&nbsp;de randonnée à pieds&nbsp;», hein&nbsp;? pas fait pour rouler, jouer au ballon, ou même courir (quel traileur ne s'est pas ramassé un jour une remarque désobligeante par un randonneur peureux sur un chemin un peu étroit&nbsp;?).

Interdire le VTT sur les sentiers du massif&nbsp;? c'est un projet que certains membres du club Vosgien semblent caresser avec envie. Des municipalités, sans doute sous influence, avaient déjà tenté le coup, comme à [Ottrott en 2015](http://mbf-france.fr/dossiersentinelle/ottrott-67/) avant une contestation en bonne et dûe forme par la Mountain Bike Foundation qui rappelle d'ailleurs [quelques fondamentaux juridiques](http://mbf-france.fr/actionnationaleinter/quel-statut-vtt-en-foret/) à propos du code forestier (un autre guide juridique est trouvable sur le site [alsace-velo.fr](http://www.alsace-velo.fr/index.php/item/2075-un-point-juridique-sur-la-circulation-des-vtt)).

Bref, voilà que le Club Vosgien de Masevaux a relancé une polémique en mai 2017, bénéficiant d'une [tribune dans les Dernières Nouvelles d'Alsace](http://www.dna.fr/edition-de-mulhouse-et-thann/2017/05/16/le-club-vosgien-est-fache-avec-le-vtt)&nbsp;: «&nbsp;La montagne n’est pas un stade&nbsp;». L'accroche&nbsp;? elle se résume en trois points&nbsp;:


1. La dégradation des sentiers serait l'apanage exclusif des VTT... et de se lancer dans une problématique digne de l'oeuf ou de la poule à savoir si ce sont les freinages des vététistes qui aggravent l'érosion naturelle ou si c'est l'érosion naturelle qui cause l'instabilité du chemin et donc nécessite des freinages. Car dans le premier cas, il faudrait peut-être cesser de croire que les vététistes s'amusent à détériorer les chemins pour le plaisir et dans l'autre cas, les problèmes d'érosion ont bien souvent leur remède dans la gestion du couvert végétal. Est-ce pour autant que *tous* les sentiers devraient être interdits aux VTT&nbsp;? Comment s'effectue donc cette généralisation entre quelques chemins érodés et tous les sentiers&nbsp;? Elle ne peut s'expliquer autrement que par le souhait de ne plus voir de VTT sur les chemins parce qu'ils «&nbsp;dérangent&nbsp;» le Club Vosgien, parce qu'il s'agit d'un public différent.
2. «&nbsp;le code forestier article n° 163-6, interdit la pratique du VTT sur des itinéraires de moins de deux mètres&nbsp;», c'est faux, comme le montre le code forestier lui-même (cf. les liens cités plus haut).
3. On notera de même que le chantage est à l'appui&nbsp;: «&nbsp;Soit les communes nous suivent par des arrêtés de réglementation de circulation qui interdit la pratique du VTT sur les sentiers, soit on arrête de faire l’entretien basique (piochage, élagage, ratissage) et en cinq ans les sentiers seront morts&nbsp;». Une citation bien paradoxale de la part du Club Vosgien, parce que la mort des sentiers signerait aussi la mort du Club&hellip;


Enfin, comme le démontrent de nombreuses photographies prises par les vététistes et les randonneurs, on ne compte plus sur le massif vosgien le nombre de sentiers littéralement saccagés par les travaux forestiers. À juste titre ou non, pour les besoins de l'exploitation forestière, le passage d'engins favorise inévitablement une forme d'érosion bien plus grave que le creusement d'une rigole de 20 mètres ici où là&nbsp;: c'est carrément de la gestion forestière raisonnée qu'il s'agit, et cela dépasse de loin les petites polémiques.

Et pourtant, le Club Vosgien a déjà dénoncé les travaux forestiers peu scrupuleux, notamment dans les pages des Dernières Nouvelles d'Alsace en [mars 2017](http://www.dna.fr/edition-de-mulhouse-et-thann/2017/03/28/une-activite-incessante). L'enjeu est de taille et le combat mérite en effet d'être mené car il concerne de multiples acteurs… hélas, il est bien plus facile d'incriminer les VTT. Si bien qu'une seconde fois en ce début d'année 2018 le journal Les Dernières Nouvelles d'Alsace titrait «&nbsp;[Les sentiers de la discorde](http://www.dna.fr/edition-de-mulhouse-et-thann/2018/02/03/les-sentiers-de-la-discorde)&nbsp;». L'article est à l'avenant, et, citant les interlocuteurs du Club Vosgien&nbsp;:

> Nous ne sommes pas des conservateurs à tendance réactionnaire, l’avenir passe par une bonne cohabitation avec les pratiquants tout en intégrant le respect de la réglementation. Le VTT étant un véhicule sur le plan juridique, le vttiste doit pratiquer en dehors de nos sentiers étroits, inférieurs à un mètre.


Comprendre&nbsp;: «&nbsp;je ne suis pas réac… mais&nbsp;», ou bien ce qu'on appelle en réthorique une [prétérition](https://fr.wikipedia.org/wiki/Pr%C3%A9t%C3%A9rition), qui permet de passer sous silence plusieurs éléments manquant à l'argumentation&nbsp;: ici, par exemple, la rigueur de l'argumentation juridique. Ce qui ne démonte pas la Mountain Bike Foundation, répondant du tac au tac&nbsp;:


> «&nbsp;Le code de la route ne s’applique pas à 900 m d’altitude&nbsp;», affirme Jonathan Choulet, référent MBF Florival, par ailleurs fonctionnaire de police. «&nbsp;Le droit d’itinérance sur les sentiers relève du droit constitutionnel d’aller et venir. Quatre codes régissent la circulation de tous en milieu naturel, mais aucun, excepté le code de la route, ne définit le VTT comme un véhicule. (...)&nbsp;»


## Solution 1 : le dialogue

Dialoguer avec le Club Vosgien n'est pas toujours une sinécure. Pourtant tout semblait se présenter sous les meilleurs auspices en été 2017, lors d'une [rencontre entre la MBF et le président de la Fédération du Club Vosgien](http://mbf-france.fr/actionnationaleinter/rencontre-du-club-vosgien/), qui n'avait jamais auparavant répondu aux lettres de la MBF et semblait découvrir ses activités.

Peu de temps après, suite à l'article du Club Vosgien section Masevaux, une seconde rencontre eu lieu, dans une intention de dialogue et de coopération. D'après le [compte rendu figurant sur le site de la MBF](http://mbf-france.fr/actionnationaleinter/reunion-antenne-mbf-club-vosgien-masevaux/), il y a vraisemblablement une claire différence de conception du terme «&nbsp;dialogue&nbsp;» entre les antennes locales (plus ou moins indépendantes, mais est-ce vraiment souhaitable ?) et la fédération du Club Vosgien. Voici un extrait significatif de cette rencontre entre les référents MBF du secteur Thur-Doller et la présidence du Club Vosgien de Masevaux&nbsp;:

> Mais le responsable du C.V ne l’entend pas ainsi et quitte la salle en nous rappelant qu’au titre de la propriété intellectuelle, nous ne devrions même pas regarder «&nbsp;leur&nbsp;» balisage. Deux autres membres du C.V de Masevaux quittent également la salle sans daigner nous saluer.


Pourtant les bonnes volontés ne manquent pas, de part et d'autre. La MBF agit déjà dans le domaine de l'entretien de sentiers, de manière systématique, en Alsace et dans d'autres régions de France, et organise même des [défis internationaux](http://mbf-france.fr/actionnationaleinter/tcoyt2018/) sur ce thème. Pour preuve [cette session de coopération](http://mbf-france.fr/brigadesvertes/entretien-sentier-masevaux/) entre les vététistes et le Club Vosgien de la même section de Masevaux, une collaboration qui pourtant n'a pas satisfait tout le monde, [selon Jean Koehl](http://www.dna.fr/edition-de-mulhouse-et-thann/2018/02/03/les-sentiers-de-la-discorde), vice-président du CV Masevaux&nbsp;:


> On a accueilli des groupes MBF pour deux demi-journées de travail et une journée pleine. Ils en ont profité pour faire leur com’en faisant venir France 3…


C'est vraiment à se demander ce que reprochent réellement les membres du Club Vosgien aux vététistes. Il semblerait que,  quelles que soit les offres de coopération, les éternels insatisfaits sont toujours les mêmes. Pourtant le dialogue est bel et bien la clé d'une bonne entente entre randonneurs et vététistes&nbsp;: la coopération dans l'entretien des sentiers et les code de conduite des vététistes (comme par exemple la limitation des parcours sauvages d'enduro).

Le dialogue c'est l'apprentissage de la diversité. Diversité des usages et diversité sociale. Les vététistes sont particulièrement au clair sur ce point en distinguant les pratiques entre le cross country (qui n'a pratiquement aucun impact sur l'érosion des sentiers et représente plus de 80% des pratiques), l'enduro et la descente (DH). Peut-être que le Club Vosgien pourrait faire aussi un effort de son côté en se questionnant sur son rapport au paysage commun et les notions de partage que cela implique.

## Solution 2 : attractivité

Cette attitude de la part du Club Vosgien pourrait notamment jouer en défaveur de l'attractivité du Massif Vosgien. Comme exprimé dans les Dernières Nouvelles d'Alsace, les revendications du Club Vosgien se résument à demander la création de Bike Park pour y cantonner les VTT et de ce fait leur interdire les sentiers…  Une revendication qui s'établirait en défaveur évidente de l'écrasante majorité des pratiquants sur le segment du cross country. Autant dire que si les communautés de communes contactées à ce sujet par le Club Vosgien valident ces intentions, c'est toute l'attractivité du VTT sur le Massif qui en serait affectée&nbsp;: adieu les descentes techniques et les petits frissons qui pourtant sont souvent accessibles aux débutants. On ira expliquer aussi aux touristes que pour monter à plus de 900 mètres d'altitude ils devront emprunter les autoroutes à grumiers...

En la matière, on ne peut que saluer [la lettre du maire de Hohrod](http://mbf-france.fr/wp-content/uploads/2017/12/MBF-Article-Vosges_Hohrod.pdf) (68142) qui rappelle que l'équipement de randonnée est une liberté de choix&nbsp;:


> Randonner est une liberté individuelle fondée sur le principe de la liberté de circuler, un droit fondamental et universel. Que l’on circule à pieds ou à vélo est sans importance, chacun est libre de choisir l’équipement nécessaire à sa randonnée (...)


et que par conséquent&nbsp;:

> La réponse éthique est de ne privilégier aucune pratique et de les placer à égalité dans leur accueil et leur traitement, les principes d’équité et de non-discrimination alimentent avantageusement l’argumentation en faveur des activités de plein air.


La valorisation du massif passe donc non seulement par le partage mais aussi par le dialogue entre les pratiquants. Ce besoin de coopération n'est pas seulement vital pour le Massif Vosgien, il l'est aussi pour que le paysage et ses sentiers ne comptent pas parmi les trop nombreux secteurs où le partage et le respect n'ont plus cours.

Alors oui, le Club Vosgien doit partager la montagne, le balisage et les sentiers. Les vététistes de leur côté n'ont pas à rougir de l'aide qu'ils offrent (même en communiquant dessus), comme en témoigne l'opération de grande envergure [Take Care Your Trails](http://mbf-france.fr/actionnationaleinter/tcoyt2018/). Cette communauté est active et respectueuse de l'environnement, qui irait le lui reprocher&nbsp;?

Heureusement, les interlocuteurs bienveillants de la MBF sont nombreux, tels les Parcs, les Offices de Tourisme, les élus&hellip; et bien souvent très attentifs aux contraintes et aux paradoxes dont la communauté des vététistes n'est pas exempte. Ainsi, par exemple, l'image du VTT pâti bien souvent des quelques incivilités rencontrées ici et là, ou des vidéos trash sur Youtube dont ne manquent pas de s'emparer les détracteurs du VTT pour généraliser et justifier leur hargne. Mais qu'importe. L'essentiel est d'atteindre la maturité nécessaire au dialogue et dépasser ces contradictions. En la matière, certaines sections du Club Vosgien devraient prendre exemple, justement, sur la MBF.
