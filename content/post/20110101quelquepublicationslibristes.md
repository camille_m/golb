---
title: "Quelques publications libristes"
date: 2011-01-01
author: "Christophe Masutti"
image: "/images/postimages/ideas.png"
description: "Voici une sélection de publications en lien avec le logiciel libre, la culture libre, les libertés numériques, le capitalisme de surveillance..."
tags: ["Libres propos", "Publication", "Auteur"]
categories:
- Libres propos
---

— À part ce qui suit, il y a aussi [une thèse](https://halshs.archives-ouvertes.fr/hal-00735543/) (et [un livre](http://www.lulu.com/shop/christophe-masutti/les-faiseurs-de-pluie-dust-bowl-%C3%A9cologie-et-gouvernement-%C3%A9tats-unis-1930-1940/paperback/product-20402531.html)), et plein d'autres [vieux trucs](https://statium.link/nuage/s/AG3BwL8d7nrxFMc) académiques.

## Des livres



|       Publication                                    |    Couverture                               |
|----------------------------------------------------|-----------------------------------------------|
| Chris­tophe Masut­ti, *Affaires Privées. Aux sources du capi­ta­lisme de sur­veillance*, Caen, C&F Edi­tions, mars 2020. [Lien](https://cfeditions.com/masutti/). | ![](/images/aff_priv_150x150b.jpg) |
| Richard M. Stall­man, Sam Williams, Chris­tophe Masut­ti, *Richard Stall­man et la révo­lu­tion du logi­ciel libre. Une bio­gra­phie auto­ri­sée*, Paris, Eyrolles, 2011. Liens : [Eyrolles (2<sup>e</sup>éd.)](https://www.eyrolles.com/Informatique/Livre/richard-stallman-et-la-revolution-du-logiciel-libre-9782212136357), [Fra­ma­book](https://framabook.org/richard-stallman-et-la-revolution-du-logiciel-libre-2/). | ![](/images/Stallman_Revolution.jpg) |
| Camille Paloque-Berges et Chris­tophe Masut­ti (dir.), *His­toires et cultures du Libre. Des logi­ciels par­ta­gés aux licences échan­gées*, Lyon/Framasoft, Fra­ma­book, 2013. [Lien](https://framabook.org/histoiresetculturesdulibre/). |  ![](/images/premiere_couvHCLmoyen150x150.png) |
| Chris­tophe Masut­ti, *Liber­tés numé­riques. Guide de bonnes pra­tiques à l’usage des DuMo*, LMyon/Framasoft, Fra­ma­book, 2017. [Lien](https://framabook.org/libertes-numeriques/). | ![](/images/minicartouchewebDumo.png) |
| Chris­tophe Masut­ti (trad. et adap­ta­tion de Bruce Byfield), *LibreOf­fice Wri­ter, c’est sty­lé !*, Lyon/Framasoft, Fra­ma­book, 2018. [Lien](https://framabook.org/libreoffice-cest-style/). | ![](/images/LOSpetitcartouche.png) |




## Des articles


Anouch Seyd­ta­ghia (inter­view par), « Les résis­tants du logi­ciel libre », *Le Temps*, 11/10/2018. [Lien](https://www.letemps.ch/economie/resistants-logiciel-libre).

Chris­tophe Masut­ti, « Le capi­ta­lisme de sur­veillance », *Vers l’Éducation Nou­velle*, num. 571, 2018.

Chris­tophe Masut­ti, « Vie pri­vée, infor­ma­tique et mar­ke­ting dans le monde d’avant Google », docu­ment de tra­vail, HAL-SHS, 2018. [Lien](https://halshs.archives-ouvertes.fr/halshs-01761828).

La série *Anciens et Nou­veaux Lévia­thans* parue sur le Fra­ma­blog. [Lien](https://framablog.org/tag/leviathanscm/)&nbsp;:

-   Les Nou­veaux Lévia­thans IV « La sur­veillance qui vient ».
-   Les Nou­veaux Lévia­thans III. « Du capi­ta­lisme de sur­veillance à la fin de la démo­cra­tie ».
-   Les Nou­veaux Lévia­thans II « Sur­veillance et confiance ».
-   Les Nou­veaux Lévia­thans I « His­toire d’une conver­sion capi­ta­liste ».
-   Les Anciens Lévia­thans I « Le contrat social fait 128 bits… ou plus ».
-   Les Anciens Lévia­thans II « Inter­net. Pour un contre-ordre social ».

Christophe Masutti, « Du *software* au *soft power* » , dans Tristan Nitot et Nina Cercy (éds.), *Numérique, reprendre le contrôle*, Framabook, 2016, pp. 99-107. [Lien](https://framabook.org/numerique-reprendre-le-controle/).

Chris­tophe Masut­ti, « Ingé­nieurs, hackers : nais­sance d’une culture », in : Camille Paloque-Berges et Chris­tophe Masut­ti (dir.), *His­toires et Cultures du Libre. Des logi­ciels par­ta­gés aux licences échan­gées*, Lyon/Framasoft, Fra­ma­book, 2013, pp. 31 – 65.

Chris­tophe Masut­ti, « Inter­net, pour un contre-ordre social », *Linux Pra­tique*, num. 85 Septembre/Octobre 2014. [Lien](https://framablog.org/2014/09/05/internet-pour-un-contre-ordre-social-christophe-masutti/).

C. Masut­ti, Ben­ja­min Jean, *Pré­face*, in : J. Smiers et M. van Schi­jn­del, *Un monde sans copy­right… et sans mono­pole*, Paris, Fra­ma­book, 2011. [Lien](https://framabook.org/un-monde-sans-copyright-et-sans-monopole-2/).

Chris­tophe Masut­ti, « Pour libé­rer les sciences », *Fra­ma­blog*, 15 décembre 2010. [Lien](https://framablog.org/2010/12/20/pour-liberer-les-sciences-christophe-masutti/).
