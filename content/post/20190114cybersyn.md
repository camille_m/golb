---
title: "Cybersyn / techno-socialisme"
date: 2019-01-14
author: "Christophe Masutti"
image: "/images/postimages/bazarelectro.jpg"
description: "Entre 1970 et 1973, sous le gouvernement de Salvador Allende au Chili est né le projet Cybersyn. Ce fut une idée d'organisation cybernétique de gouvernement, une sorte de procédé semi-automatique d'organisation économique et politique... un outil surprenant et esthétiquement peu éloigné des épisodes de Star Trek"
tags: ["Histoire", "Libres propos", "Informatique", "Politique"]
categories:
- Libres propos
---

Voici un extrait (découpé) tiré de mon projet d'ouvrage à paraître chez C&F Édition. Il porte sur le projet Cybersyn dans le Chili des années 1970-1973. Peut-on hacker le socialisme ? y'en a qui ont essayé...






## Mise en garde


Entre 1970 et 1973, sous le gouvernement de Salvador Allende au Chili est né le projet Cybersyn. Ce fut une idée d'organisation cybernétique de gouvernement, une sorte de procédé semi-automatique d'organisation économique et politique... un outil surprenant et esthétiquement peu éloigné des épisodes de Star Trek. On l'a aussi accusé d'être une tentative de contrôle des individus, mais sa courte vie ne permet pas vraiment de l'affirmer.


Avant d'expliquer plus exactement ce qu'était ce projet (et nous irons progressivement), il faut prendre quelques précautions.


Cybersyn est un projet connu par toute personne qui s'intéresse à l'histoire de la cybernétique. En effet, il fut conçu par l'un des plus importants personnages de cette discipline, Stafford Beer. Dans son livre *Cybernetic Revolutionaries*[^1], Eden Medina retrace l'aventure de manière détaillée, documentée, et sur la base des témoignages des participants. L'historien des sciences Andrew Pickering consacre lui aussi une grande partie de son ouvrage *The Cybernetic Brain* à Stafford Beer. 

[^1]: Eden Medina, *Cybernetic Revolutionaries. Technology and Politics in Allende’s Chile*, Boston, MIT Press, 2011.

[^2]: Andrew Pickering, *The Cybernetic Brain. Sketches of Another Future*, Chicago, University of Chicago Press, 2010.

Une autre personne s'y est récemment intéressé. Connu pour ses fines analyses du discours technophile dominant autour des *big data*, Evgeny Morozov publia un article sur Cybersyn dans *The New Yorker* en 2014. Il l'intitula : « The Planning Machine. Project Cybersyn and the origins of the Big Data nation »[^3]. Le parti pris de Morozov est de montrer combien le projet Cybersyn inaugurait l'ère de la surveillance via les données et les capteurs qui jalonnent nos quotidiens dans le monde capitaliste d'aujourd'hui. 

[^3]: Le même article traduit en français dans *Vanity Fair* en janvier 2015, fut intitulé « Big Brother. Cybersyn, une machine à gouverner le Chili ». Evgeny Morozov, « The Planning Machine. Project Cybersyn and the origins of the Big Data nation », *The New Yorker*, 13 octobre 2014. [En ligne](https://www.newyorker.com/magazine/2014/10/13/planning-machine). Evgeny Morozov, « Big Brother. Cybersyn, une machine à gouverner le Chili », *Vanity Fair France*, 19, janvier 2015.

Cet article d'E. Morozov ne m'a pas paru très convainquant. C'est ce qui m'a incité à m'intéresser de plus près à cet épisode étonnant du projet Cybersyn qui, à bien des aspects, appartient aussi à l'histoire de l'informatique.


E. Morozov est allé un peu trop vite en besogne. Certes, Cybersyn peut tenir lieu d'illustration d'une politique victime du solutionnisme technologique (pour reprendre l'un des sujets chers à E. Morozov[^4]). Mais d'un point de vue méthodologique, à part l'idée (revue et rabâchée) d'un « œil de Moscou » à la sauce orwellienne, il est non seulement anachronique mais aussi exagéré d'établir une corrélation ou même un simple parallèle entre un modèle de gouvernement cybernétique des années 1970 et l'état de l'économie de la surveillance d'aujourd'hui. La question des monopoles du web, de la vie privée, de la surveillance de masse par des États, et les *big data*, tout cela n'entre pas cette histoire.

[^4]: Evgeny Morozov, *Pour tout résoudre, cliquez ici*, Paris, Éditions FYP, 2014.


Cybersyn est le fruit d'une période aujourd'hui bien révolue, celle où l'accumulation et le traitement des données par les institutions était vue comme la garantie d'une bonne gouvernance. On y croyait sérieusement. Et pas qu'au Chili, d'ailleurs : partout ! C'est la grande époque des grandes bases de données, pas seulement celle qui faisaient scandale, non… toutes celles qui, en réalité, donnaient corps au grand soir technocratique. Ces ambitions de contrôle ont rapidement fini par s'étioler au profit d'une science des organisations justifiant parfois d'elle-même son existence.



## L'histoire


À son arrivée au pouvoir en novembre 1970, Salvador Allende doit faire face à un Chili qui subissait une inflation catastrophique. Il prend des mesures économiques qui le confrontent à la bourgeoisie et fragilise sa position face au Congrès. Il mène en particulier un grand programme de nationalisation de l'industrie et de planification économique. Ce faisant, la difficulté est double : trouver un moyen pour diriger ce panel industriel (or, très vite un manque de personnel qualifié se fait sentir), et garantir face à la récession l'approvisionnement de matières premières et de pièces détachées.


Il vint alors à l'idée d'un des hauts fonctionnaires de l'agence de la production et du développement (CORFO[^5]), Fernando Flores, de contacter un éminent spécialiste en recherche opérationnelle, le britannique Stafford Beer. La demande était relativement simple dans la formulation du besoin : pouvait-on appliquer les principes du management scientifique à une échelle nationale de manière à rationaliser la décision et contourner les difficultés locales ? En d'autres termes, et pour être plus clair : si des pièces détachées manquent ici, si une baisse de production est constatée là, plutôt que d'attendre que les décideurs locaux puissent s'entendre, ne serait-il pas préférable de centraliser les informations en temps réel et passer des ordres de manière à optimiser la réactivité de la production ?

[^5]: Production Development Corporation (CORFO) (en espagnol: *Cor*poración de *Fo*mento de la Producción de Chile).


Commander et communiquer sont, depuis Norbert Wiener, l'alpha et l'oméga de la cybernétique. Entre les deux, les dispositifs techniques permettent l'automatisation, la transmission, l'apprentissage. Dès la rencontre entre Stafford Beer et Fernando Flores, il était évident qu'il était possible de construire un modèle de gouvernance cybernétique, appuyé par un système informatique adéquat, capable d'assurer le management de la production industrielle nationale. Le plan qu'ils élaborèrent ne devait cependant pas se contenter d'être une réponse à un besoin d'organisation de la productivité. Il devait inclure l'idéal socialiste de l'économie, c'est-à-dire briser-là les formes classiques de la planification.


Par « forme classique de la planification », on peut comprendre la manière dont circule habituellement l'information et l'ordre dans les institutions. Les unités de production remontent des informations par le biais de rapports et de remplissage d'indicateurs, ce qui crée une masse de données que les décideurs doivent ingérer et comprendre. À partir de ces informations, ils proposent alors des orientations économiques ou des activations de leviers (par exemple une réduction d'impôts dans un secteur pour permettre l'investissement) elles-mêmes soumises à des enjeux de pouvoir et du lobbying, en particulier si les décisions sont prises de manière incohérentes. Ce manque de cohérence était en partie résolu avec la nouvelle orientation du CORFO voulue par le gouvernement Allende. Il restait néanmoins à coordonner efficacement la production et surtout intégrer les opérateurs dans la gestion, à toutes les échelles décisionnaires, dans l'optique d'une réappropriation populaire des outils de production.

{{< figure src="/images/beer-cybersyn-plan-action-1024x658.png" title="Le plan d'action. S. Beer, Brain of the Firm, 2nd édition, 1981, p. 255" >}}


Stafford Beer, dans son livre *Brain of the Firm*, qu'il écrit simultanément au projet et publie en 1972, retrace son travail à partir duquel il donne une forme concrète à sa théorie des systèmes viables (Viable System Model, VSM). Les systèmes viables sont des systèmes adaptatifs et autonomes, capables de maintenir leur structure organisationnelle malgré un environnement changeant. L'inspiration est éminemment biologique[^6]. Pour Beer, chaque composante du gouvernement Chilien est un des 5 sous-systèmes qui composent un système viable. La présidence est au système 5, censée maintenir, par la décision politique, l'équilibre entre les stratégies rationnelles du système 4 (que sont les organes institutionnels) et l'impact de la décision au niveau concret, devant le peuple. Au système 1 à 3, on trouve dans l’ordre : 1) les activités primaires, 2) les canaux d’information et 3) les règles, droits et responsabilités (le contrôle) qui jouent l’interface entre système 4 et système 1. Tous les travailleurs sont au système 1 et contribuent à la viabilité de tout le système, de 1 à 4. L'apport essentiel du projet dans la gestion de la production de tout le pays réside à la fois dans un système de communication efficace censé recenser tous les indicateurs de l'état de production et du travail (état des stocks, état des  flux, et même l'absentéisme des travailleurs, etc.), dans la mise en place d'algorithmes censés traiter l'ensemble des données et fournir des indicateurs simplifiés pour faciliter la prise de décision. Enfin, il était envisagé par S. Beer une forme de récursivité travailleurs/citoyens-décideurs par le biais de référendum permanent (le projet Cyberfolk).

[^6]: Une section de l'ouvrage d'Andrew Pickering est consacrée au VSM. Andrew Pickering, *The Cybernetic Brain. Sketches of Another Future*, Chicago, University of Chicago Press, 2010, p. 243 *sq.*


Le système de Beer repose sur un emboîtement des VSM (Viable System Model) et s'applique à tout le Chili. Chaque entreprise nationale est un VSM placé sous l'autorité du CORFO et en même temps des VSM à plus petite échelle représentent les secteurs de l'économie (comme l'acier, le textile, l'alimentaire, les ressources minières…), d'autres VSM sont les sous-secteurs économiques jusqu'au niveau le plus bas : l'usine et l'équipe de travail. Pour chaque VSM, le travailleur participe à la gestion par la consultation que lui permet le système 4.


Pour obtenir un tel système il fallait d'abord en faire la démonstration. C'est tout l'objet du projet Cybersyn, abréviation de « cybernetic synergy », dont l'objectif clairement formulé par Beer était le suivant : 


> Installer un système préliminaire d'information et de réglementation pour l'économie industrielle qui démontrera les principales fonctionnalités de la gestion cybernétique et commencera à faciliter la prise de décision d'ici le 1er mars 1972.
> 
> -- <cite>Stafford Beer, *Brain of the Firm*, New York, John Wiley &amp; Sons, 1981 (première édition 1972), page 251-252.</cite>


Pour ce faire, il fallait disposer rapidement d'un réseau, c'est-à-dire une infrastructure de communication sur laquelle reposerait tout le système, en communication directe avec deux ordinateurs centraux à Santiago. Un sous-projet nommé Cybernet fut mis en œuvre sans tarder. L'ensemble du système démontrait aussi qu'on pouvait réaliser un tel projet sans pour autant disposer des machines dernier cri, ce qui fut d'autant plus important que le coût total de l'opération fut assez modique si on le compare, par exemple, avec d'autres projets américains ou européens. Les deux ordinateurs centraux étaient déjà anciens (en service depuis plus de cinq ans), il s'agissait d'un IBM System 360/50 (créé en 1964) et d'un Burroughs 3500 (créé en 1966). Quant au réseau lui-même, il s'agissait de donner une seconde fonction au réseau existant, c'est à dire un réseau Telex, certes ancien mais efficace, puis implémenter des téléscripteurs dans les entreprises faisant partie du plan de nationalisation.


Avec Cybernet, un autre sous-projet nommé Cyberstride avait pour objectif de créer un logiciel qui a) rassemble et organise tous les indicateurs de production, b) détecte et signale les variations, applique les seuils d'alarme, c) rend possible les prévisions de production à partir des mesures précédentes. En somme c'est un système dynamique dont les variations devaient être rendues lisibles, ce qui fut envisagé avec le compilateur DYNAMO. Ce dernier, inventé par Jay W. Forrester au MIT, est un programme informatique qui produit sous forme de tableaux ou de graphiques les résultats des simulations qui jouent sur les variables d'un système dynamique (c'est un compilateur car il transforme les données du programme en un « langage » lisible par l'humain, des tableaux et des graphiques).


Enfin, pour compléter l'ensemble du dispositif, un simulateur permanent devait pouvoir être utilisé de manière à identifier les variables d'ajustement de la production et l'impact des changements à toutes les échelles du système. Ce fut le sous-projet Checo, un simulateur de l'économie chilienne.

{{< figure src="/images/control-room-large-22-1024x709.jpg" title="Cybersyn Operations Room Datafeed with Chairs, 1972-73. Gui Bonsiepe." >}}



La partie la plus impressionnante de Cybersyn était au système 5 une salle (*op-room*) qui permettait la rencontre des décideurs, équipée d'écrans affichant les systèmes viables avec plusieurs niveaux de récursivité (changez un paramètre analysez le retour du changement d'état du système), des indicateurs exprimant en termes quantitatifs différentes données (comme par exemple des taux d'approvisionnement), et une sortie DYNAMO. Mais pour l'essentiel des apports concrets à l'alimentation du modèle, tout se jouait aux systèmes 1 à 3, où parfois étaient même acheminés à dos de mulet les informations de production au télex le plus proche. 



## Un modèle de gouvernance ?


Modulo certains de ces aspects, disons artisanaux, le projet Cybersyn était cependant conçu de manière à éviter la verticalité de la décision à partir d'analyses hors-sol. Comme le mentionne  E. Medina, à propos du rapport de S. Beer : 


> Son rapport critiquait les méthodes de planification conventionnelles du Chili, qui utilisaient des instantanés de l'économie à des moments discrets, inondaient les gestionnaires du gouvernement avec une mer de données impliquant une gestion du haut vers le bas. Au lieu de cela, il a proposé l'idée d'un processus itératif où les politiques descendent du gouvernement jusqu'aux usines et les besoins des usines montent. Il a positionné la gestion au milieu du système, où il a implémenté un homéostat[^7] qui couple les besoins des niveaux inférieurs avec les ressources allouées d'en haut. Les fonctionnaires pouvaient donc modifier et adapter les politiques gouvernementales pour répondre aux besoins des usines, à condition que ces changements n'aient pas d'effets négatifs importants sur d'autres secteurs de l'économie. Beer a écrit : « Ce système détruit les dogmes de la centralisation et de la décentralisation. Cette approche est organique ». L'approche itérative était également continue et adaptative, conformément à la vision de Beer en matière de contrôle cybernétique. De plus, elle utilisait la cybernétique comme référentiel pour la façon dont le gouvernement pourrait mettre en œuvre le socialisme démocratique proposé par Allende ; elle a donné à l'État le contrôle de la production tout en permettant une large participation.
> 
> -- <cite>Eden Medina, *Cybernetic Revolutionaries. Technology and Politics in Allende’s Chile*,   Cambridge, Massachusetts, MIT Press, 2011, section « Technology for an   Adaptive Economy ». Voir aussi Eden Medina, « Desiging freedom,   regulating a nation: socialist cybernetics in Allende’s Chile ». In: *Journal of Latin American Studies*, 38, 2006, p. 571-606.

[^7]: L'homéostasie est la tendance d'un système à maintenir ses facteurs de modification autour de valeurs bénéfiques par un processus de régulation. Un homéostat est un dispositif permettant de mesurer ces valeurs et ce fonctionnement à l'aide d'indicateurs.


Ce que S. Beer cherchait aussi à démontrer, c'est tout l'intérêt de l'application des concepts de la recherche opérationnelle. Dans le livre qui l'a fait connaître auprès de ses *aficionados* chiliens, *Decision and Control* publié en 1966, il montre qu'un système opérationnel n'a pas à se surcharger d'informations. C'est pourquoi il faut bien distinguer un système en recherche opérationnelle et un centre de données : pour le premier les informations doivent être utilisées et disponibles de manière à produire une décision, dans le second c'est l'information qui est transformée en données pour produire d'autres données que l'on peut interroger, ce qui suppose d'avoir des modèles ou, par apprentissage, créer des modèles à partir de variables. S. Beer affirmait en effet :


> Nos collecteurs de données modernes savent tout ce qu'il faut savoir. C'est empilé dans des caves sur des cartes perforées ; cela sort des ordinateurs sur bande magnétique ; c'est joliment tabulé sur du papier blanc à un rythme de 600 lignes par minute ; cela apparaît sur les bureaux des gestionnaires en de tels volumes qu'ils sont trop occupés pour les lire ; c'est publié par des ministères dans de gigantesques annuaires. Car c'est l'ère du « traitement automatique des données ». Pourtant, tout cela ne nous dit rien sur les raisons pour lesquelles les choses sont telles qu'elles sont. Il faut de la recherche opérationnelle pour le découvrir.
> 
> -- <cite>Stafford Beer, *Decision and Control : The Meaning of Operational Research and Management Cybernetics*, New York, John Wiley &amp; Sons, 1994 (première édition 1966), p. 70.</cite>


Cet allègement du système par rapport à la massification des données, est d'abord pour S. Beer une manière de démontrer aussi que le système a pour objectif de favoriser l'autonomie des sujets. Cybersyn est d'abord un immense réseau de capteurs des signaux de production dont la sensibilité est censée pallier la surcharge cognitive des employés pour leur permettre de se concentrer en premier lieu sur leur métiers et non plus sur les charges administratives, autorisations diverses ou prises en compte des externalités qui perturbent leur production : le système les capte et envoie des instructions. 


C'était aussi la conception d'un autre responsable du projet, Hermann Schwember. Juste avant le coup d'État de Pinochet, ce dernier rendait compte en 1973 dans la revue *Esprit*[^8], des solutions envisagées au Chili pour concilier l'économie et la convivialité que définit Yvan Illich comme cette interrelation créative et autonome des individus entre eux et avec leur environnement. Alors que la production industrielle est d'essence destructive, cette convivialité peut être atteinte à la fois par l'intensification des communications entre les hommes (aussi à la source d'une prise de conscience planétaire) et la diminution de l'appropriation impérialiste. Pour H. Schwember, une société conviviale est « nécessairement socialiste » et seule capable de mener à un modèle post-industriel, c'est-à-dire dépasser les contraintes du salariat industriel, l'appropriation capitaliste, et surtout les dictatures bureaucratiques où n'existent pas « les mécanismes de correction, de participation et d'expression autonome provenant de la base populaire ». 

[^8]: Hermann Schwember (trad. Alain Labrousse), « Convivialité Et Socialisme », *Esprit*, 426 juillet-août 1973, p. 39-66 (voir p. 45).

Une fois que le projet fut rendu public, la principale critique des médias de l'opposition au Chili fut d'ordre économique. Accusé de laisser pour compte les petites entreprises au profit des grandes structures nationales, le projet était catalogué parmi les plus technocratiques. Certains journaux au Chili comme à l'étranger, ne tardèrent pas à faire le parallèle entre un pays « gouverné par un ordinateur » et le monde de Georges Orwell. Il reste que le projet Cybersyn fut catalogué comme un projet de surveillance, entendu comme un contrôle des individus, ce qu'il n'était pourtant pas.


Il était conçu pour implémenter un système idéologique dans un système technique de prise de décision. Ce techno-socialisme convenait parfaitement à Salavador Allende et sa vision d'un Chili émancipé. Mais au-delà de cette vision, la perception technocratique du modèle de Stafford Beer provenait en réalité d'une mauvaise presse de la cybernétique, qui assimilait les modèles cybernétiques appliqués aux organisations humaines à des mécanismes de contrôle qui transforment les hommes en automates. Cette vision était d'autant plus acceptée que les exemples connus de centres opérationnels se trouvent généralement en temps de guerre dans l'armée et son organisation hiérarchisée, du centre de décision vers les cellules opérationnelles. Or, dans le projet Cybersyn était parfaitement intégrée la capacité des groupes d'individus à corriger le modèle de manière créative à chaque instant. En d'autres termes, les décisions dans ce modèle étaient systématiquement soumises à la possibilité d'exercice d'un contre-pouvoir. Techniquement, tout le génie de S. Beer résidait dans son approche itérative du système.



## Une leçon difficile


Évidemment, comme le mentionna plus tard Hermann Schwember[^9], tout n'allait pas pour le mieux. Par définition, le niveau de perfectionnement du système avait comme limite le plus haut niveau de perfectionnement possible de l'outil de production. Une usine qui ne pouvait pas être modernisée à cause du manque d'investissement dont souffrait cruellement le Chili, ne pouvait pas s'ajuster aux objectifs identifiés par le système. 

[^9]: Hermann Schwember (1977) « Cybernetics in government: experience with new tools for management in Chile 1971-1973 ». In H. Bossel, Ed. Concepts and Tools of Computer Based Policy Analysis, Vol. 1., Birkhäuser - Springer Basel AG, Basel, p. 79-138. (pp. 136).


Moins idéaliste, une autre raison peut aussi expliquer certains biais du projet Cybersyn : un modèle cybernétique est d'abord un modèle informationnel, or si on l'applique au fonctionnement d'usines, quelle que soit la provenance de l'information, cette dernière est toujours indépendante des conditions de production ou des réalités sociales. Sans un contrôle qualité, n'importe quelle information entrée par un agent est supposée être fiable ou du moins sincère. Si un modèle comme Cybersyn n'était pas construit pour avoir, au moins en partie, un rôle de contrôle, le système est à la merci des bonnes (ou mauvaises) volontés. Pour avoir la paix, le manager d'une usine est prêt à mentir sur ses indicateurs de production.


Le coup d'État de Pinochet mit brutalement fin à Cybersyn, si bien que d'autres critiques encore intervinrent à contre-temps. Elle sont résumées par Andrew Pickering[^10] en quatre points.

[^10]: Andrew Pickering, *The Cybernetic Brain. Sketches of Another Future*, Chicago, University of Chicago Press, 2010, pp. 265-268.


Le premier était que le projet, malgré toutes les bonnes intentions, était technocratique pour deux raisons : pour commencer il supposait que tous les Chiliens adhéraient au modèle et ensuite que la conception sur VSM de l'organisation productive chilienne est-elle même un modèle fixe et non dynamique (pas de possibilité que le modèle global se transforme excepté à l'intérieur de ses limites).


Le second point portait sur les signaux (les alarmes ou « signaux algédoniques ») censés porter à l'attention des décideurs des seuils à ne pas franchir : ces signaux pouvaient à tout moment, notamment en cas de changement de régime politique, devenir des éléments de surveillance pouvant se retourner contre le système 1 (les travailleurs).


Le troisième point est que Cybersyn n'a certes pas été conçu pour créer une chaîne de commandement et de contrôle verticale uniquement du haut vers le bas, mais il pouvait facilement le devenir. Et il fut effectivement utilisé dans cette intention lors d'un événement, une grève générale de la confédération des transports en 1972 (opposée à la nationalisation du secteur et soutenue par la droite politique) : le réseau Cybernet a alors été utilisé pour identifier et surveiller les nœuds de grèves, et trouver des solutions de contournement pour maintenir les flux. En ce sens le système a parfaitement joué son rôle, mais il a alors été transformé en un système de surveillance dans une lutte politique (et le conflit fut assez violent).


Un quatrième point porte à s'interroger sur les objectifs de Cybersyn et du VSM en général : se maintenir en vie. On peut effectivement se demander si, dans une certaine mesure, le modèle ne confond pas le moyen et la fin, un peu à l'image de ce qui se passait en France vers la fin des années 1970.


... et pour le savoir, il faudra attendre la publication de mon ouvrage (printemps 2019)… Teasing !

