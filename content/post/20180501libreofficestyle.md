---
title: "LibreOffice Writer, c'est stylé!"
date: 2018-05-01
author: "Christophe Masutti"
image: "/images/postimages/loostyle.png"
description: "Traduction et adaptation d'un manuel LibreOffice"
tags: ["Logiciel libre", "Publication", "Livre", "Manuel", "LibreOffice"]
categories:
- Logiciel libre
---

En ce début de printemps 2018, voici une traduction et adaptation d'un manuel sur LibreOffice Writer et (surtout) les styles ! *LibreOffice Writer, c'est stylé!* est une publication Framabook. Il s'agit d'un projet de longue date, que j'ai le plaisir de voir aboutir, enfin !


> Halte aux manuels bourrés de procédures qui transforment les logiciels en cliquodromes. Pour changer, laissez-vous guider vers la compréhension des règles de mise en page et de la typographie, à travers l'usage des styles dans LibreOffice Writer. L'objectif est de vous aider à vous concentrer sur le contenu de vos documents, gagner en rapidité et en précision, tout en vous formant à l'automatisation de la mise en forme.
> 
> Cet ouvrage s'adresse aux utilisateurs débutants mais ayant déjà fréquenté un logiciel de traitement de texte. Quels choix de polices et d'interlignage devez-vous faire ? Comment créer des styles et les enchaîner correctement ? À quoi servent les styles conditionnels ? Paragraphes, listes, tableaux, titres, et hiérarchie des styles, toutes ces notions n'auront plus de secret pour vous. Voici un aperçu unique de LibreOffice Writer, de ses fonctionnalités importantes et de ses capacités.

Vous pouvez acheter, télécharger, consulter librement cet ouvrage sur [Framabook.org](https://framabook.org/libreoffice-cest-style/).

![Couverture LibreOffice c'est stylé !](/images/LOSPremcouv_500x738.png)
