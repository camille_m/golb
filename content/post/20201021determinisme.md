---
title: "Déterminisme et choix technologiques : tentative de synthèse depuis les débuts de l'informatisation de la société"
date: 2020-10-21
author: "Christophe Masutti"
image: "/images/postimages/inge-vintage.jpg"
description: "L'objectif de cet article est de déployer une lecture générale de la révolution informatique tout en prenant la mesure du discours déterministe qui traverse au quotidien ce processus historique d'informatisation de la société depuis les années 1960 jusqu'à nos jours. Nous emploierons pour cela plusieurs « manières de voir » les technologies (au sens de *computer technologies*), c'est-à-dire que nous analyserons dans leur contexte les discours qui les encadrent,
promoteurs ou détracteurs, ainsi que les critiques philosophiques ou historiques, de manière à en comprendre les variations au fil du temps et comment, *in fine*, des choix sociaux ont fini par s'imposer, tels le logiciel libre, en créant des zones de résistance."
tags: ["Libres propos", "Philosophie", "Histoire", "Technologie"]
categories:
- Libres propos
---


(**Préambule**) Cette tentative de synthèse est avant tout un document martyr. J'essaie
de saisir les discours emprunts de déterminisme technologique au long de
la révolution informatique. Ces discours valident un certain ordre
économique et envisagent la production et l'innovation comme des
conditions qui s'imposent à la société. Les dispositifs technologiques
sont-ils uniquement des dispositifs de pouvoir ? impliquent-ils
 des représentations différentes du rapport entre technique et
société ? Un peu des deux sans doute. Avant d'employer ce mot de
dispositif et de nous en référer à Michel Foucault ou autres penseurs
postmodernes, j'essaye de tracer chronologiquement la synergie entre
l'émergence de l'informatique dans la société et les discours qui
l'accompagnent. Il n'y a donc pas vraiment de thèse dans cet article et
certaines affirmations pourront même paraître contradictoires. Je pense
en revanche exposer quelques points de repères importants qui pourront
faire l'objet ultérieurement d'études plus poussées, en particulier sur
le rôle des principes du pair à pair, celui du logiciel libre et sur
l'avenir d'Internet en tant que dispositif socio-technique. Il me
semblait cependant important de dérouler l'histoire tout autant que
l'historiographie de manière à contextualiser ces discours et tâcher
d'en tirer enseignement à chaque étape.

P.S. : à plusieurs reprises je copie-colle quelques paragraphes de mon
ouvrage publié en mars 2020. Cette redondance est voulue : tâcher
d'aller plus loin.

-- [Lire cet article en version PDF](https://statium.link/nuage/s/nAwWg3iAAHScLcr)


{{< toctoc >}}


## Introduction


Nous avons une tendance à nommer les sociétés et les âges en fonction
des artefacts. De l'âge du fer à la société numérique d'aujourd'hui,
qu'il s'agisse de l'outil, ou du rattachement des usages à un dispositif
technique, tantôt sacré, tantôt profane, nous devons toujours lutter
contre le déterminisme technologique qui restreint le degré de
complexité nécessaire pour appréhender la société, son rapport à
l'espace et au temps. Cette simplification n'en est pas vraiment une. En
effet, prétendre que la technologie est un paramètre indépendant qui
conditionne le développement humain ou social, suppose de savoir ce qui
exactement dans l'ensemble des dispositifs techniques présents joue un
rôle significatif. Ce choix doit être éclairé, débattu jusqu'à parfois
identifier des « technologies clé » ou des combinaisons de technologies
dont les effets rétroactifs conditionnent les choix économiques en
termes d'innovation, de compétitivité, de profit et, au niveau
macro-économique, la géostratégie.

Forme pervertie du déterminisme technologique, le solutionnisme (Morozov
2014) et les idéologies de dérégulation libertarianistes (Husson 2018)
partent de ce principe qu'appréhender tout problème par algorithme et
ingénierie permet de proposer des solutions techniques efficientes,
indépendamment des conditions et des paramètres du problème et des
finalités poursuivies. Si vous êtes fatigué-e d'une journée de travail
au point de vous endormir au volant sur le chemin du retour, vous pouvez
toujours utiliser des lunettes bourrées de capteurs qui vous permettront
de faire sonner une alarme si vous vous endormez au volant... mais vous
ne réglerez pas le problème qui consiste à se demander pourquoi votre
travail est à ce point harassant.

Depuis la sortie de K. Marx (Marx 1896, chap. 2) sur le moulin à bras et
le moulin à vapeur[^1], nombre de chercheurs ont travaillé sur le
rapport déterminant ou non de la technologie sur la société ou, de
manière plus restrictive, la politique, la culture ou l'économie. Dans
la seconde moitié du XXe siècle, le débat fut largement occupé par les
historiens tels Robert L. Heilbroner, partisan d'un déterminisme doux,
tandis que les philosophes tels Martin Heidegger ou Jacques Ellul,
proposaient une lecture ontologique ou matérialiste qui les fit à tort
assimiler à des technophobes là où il ne faisaient que réclamer une
critique radicale de la technique. Enfin vinrent les études sociales sur
les sciences et les techniques (STS) qui montrèrent que les techniques
ne naissent pas de processus autonomes mais sont les fruits
d'interactions sociales (Vinck 2012), même si la version radicale de
certains implique que tout serait le résultat de processus sociaux,
tandis que pour d'autres le réflexe déterministe reste toujours très
présent (Wyatt 2008).

Avant d'être un biais conceptuel pour l'historien, le déterminisme
technologique est d'abord un discours *sur* la technologie. Il projette
des représentations qui impliquent que la société doit évoluer et faire
ses choix en fonction d'un contexte technologique (l'innovation et la
compétitivité économique en dépendent) tout en démontrant par la même
occasion que dans la mesure où le développement technologique est sans
cesse changeant, il projette la société dans l'incertitude. On ne compte
plus les discours politiques contradictoires à ce propos.

Les mouvements ouvriers ont pu chercher à éviter ces paradoxes en
proposant le troisième terme qu'est le capitalisme pour articuler les
rapports entre société et techniques. Ainsi, de la révolution
industrielle, on peut tenir le fordisme comme la forme la plus élaborée
de l'équilibre entre production et consommation. Cela eut un coût, très
vite identifié par Antonio Gramsci : il ne s'agissait pas seulement
d'une nouvelle organisation du travail, mais une soumission au
machinisme, ce qui fini par conditionner une représentation de la
société où le capitalisme « annexe » la technique en y soumettant le
travailleur et par extension, la société. Ce sont aussi les mots de
Raniero Panzieri, l'un des fondateurs le l'ouvriérisme italien
(opéraïsme) des années 1960 : le capitalisme détermine le développement
technologique et pour cela il lui faut un discours qui doit montrer que
c'est à la société de s'adapter au changement technologique et donc
d'accepter la soumission ouvrière à la machine -- là où la manufacture
morcelait le travail encore artisanal, l'usine et ses machines
systématisent l'exploitation de l'ouvrier et le prive du sens de son
travail (Panzieri 1968, 105).

La critique marxiste fut néanmoins largement minorée après les années
1960 au profit d'une mise en accusation de la « technoscience » d'être
le nouvel épouvantail du totalitarisme. Cependant, il faut regarder de
près les multiples variations de discours au sujet des « nouvelles
technologies » dont les ordinateurs et les infrastructures numériques
représentaient l'essentiel sur lequel allaient définitivement s'appuyer
les sciences comme l'industrie et les services. On voit alors comment se
déploie une acculturation commune à l'informatique simultanément à une
popularisation d'un déterminisme technologique qui déjà correspond à une
certaine tradition de la critique -- disons -- non-marxiste.

Pour caricaturer, une version extrême de ce déterminisme pourrait
aujourd'hui figurer dans le discours du Président de la République
Française Emmanuel Macron le 14 septembre 2020 au sujet des nouveaux
standards de téléphonie mobile. « Le tournant de la 5G », dit-il, « est
le tournant de l'innovation. J'entends beaucoup de voix qui s'élèvent
pour nous expliquer qu'il faudrait relever la complexité des problèmes
contemporains en revenant à la lampe à huile : je ne crois pas au modèle
Amish (...) ». Nous pouvons nous interroger sur le fait qu'un discours
politique en 2020 puisse ainsi opposer la présence d'une technologie à
un faux dilemme. Car c'est lorsque nous avons toutes les peines du monde
à penser la société et la technique comme un tout interagissant que nous
réduisons le problème à un choix entre (techno)phobie et adhésion. La
technique s'impose d'elle-même à la société, la société doit s'y
adapter... Il y eu des échappées à ce choix durant la « révolution
informatique » : les espoirs du progrès social, les mythes qui
idéalisaient les innovateurs de l'informatique, le partage communautaire
des usages, les combats pour la vie privée (contre les bases de
données)... Échappèrent-ils pour autant aux faux-dilemmes ?

Dans cet article, ce sont ces contradictions qui nous intéresseront à
travers une lecture de la Révolution Informatique à partir des années 1960. Nous la nommerons aussi « informatisation de la société ». Nous
préférons ce terme d'informatisation parce qu'il contient l'idée qu'un
processus est à l'œuvre qui ne concerne pas seulement les choix sociaux
mais aussi les effets persuasifs des discours sur les artefacts
concernés : les ordinateurs, les programmes, les réseaux et leurs
infrastructures, les acteurs humains de l'informatisation, leurs rôles
et leurs représentations du processus. Nous maintenons, pour commencer,
que l'adhésion du public à une conception déterministe de la technologie
est cruciale dans ce qu'on peut appeler un processus historique
d'assimilation, c'est-à-dire, d'une part une diffusion des discours qui
assimilent progrès informatique et progrès social, et d'autre part une
acculturation auprès des groupes professionnels, ingénieurs, parties
prenante de l'économie numérique naissante. Cependant, des promesses
d'un avenir économique radieux au mythe de l'ingénieur ermite qui
développe un artefact « révolutionnaire » dans son garage, en passant
par le partage de pratiques : de quel déterminisme est-il question ?

Nous pouvons opposer deux logiques. L'une « extérieure » qui consiste à
interpréter de manière déterministe ou non le changement technologique,
apanage des chercheurs, philosophes, historiens et sociologues. L'autre,
« intérieure », plus ou moins influencée par la première, mais qui
développe consciemment ou non un déterminisme discursif qui modèle
l'adhésion à une culture de l'artefact, ici une culture informatique, et
plus généralement l'adhésion à un modèle économique qui serait imposé
par une technologie autonome. Typiquement, ramené à des considérations
très contemporaines : avons-nous réellement le choix de nous soumettre
ou non au capitalisme de surveillance, fruit de la combinaison entre
technologies dominantes et soumission des individus à l'ordre social
imposé par ces technologies et leur modèle économique fait de monopoles
et d'impérialisme (Masutti 2020). Il est alors important de voir jusqu'à
quel point ce discours déterministe est intégré, s'il y a des points de
rupture et de résistance et comment ils se manifestent.

L'objectif de cet article est de déployer une lecture générale de la
révolution informatique tout en prenant la mesure du discours
déterministe qui traverse au quotidien ce processus historique
d'informatisation de la société depuis les années 1960 jusqu'à nos
jours. Nous emploierons pour cela plusieurs « manières de voir » les
techniques (et nous pourrons aussi bien employer le mot « technologies »
pour désigner les *computer technologies*), c'est-à-dire que nous
analyserons dans leur contexte les discours qui les encadrent,
promoteurs ou détracteurs, ainsi que les critiques philosophiques ou
historiques, de manière à en comprendre les variations au fil du temps
et comment, *in fine*, des choix sociaux ont fini par s'imposer, tels le
logiciel libre, en créant des zones de résistance.

## Que faire avec les ordinateurs ?


### Réussir dans les affaires


Si l'histoire de l'informatique traite en premier lieu des « gros
ordinateurs » ou *mainframe*, c'est parce que, au cours des deux
décennies des années 1950 et 1960, il furent porteurs des innovations
les plus décisives et qui dessinent encore aujourd'hui notre monde
numérique : les matériaux et l'architecture des machines, les systèmes
d'exploitation, les réseaux et leurs infrastructures, la programmation
et les bases de données. En même temps que ces innovations voyaient le
jour, l'industrialisation des ordinateurs répondait à la demande
croissante des entreprises.

D'où provenait cette demande ? L'Association internationale pour l'étude
de l'économie de l'assurance (autrement connue sous le nom Association
de Genève) consacra en 1976, première année de publication de sa
collection phare *The Geneva Papers on Risk and Insurance*, un volume
entier produisant une prospective des pertes économiques en Europe liées
à l'utilisation de l'informatique à l'horizon 1988. Sur la base de
l'expérience des vingt années précédentes, elle montrait que la
confiance en l'informatique par les décideurs et chefs d'entreprise
n'était pas communément partagée. Les raisons qui ont permis de lancer
concrètement l'industrie informatique dans la fabrication de masse de
séries complètes d'ordinateurs, fut le passage des machines *mainframe*
aux mini-ordinateurs, c'est-à-dire des ordinateurs plus petits, fournis
avec une gamme d'applicatifs selon les secteurs d'activité des
entreprises et des langages de programmation faciles à l'usage. C'est
l'effort marketing d'entreprises comme IBM qui changea complètement la
représentation des ordinateurs chez les cadres et chefs d'entreprises.

Auparavant, l'ordinateur représentait la puissance de calcul et l'aide à
la décision. C'était ainsi qu'il était vanté dans les films
publicitaires de la Rand pour la gamme Univac. Les domaines les plus
réputés devoir se servir de telles machines étaient les universités, les
banques, les assurances, ou l'aéronautique. Mais comment le chef d'une
petite entreprise pouvait-il être convaincu par l'investissement dans
une telle machine ? Il fallait réinventer l'utilité de l'ordinateur et
c'est tout l'enjeu de la production de ce secteur dans les années
1960 (Ceruzzi 1998, 110). Les efforts en marketing furent largement
accrus pour passer un message essentiel : l'ordinateur ne devait plus
traiter l'information existante parallèlement à la production, mais
produire l'information, être le pilier du système d'information et, en
tant que « système expert », aider à prendre les bonnes décisions. Mieux
encore, l'ordinateur pouvait prédire l'avenir : savoir déterminer à
l'avance les chances de succès d'un nouveau produit en analysant le
marché et l'environnement. C'est tout l'objet des projets de traitements
de données quantitatives et qualitative dont le projet DEMON (Decision
Mapping via Optimum Go-No Networks), débuté en 1967, fut longtemps
l'illustration dans le domaine de l'informatique appliqué au
marketing (Charnes et al. 1968a ; Charnes et al. 1968b).

Après deux décennies de tâtonnements, de bricolages savants et de
recherche, l'ordinateur des années 1960 était passé du rang de produit
d'ingénierie complexe à celui d'instrument de réussite entrepreneuriale.
Cela n'allait pas sans les nombreuses inventions dans le domaine des
périphériques et des applicatifs, mais la dynamique des ventes
d'ordinateurs, la transformation des fabricants en multinationales et le
marché concurrentiel mondial de l'informatique n'auraient jamais pu voir
le jour sans qu'un discours convainquant ne puisse être véhiculé. Ce
n'était pas celui de la persuasion d'un produit performant de qualité,
et il était loin d'être technique (car il ne s'adressait pas aux
techniciens). L'ordinateur était fait pour l'homme d'affaire. Ainsi que
le vantait la publicité pour l'IBM System/3 :

> Toutes les activités économiques ont une chose en commun :
> l'information. \[...\] et si au lieu de travailler avec ces
> informations, vous pouviez travailler pour elles ? \[...\] Voici
> System/3. L'ordinateur pour l'homme d'affaires qui n'avait jamais
> pensé qu'il pouvait s'en payer un.

### Organiser la paix sociale


Aborder les aspects techniques la réorganisation informatique d'une
entreprise était un sujet qui ne pouvait pas passer inaperçu. La même
publicité pour System/3 précisait :

> Un langage de programmation est aussi disponible, basé sur les besoins
> courants des entreprises. Il est simple à apprendre et à utiliser pour
> vos employés...

La formation des employés était un aspect incontournable de
l'investissement informatique d'une entreprise. Plus précisément, face
aux promesses de rentabilité, d'optimisation des processus de production
et de vente, le travail des bases de données ne pouvait pas
s'improviser. Les ordinateurs de nouvelle génération ne se réduisaient
pas à la retranscription et au stockage de listes et de procédures pour
les automatiser. Si l'ordinateur était un instrument de réussite sociale
des décideurs, la formation des employés devait être l'objet d'un
discours plus général sur l'avancement de la société toute entière.

Les langages informatiques de haut niveau tels FORTRAN, LISP, COBOL,
ALGOL sortirent au début des années 1960 du cercle fermé des experts
universitaires et industriels pour intégrer peu à peu des procédures
d'automation dans les entreprises. Pour cela il fallait former de futurs
salariés au travail algorithmique de la donnée, tout en abaissant le
niveau de qualification global comparé aux savoirs universitaires ou
d'ingénierie que nécessitait la maîtrise des ordinateurs *mainframe*.
Dès 1965, c'est l'une des raisons qui poussèrent IBM à produire des
systèmes comme l'IBM 360. Outre ses équipements qui intéressaient les
secteurs de la recherche et de la haute ingénierie, la série d'IBM
permettait d'initier de nouvelles pratiques, comme la possibilité de
passer d'un ordinateur à l'autre avec les mêmes programmes, voire les
mêmes systèmes d'exploitation, ou d'augmenter la vitesse d'exécution des
programmes (grâce aux techniques de *microprogrammation*). L'avantage
consistait aussi en l'utilisation plus harmonisée des langages de
programmation réduisant ainsi les coûts de formation tout en optimisant
la rentabilité de l'investissement que représentait l'achat de ces
machines.

Aux États-Unis, la complexification des systèmes d'information des
organisations et l'apparition des grands pôles industriels destinés à
l'innovation électronique, poussèrent la population des plus diplômés à
se spécialiser dans des secteurs très compétitifs. Les universités ne
produisaient qu'un nombre limité de ces ingénieurs et scientifiques, le
plus souvent issus d'établissements prestigieux et d'une classe sociale
élevée. La communauté des ingénieurs informaticiens su faire face à ce
qui fut appelé en 1968 la « crise des programmeurs » et qui en réalité
avait commencé bien avant. Chaque entreprise disposant d'un ordinateur
devait développer ses propres logiciels, ce qui à chaque fois augmentait
la masse salariale de programmeurs qui développaient souvent des
programmes similaires. C'est ainsi que le besoin s'est fait sentir de
rationaliser et standardiser les programmes dans un secteur économique
nouveau. La création formelle de l'IEEE Computer Society en 1971
validait ce qui déjà était une évidence : s'il fallait une industrie du
logiciel, les ingénieurs devaient se regrouper pour s'entendre sur les
standards et appuyer cette industrie naissante.

Pourtant, ce n'est pas pour combler un manque de main d'œuvre
qu'apparurent les centres de formation (souvent pour adultes) destinés à
l'apprentissage des langages de haut niveau et plus généralement pour
occuper des postes informatiques de premier niveau. Dans la veine des
mouvements pour les droits civils et pour faire face à des risques
d'émeutes dans certains quartiers de centres urbains, les États et les
municipalités construisirent des programmes de formations destinés de
préférence aux populations afro- et latino- américaines (Abbate 2018).
Le discours tenu était emprunt d'une croyance dans le pouvoir des
technologies à provoquer le changement social : s'adapter à
l'informatique et apprendre les langages informatiques était une voie
vers l'émancipation, la paix sociale et la démocratie. De fait, la
réorganisation des entreprises autour de l'informatique créait de
nouvelles hiérarchies entre cols blancs et manipulateurs, au fur et à
mesure que les tâches s'automatisaient et que le travail des
scientifiques-ingénieurs devait se spécialiser toujours plus au risque
de se diluer dans la masse d'exécutants sur machines.

Pourtant, si l'apprentissage des langages informatiques pouvait donner
du travail, il n'exonérait ni de la soumission hiérarchique à l'ordre
blanc en col blanc, ni des inégalités salariales et sociales (Nelsen
2016). Si nous pouvons considérer l'histoire de l'industrie informatique
selon les cycles d'innovations technologiques et envisager leur impact
sur la société par le prisme de l'informatisation de la société, une
lecture tout aussi pertinente peut nous amener à nous interroger la
manière dont la société s'est imprégnée des discours qui accompagnèrent
le marché de l'informatique. Plus ce dernier s'étendait, plus le
discours devait avoir une ambition universelle et inclusive,
indépendamment de la réalité sociale.

### Promouvoir l'accomplissement individuel


En 1977, lorsque le premier ordinateur personnel de la gamme Apple II
sorti sur le marché, l'annonce[^2] parue dans la presse spécialisée en
micro informatique précisait, outre les fonctionnalités déjà *intégrées*
et prêtes à l'usage : « Mais le plus grand avantage -- indépendamment de
la façon dont vous utilisez Apple II -- est que vous et votre famille
vous familiarisez davantage avec l'ordinateur lui-même ».

Aussi bien pour son concepteur Steve Wozniak que pour Steve Jobs, Apple
II est le premier micro-ordinateur destiné à un usage individuel et dont
le pari réussi fut d'être produit à grande échelle (et pas sous la forme
d'un kit électronique réservé aux spécialistes). Utilisé le plus souvent
par la catégorie des cadres d'entreprises, au moins pour les premières
années de sa présence sur le marché, les principaux atouts résidaient
dans la gamme logicielle fournies (en particulier le tableur VisiCalc,
sorti en 1979), le stockage et archivage sur cassette, et les jeux
vidéos. L'apparition des gammes concurrentes, comme celles de Commodore,
et jusqu'à la vente des Apple Macintoch, Apple II resta longtemps le
modèle de référence de l'ordinateur personnel, entré dans les foyers
comme le véhicule d'une acculturation à l'informatique domestique.

Censé propulser les familles dans un mouvement général d'encapacitation
économique et sociale, l'ordinateur personnel entrait en rupture avec la
représentation courante d'une informatique à la fois puissante et
encombrante, réservée à l'entreprise. Dans un monde où l'informatisation
des secteurs tertiaires (banques et assurances) et industriels créait de
grands bouleversements dans l'organisation du travail, l'informatique
personnelle se présentait comme une part de « démocratisation »
technique dans les foyers alors qu'elle était habituellement vécue dans
l'entreprise comme l'application d'une stratégie autoritaire et
hiérarchique.

L'historien Clifford Conner rapporte une communication privée avec
Frederick Rodney Holt, l'un des piliers du développement d'Apple II, qui
affirme (Conner 2011, 458) :

> L'Apple II n'était pas une machine à écrire : c'était un instrument
> permettant de concevoir de beaux algorithmes. Les gens ne l'achetaient
> pas pour exécuter des programmes. Ils l'achetaient pour réaliser leurs
> propres logiciels. Avec les machines d'aujourd'hui, et leurs centaines
> de mégaoctets de code obscur, il n'est plus possible d'apprendre
> comment marche un ordinateur ni même d'apprendre à écrire du code.
> Mais en 1976, des gamins de douze, quatorze ans pouvaient réparer une
> machine qui ne fonctionnait pas.

Ce type de réflexion est très courant. Il s'agit de postuler que la
connaissance du code informatique est un moyen d'appropriation de la
machine, une forme de lutte contre l'aliénation technologique d'où
résulterait un mieux-être social. Tout comme l'apprentissage des
langages informatique devait être une opportunité de plein emploi et de
paix sociale, suffirait-il, pour briser les chaînes de nos esclavage
modernes, d'équiper les individus d'ordinateurs personnels ?

À la fin des années 1970, le mythe de l'ordinateur personnel obéissait
au même schéma discursif entretenu durant la décennie
précédente (Pfaffenberger 1988). La raison pour laquelle il était,
parfois de bonne foi, imaginé comme un vecteur d'égalité sociale, c'est
qu'il était le produit d'une éthique commune, celle des hackers de la
première heure, opérant dans les universités sur des projets
développement scientifiques et industriels, et dont le principal appétit
était le partage collectif des savoirs et des techniques en dehors des
contraintes hiérarchiques et académiques. Mais il était aussi le produit
d'une autre génération de hackers, ceux sortis des institutions au
profit de l'aventure entrepreneuriale (Agre 2002) et qui considéraient
l'innovation du point de vue libéral voire, plus radicalement, d'un
point de vue libertarien (Turner 2012). Si bien que ce discours, adressé
en premier lieu à ceux qui avaient les moyens financiers d'acquérir un
ordinateur personnel, rencontrait chez les cadres supérieurs des
entreprises une approbation générale. En échange du prestige et de la
distraction que promettait l'ordinateur personnel à toute la famille, on
acceptait de faire pénétrer dans le calme foyer les obligations et les
tensions du travail.

## Adhésion


### Le hacker, ingénieur hétérogène


Quels furent les vecteurs de l'adhésion générale à ces discours en
faveur d'une informatique libératrice ? Autant il est toujours possible
de centrer historiquement l'artefact « ordinateur » ou « informatique »
autour de quelques personnages connus, autant l'adhésion culturelle
qu'on pourrait traduire par « consentement au processus
d'informatisation », en dépit des controverses (sur l'organisation du
travail, par exemple), n'a jamais été traduite sur un mode consensuel et
uniforme par un groupe d'acteurs bien identifiés. Au sens de la théorie
de l'acteur-réseau (Callon 2006), l'hétérogénéité des acteurs-humains
(hackers, décideurs politiques, chefs d'entreprises, communautés
d'utilisateurs, etc.) autant que celle des acteurs-objets concernés
(infrastructures de réseaux informatiques, machines informatiques,
logiciels et langages de programmation, etc.) doivent nous amener à
beaucoup d'humilité quant à l'identification des nœuds de traduction et
des acteurs médiateurs (Akrich 2006) (objets techniques ou personnes) de
l'acculturation informatique.

Lorsque nous parlons d'acteur-réseau, remarquons que l'approche des
objets scientifiques et techniques à laquelle nous faisons référence
date du début des années 1980, au moment où les dispositifs
informatiques (réseaux et machines) devaient être considérés comme des
dispositifs inaccomplis (et Internet est encore aujourd'hui un
dispositif inachevé, nous y reviendrons) et n'ont fait que très rarement
l'objet des études de sociologie des sciences et des techniques.
Néanmoins, c'est justement pour cette raison que le concept
d'hétérogénéité est si important, car il était pour ainsi dire incarné
par les premières communautés d'ingénieurs-scientifiques des premiers
âges de l'informatique, tout particulièrement les hackers, non parce
qu'ils étaient porteur d'un discours bien problématisé, mais parce
qu'ils diffusaient des pratiques. C'est pour cette raison que les
hackers ont été dès le début identifiés comme les principaux vecteurs
d'une diffusion culturelle, d'un esprit, de l'informatique dans la
société. C'est cette vision qui a fini par s'imposer comme un paradigme
dans l'histoire de l'informatisation, avec toutes les valeurs positives
qu'il véhiculait, jusqu'à parfois construire des récits, des mythes,
dont le rôle consiste à sacraliser le rôle social des entreprises nées
de cet esprit informaticien, de Apple à Google et des riches heures de
la Silicon Valley.

Pour une approche systématique, nous devons nous limiter volontairement
au concept, défini par John Law, d'*ingénieur hétérogène* (Law 1987),
c'est-à-dire un acteur qui ne crée pas simplement des objets techniques,
mais qui définit un nouveau cadre du rôle social, des représentations et
des valeurs de cette technologie et de ceux qui l'emploient et la
diffusent. Il faut cependant comprendre que cette diffusion n'est pas
obligatoirement intentionnelle et elle est souvent moins le fait de
volontés individuelles que de communautés de praticiens dont les
activités et les techniques interagissent et s'enrichissent mutuellement
sans cesse, des premiers *phraekers* aux bidouilleurs de génie piratant
les réseaux commutés universitaires. L'apparition des pratiques
informatiques en réseau a multiplié la diffusion des pratiques et
démontré le potentiel libérateur des usages « contre-culturels » de
l'informatique.

Dans un article, Janet Abbate propose dans un plaidoyer pour une étude
STS (*Science and Technology Studies*) de l'histoire d'Internet :
« Lorsqu'on observe à la fois les créateurs et les utilisateurs de
matériels et de logiciels, on s'aperçoit que l'infrastructure et la
culture ont amorcé une fusion » (Abbate 2012, 170). En effet, dans
l'histoire de la révolution informatique, cette fusion engage à la fois
l'innovation technologique, l'économie de production de ces
technologies, et la figure de l'ingénieur hétérogène qui incarne à la
fois la culture, l'économie et les pratiques bien qu'il s'auto-définisse
comme révolutionnaire ou contre-culturel[^3]. Le hacker est un
programmeur dont le génie ne réside pas seulement dans la qualité de son
travail mais aussi dans sa manière de traduire un ensemble de valeurs de
la sphère informatique à la sphère sociale : le partage de
l'information, l'esprit d'innovation (d'amélioration), les libertés
d'usages des machines, l'idée qu'un « progrès technique » est aussi un
« progrès social » dans la mesure où tout le monde peut y contribuer et
en être bénéficiaire. Sa contre-culture ne s'exerce donc pas à
l'encontre d'un discours dominant et largement en faveur de l'adoption
de l'informatique dans toutes les organisations sociales, mais dans la
tension entre l'appropriation de ces valeurs par les institutions et la
défense de leur autonomie au niveau des pratiques sociales. C'est ce qui
fait dire à Paul Baran (un des co-inventeurs de la communication réseau
par paquets) dès 1965 que l'ingénieur informaticien a l'opportunité
d'exercer « une nouvelle forme de responsabilité sociale. » (Baran 1965)

Comment exercer cette responsabilité ? Sur ce point, on peut prendre
l'exemple du projet Community Memory qui, entre 1973 et 1975 s'est fait
le support d'une telle ambition sur un mode communautaire (Doub 2016).
Les fondateurs du projet (Judith Milhon, Lee Felsenstein, Efrem Lipkin,
Mark Szpakowski) entendaient créer un système de communication non
hiérarchisé où les membres pouvaient partager de l'information de
manière décentralisée. Techniquement, le projet était le pilote d'un
mini-réseau dans la région de la Baie de San Francisco et le succès
qu'il rencontra enthousiasma ses initiateurs tant par l'engouement du
public à pouvoir disposer d'un réseau d'information communautaire mais
aussi par la créativité spontanée de ce public pour échanger des
informations (Colstad and Lipkin 1976). Michael Rossman, journaliste,
ex--activiste du Free Speech Movement et qui fréquentait alors
assidûment le projet Community Memory, affirma à son propos : « le
projet est indéniablement politique. Sa politique est centrée sur le
pouvoir du peuple - son pouvoir par rapport aux informations qui lui
sont utiles, son pouvoir par rapport à la technologie de l'information
(matériel et logiciel). » (Rossman 1976)

Community Memory a fait des émules au-delà de San Francisco et ses choix
techniques étaient eux aussi des choix politiques : le projet était
hébergé par Resource One, une organisation non gouvernementale créée
lors de la crise de 1970 et l'invasion controversée du Cambodge par
l'armée des États-Unis en avril. Il s'agissait de mettre en place
l'accès à un calculateur pour tous ceux qui se reconnaissaient dans le
mouvement de contre-culture de l'époque. Avec ce calculateur (Resource
One Generalized Information Retrieval System -- ROGIRS), des terminaux
maillés sur le territoire américain et les lignes téléphoniques WATS,
les utilisateurs de Community Memory pouvaient entrer des informations
sous forme de texte et les partager avec tous les membres de la
communauté, programmeurs ou non, à partir de terminaux en accès libre.
Il s'agissait généralement de petites annonces, de mini-tracts, etc.
dont les mots-clés permettaient le classement.

Pour Lee Felsenstein, le principal intérêt du projet était de
démocratiser l'usage de l'ordinateur, en réaction au monde universitaire
ou aux élus des grandes entreprises qui s'en réservaient l'usage. Mais
les caractéristiques du projet allaient beaucoup plus loin : pas de
hiérarchie entre utilisateurs, respect de l'anonymat, aucune autorité ne
pouvait hiérarchiser l'information, un accès égalitaire à l'information.

Community Memory était une émergence technologique de la contre-culture
américaine. En tant qu'objet technique et idéologique, il s'opposait
surtout à tout contrôle de l'État, dans un climat de méfiance entretenu
par les épisodes de la guerre du Vietnam ou le scandale du Watergate.
Pour y accéder, il fallait que l'on accepte de participer à et d'entrer
dans une communauté, seule à même de rendre des comptes, indépendamment
de toute structure institutionnelle. Ce système de pair à pair s'oppose
frontalement à toute intention de contrôle et de surveillance externe.

C'est à travers ce type de projets que les hackers furent généralement
identifiés, du moins dans les représentations populaires. Tous les
hackers n'appartenaient pas à la contre-culture, du moins tous ne se
prêtaient pas, dans les rues, à des manifestations contestataires. Mais
tous croyaient sincèrement en une chose : l'informatique et plus
généralement les ordinateurs, doivent améliorer notre quotidien. Lee
Felsenstein et ses amis en étaient persuadés eux aussi, persuadés que ce
modèle d'échanges d'information et de savoir-faire constituait un projet
social.

### Le côté obscur


Dispositifs techniques et intérêt social interagissent mutuellement.
Cependant, cette affirmation que nous posons aujourd'hui comme une
évidence n'est pas aisée à expliquer dans l'histoire des assimilations
sociales des dispositifs informatiques. En l'espace d'à peine dix ans,
la représentation commune du rapport entre utilité et contraintes de
l'ordinateur a radicalement changé lors de l'apparition de l'ordinateur
personnel sur le marché de la consommation des foyers.

Ce changement s'est déroulé en deux temps : dans un premier temps, une
critique générale des systèmes informatiques des entreprises conçu comme
une « décapacitation » du travailleur, puis, dans un second temps, une
adoption de l'informatique individuelle sur le marché de la
consommation, motivé par le discours d'encapacitation issu à la fois de
la culture hacker et du marketing. Bien qu'une étude historique plus
approfondie soit hautement souhaitable, on peut étudier ces deux étapes
indépendamment des terrains géographiques car, comme nous allons le
voir, un discours commun dans la « culture occidentale » se dégage
autour d'une conception déterministe de la technologie.

Comme nous l'avons vu plus haut, les années 1960 connurent une embauche
massive de personnels visant d'une part à apprendre les langages
informatiques et d'autre part intégrer les systèmes d'information (le
passage de la mécanographie à l'automatisation du travail de la donnée).
Du point de vue managérial, toutefois, le thème dominant allait jusqu'à
fantasmer sur la capacité des systèmes d'information à automatiser
entièrement une entreprise (Haigh 2001), et manière plus prosaïque, on
voyait clairement émerger une nouvelle sorte d'ingénieurs, éloignés de
la programmation et du matériel : les spécialistes des systèmes
d'information.

Face à ces bouleversements radicaux, tout particulièrement dans les
grandes entreprises qui accomplissaient leur conversion informatique, on
pouvait s'attendre à ce que les syndicats manifestent bruyamment leur
méfiance. Jusqu'à la fin des années 1960, ce ne fut pas le cas dans les
pays occidentaux pour plusieurs raisons. Aux États-Unis, l'évolution
technologique était essentiellement comprise comme la clé des gains de
productivité et de la diversification des activités, en particulier la
technologie de pointe, celle justement qui permettait d'avoir des
entreprises multinationales, monopolistiques, leader de l'automatisation
et de l'informatique, surtout pourvoyeuses d'emplois qualifiés. Du côté
Européen, à l'instar de la France, on remédiait assez lentement au
retard technologique, notamment à cause de la conversion difficile de
l'économie agricole et le trop grand nombre de petites entreprises.

À la lecture des archives du Trades Union Congress (TUC) on voit
cependant que les syndicats de Grande Bretagne étaient en revanche très
attentifs à la question mais adoptaient une posture linéaire depuis le
milieu des années 1950 (où le congrès déclare se refuser à toute
interprétation alarmante au sujet de l'automation dans les
entreprises[^4]), jusqu'à la mise en place en 1964 d'un sous-comité
chargé de veiller à la mécanisation des bureaux et au développement de
l'informatique d'entreprise (avec des points de vigilance au sujet des
éventuelles suppressions de postes et l'augmentation du temps de
travail[^5]). Mais si les positions officielles du TUC étaient toujours
très conciliantes avec les politiques d'avancement technologique du
gouvernement, ce n'était pas le cas pour d'autres syndicats membres tels
l'Amalgamed Engineering Union (regroupant des ingénieurs) dont le Comité
National vota une résolution très contraignante dont les deux premiers
points impliquaient de ne pas accepter de licenciement en cas
d'automatisation de tâches, et pour tout changement organisationnel,
obliger une consultation préalable des syndicats[^6].

Du point de vue des travailleurs, en effet, le sujet de l'informatique
d'entreprise était alors un sujet complexe, une affaire d'ingénieurs qui
s'occupaient des grandes machines dont le nombre se chiffrait seulement
en milliers à l'échelle mondiale. Par contraste, la décennie des années
1970, avec la venue sur le marché des mini-ordinateurs, était une
période de digestion des transformations des organisations qui
s'informatisaient à une vitesse exponentielle, à mesure que les coûts
investis dans ce secteur baissaient drastiquement.

Dès lors, loin des préoccupations de rentabilité, les répercutions sur
l'organisation du travail furent ressenties tantôt comme une forme de
taylorisation du travail de bureau, tantôt comme une négation du travail
vivant (au sens marxiste) du travailleur, une remise en cause de son
accomplissement personnel au profit d'une automatisation routinière,
surveillée et guidée par des machines, en somme un travail
« électronicisé » (Zuboff 1988).

Une prophétie se réalisait, celle de Charles Babbage lui-même qui
affirmait en 1832 (Babbage 1963, 191) :

> La division du travail peut être appliquée avec le même succès aussi
> bien aux opérations mentales qu'aux opérations mécaniques, ce qui
> assure dans les deux cas la même économie de temps.

C'est cette taylorisation mentale que les syndicats dénonçaient avec
force. Pour certains, l'informatique était le « Cheval de Troie » de la
taylorisation, en particulier du point de vue de l'ingénieur devenu un
appendice pensant de la machine, à la capacité d'initiative réduite et
par conséquent soumis à l'organisation et au pouvoir des organisateurs.
L'informatique devenait l'instrument d'un pouvoir de classe là où elle
était censée libérer les travailleurs, développer l'autonomie créatrice
et démocratiser les organisations (Cooley 1980).

Ce point fut problématisé entre 1975 et 1977 par l'une des figures les
plus connues de la théorie du contrôle, Howard H. Rosenbrock. Selon lui,
la formulation mathématique du contrôle des processus ne peut être
exhaustive et l'approche algorithmique ne peut être que le résultat d'un
compromis entre des choix existants. Le choix ne se réduit jamais entre
ce qui est entièrement automatisable ou non. Le principal biais du
contrôle moderne est selon lui une distanciation de l'ingénieur par
rapport à son objet : parce que nous voulons toujours une solution
unique, optimale, les procédures tendent à restreindre les objectifs
parce qu'elles n'intègrent pas, ou mal, les éléments qualitatifs et les
jugements de valeurs. Si bien que, à l'ère des ordinateurs, le constat
est partagé : l'ordinateur devient un « manuel de conception
automatisé » (Rosenbrock 1977), ne laissant que des choix mineurs à
l'ingénieur tout en donnant naissance à un brouillage entre la gestion
des process et l'application de la technologie.

Entre la réduction de l'autonomie créatrice et le terrain gagné par la
taylorisation mentale, tout particulièrement dans les secteurs
tertiaires, les syndicats commencèrent à élaborer une réflexion
ambivalente.

D'un côté il existait des projets de transformation organisationnelle
dont l'objectif était très clairement guidé par une conception hacker de
partage de connaissances et de pratiques. On peut citer sur ce point les
projets scandinaves (en Norvège, au Danemark et en Suède) menés à partir
de 1975, à l'initiative conjointes des chercheurs (on retient le rôle
phare de Kristen Nygaard) et des unions syndicales afin d'influencer les
développements locaux des technologies informatiques sur un mode
démocratique, en alliant les contraintes de rentabilité, l'accroissement
des compétences des travailleurs et la qualité de vie au travail. Le
célèbre projet Utopia (Sundblad 2011), entre 1981 et 1986, était la
continuité logique de projets précédents et regroupait l'ensemble des
syndicats scandinaves des travailleurs graphiques (Bødker et al. 2000).
Néanmoins, selon les participants, les expériences montraient que les
technologies de productions en place constituaient des obstacles souvent
infranchissables pour que les demandes des syndicats (être associés aux
changement organisationnels, co-développer des dispositifs de
production, faire valoir leur expertise, accroître leurs compétences,
etc.) puissent réellement aboutir. Ce que montraient ces projets en
réalité, c'était que les nouvelles technologies limitent les
revendications des travailleurs et reflètent davantage les intérêts des
entreprises (Lundin 2011). Les technologies, et plus particulièrement
l'informatique et les systèmes d'information sont chargés de valeurs, et
elles ne sont pas toujours socialement acceptables.

Face à l'avancement des technologies dans l'entreprise, les syndicats
prenaient de plus en plus conscience que si l'informatisation de la
société s'accommodait d'un discours positif sur le progrès social et le
contrôle des technologies, les travailleurs n'avaient pour autre
solution que de se soumettre à un nouvel ordre négocié où les
technologies leur étaient justement incontrôlable. Ce paradoxe était
essentiellement nourrit d'une idée répandue alors : la technique
détermine l'ordre social.

Éclairant l'état d'esprit des chefs d'entreprises à propos de l'adoption
des systèmes de traitement de données informatisés dans les
organisations, on peut noter ces mots de Franco Debenedetti, directeur
général adjoint de Olivetti, lors d'une conférence organisée par le
*Financial Times* en 1979, à propos de l'avenir de l'EDP (Electronic
Data Processing) (Cité par S. Smith 1989) :

> *Il s'agit d'une technologie de coordination et de contrôle d'une
> main-d'œuvre, les cols blancs, que l'organisation (traditionnelle) ne
> couvre pas (...). En ce sens, l'EDP est en fait une technologie
> organisationnelle, et comme l'organisation du travail elle a une
> double fonction de force productive et d'outil de contrôle pour le
> capital.*

Si bien que, d'un autre côté, face à des projets de transformation
technologique où la négociation se concentrait plus sur l'aménagement du
poste informatique que sur le rôle de l'informatique dans la
transformation du travail, des voix réfractaires se faisaient entendre.

Comme le montre Tali Kristal pour ce qui concerne l'industrie américaine
des années 1970 (Kristal 2013), la face « noire » des technologies
informatiques était d'une part le fait de l'inégalité de classe qu'elles
induisaient historiquement dans l'entreprise qui s'informatise tout en
diminuant le pouvoir de l'action syndicale, mais aussi en dissociant le
travail de l'expertise des cadres intermédiaires et des ingénieurs. En
somme, l'informatisation a été aussi considéré comme un mouvement
capitaliste construit sur une concentration des pouvoirs et la
diminution des contre-pouvoirs dans les institutions de l'économie.

Pour prendre l'exemple de la France, le « mai des banques » en 1974[^7],
fut un épisode pour le moins surprenant : dans un secteur où se jouait
déjà depuis des années un processus d'informatisation qui promettait à
la fois qualité de travail et gains de productivité, les travailleurs ne
se plaignaient pas seulement de leurs conditions de travail et
salariales mais en interrogeaient aussi le sens. En effet le secteur
bancaire n'avait pas complètement terminé ce processus d'automatisation
si bien que le passage à l'informatique devait passer par une forme
assumée de taylorisation de travail de bureau et, par extension, une
déqualification d'une partie des employés (Moussy 1988). Préalablement,
une augmentation de la demande de produits bancaires résultant de la
réforme Debré de 1966 (une déréglementation de la banque qui introduisit
une course concurrentielle au nombre guichets) avait suscité une
embauche en nombre d'employés dont la qualification était de niveau
moyen à bas : ces mêmes employés qu'il fallait former pour parachever la
modernisation de la banque tout en maîtrisant la masse et le coût
salarial. À une déqualification générale correspondait alors au début
des années 1970 (et ce, jusqu'au milieu des années 1980) une
employabilité toute aussi concurrentielle de jeunes diplômés
sur-qualifiés pour le travail demandé, alors qu'en même temps la
formation interne peinait à suivre le mouvement. Mutation informatique
et déréglementation furent les principales causes de ce mouvement de
1974, dont le nom est une référence aux grèves de « mai 1968 », débuté
en janvier au Crédit Lyonnais puis à la Banque de France et suivi
presque aussitôt et bruyamment par les employés d'autres établissements
jusqu'en avril (Feintrenie 2003).

On connaît les positions de la CFDT (Confédération française
démocratique du travail) qui s'impliqua très tôt dans ce conflit du
secteur bancaire. Suivant les positions d'autres syndicats en Europe,
elle s'était lancé depuis quelque temps dans une réflexion de fond sur
le rapport entre informatique et travailleurs. Le « mai des banques »
fut un élément déclencheur qui donna lieu avec succès à un colloque en
1976 intitulé « Progrès technique, organisation du travail, conflits ».
Organisé par le secteur « Action Revendicative » de la CFDT et le trio
Jean-Louis Missika, Jean-Philippe Faivret -- alias Philippe Lemoine --
et Dominique Wolton, il donna lieu à une publication au Seuil : *Les
dégâts du progrès. Les travailleurs face au changement technique*. Il
s'agissait de penser ce nouveau rapport à la technologie dans des
secteurs d'activité très différents en prenant en compte l'impact social
et économique de l'informatisation et son articulation avec
l'organisation du travail. L'auteur de la préface, Edmond Maire
(secrétaire général de la CFDT de 1971 à 1988), résume ainsi (Maire
1977) :

> Il suffit d'avoir en mémoire les grandes grèves des banques et des PTT
> en 1974 où les OS du tertiaire sont apparus en pleine lumière, celles
> de Renault au Mans sur le travail à la chaîne, de Péchiney-Noguères
> contre le chantage technologique, d'Usinor-Dunkerque contestant la
> prétendue impossibilité technique d'assurer la sécurité ; et, au-delà,
> la mise en cause croissante du travail posté, de la dangereuse
> accélération du programme d'énergie nucléaire, de l'impérialisme de
> l'informatique et du danger qu'il fait courir aux libertés.

Cet « impérialisme de l'informatique » est vécu dans le monde du travail
des années 1970 de deux manières. Premièrement, il s'agit de mettre en
exergue l'informatique comme outil de contrôle. Mais ce contrôle n'est
pas celui du contremaître ou de l'informatique de gestion. Il s'agit
d'un contrôle qui change radicalement l'organisation même du travail
parce qu'il s'agit d'automatiser les tâches pour qu'elles deviennent des
sources d'information les plus exhaustives possible. Deuxièmement, ces
données ont pour effet d'intellectualiser le travail, c'est-à-dire
qu'elles détachent le travail de la production. Comme le montrent les
auteurs : « les tâches de contrôle et de surveillance remplacent les
tâches de production. » (Missika, Faivret, and Wolton 1977, 41)

Le discours était le même pour ce qui concernait le secteur bancaire et
ajoutait la dimension supplémentaire du service rendu à la clientèle
qui, considéré du point de vue de l'informatisation, présentait ce
danger de la surveillance des individus (Missika, Faivret, and Wolton
1977, 101) :

> L'informatique n'est pas seulement une « solution » aux difficultés
> rencontrées en raison du développement bancaire, elle est avant tout
> la solution patronale : pour tout autre, elle pose plus de problèmes
> qu'elle n'en résout. Pour le personnel, elle signifie la
> standardisation des opérations et la scission entre une masse
> déqualifiée et un petit noyau super-qualifié. Pour la clientèle, elle
> constitue une action commerciale anonyme et un risque pour ses
> libertés.

## Vie privée : inquiétudes


Choc pétrolier et crise économique, augmentation du chômage et
prolétarisation intellectuelle, ont fait des années 1970 une décennie où
les promesses de la technologie devenaient aussi des menaces. S'y
adapter devenait un enjeu crucial pour les travailleurs : cette prise de
conscience était celle de l'impact radical du changement technologique
sur la société.

Démonstration de cet impact dans les représentations communes, en
France, le plan « informatique pour tous » (IPT) lancé en 1985 par le
ministre Laurent Fabius ne poussait pas seulement la société Thomson à
des prétentions au niveau de la grande IBM. Selon les mots du ministre,
ce plan entendait « donner à notre société la chance de mieux dominer
l'avenir » (Fabius 1985). Le message était si bien reçu que, dans les
enquêtes « Informatique pour tous », les enseignants démontraient par
leurs réflexions qu'ils avaient profondément intégré le rapport entre
société et informatique : pour eux le plan IPT ne représentait pas un
moyen de permettre à des élèves de découvrir l'informatique, mais
d'intégrer les usages déjà existants dans le domaine de l'enseignement.
En somme, une nécessaire adaptation de l'école à la société (Narcy
1986) :

> S'il n'est pas surprenant qu'ils voient presque unanimement en elle un
> instrument motivant pour les élèves, il est plus étonnant qu'ils la
> considèrent davantage comme une « nouvelle nécessité culturelle » que
> comme un outil pédagogique paré des vertus du « rattrapage », de
> « l'individualisation » ou de la « rigueur intellectuelle ». C'est, un
> peu comme si l'informatique devait, avant tout, être présente dans
> l'enseignement parce qu'elle est présente dans la vie sociale,
> indépendamment de ses apports spécifiques.

Pour qu'en 1985 les enseignants français puissent à ce point admettre
que l'adoption générale de l'informatique dans la société pousse l'école
à s'adapter aux usages, et non les créer, c'est parce que les
représentations communes allaient dans le sens d'un certain déterminisme
technologique : la technologie était présente et c'était à la société de
s'y adapter.

Les critiques syndicales des transformations technologiques n'étaient
pourtant que rarement technophobes. Simplement, elles avaient tendance à
suivre la même idée qu'à une autonomie de la technologie (au sens de
Jacques Ellul) il fallait répondre par négociations et compromis alors
même que les fonctions de contrôle et de surveillance des technologies
en question étaient considérées comme un instrument de pouvoir patronal.
Même dans les projets où la transformation technologique pouvait
paraître démocratique, l'idée qu'elle puisse être contrôlable relevait
davantage de la croyance que de la soumission à un nouvel ordre
capitaliste. De ce point de vue on peut mieux comprendre la construction
d'une philosophie déterministe de la technique à cette période cruciale
(voir section suivante).

Ceci était d'autant plus paradoxal que le rapport entre vie sociale et
informatique contenait un point particulièrement problématique depuis le
milieu des années 1960 : face aux bases de données informatisées et les
menaces qu'elles portaient sur la vie privée, le débat public n'a cessé
de croître. Là où il fut le plus visible, ce fut sur le territoire
nord-américain à partir du milieu des années 1960 pour au moins deux
raisons : c'est là que s'est industrialisé l'exploitation des bases de
données informatiques (Atten 2013), surtout dans les secteurs bancaires,
assurantiels, et dans l'administration publique, et c'est là qu'on peut
identifier dans les publications (monographies, articles et rapports)
une construction de la *privacy* à partir d'une définition juridique de
la vie privée datant de la fin du XIXe siècle (*The right to be let
alone*).

Pendant près de 50 ans jusqu'à nos jours (et nous pouvons gager que la
question est loin d'être réglée), la construction de la *privacy* est le
fruit d'une convergence entre l'histoire technique des infrastructures
informatiques, l'histoire d'une économie capitaliste de la donnée, et
l'histoire de la lutte pour la protection de la vie privée dans le débat
public et les pratiques de régulation gouvernementales (Masutti 2020).
Face aux abus des pratiques bancaires et des sociétés de crédit
constatés par les consommateurs, l'État fédéral américain mis en place
le Fair Credit Information Act en 1970 puis, résultat des débats tenus
au Congrès sur l'usage des banques de données publiques, le Privacy Act
de 1974 fut voté à son tour.

Les publications furent abondantes au sujet des « menaces sur la vie
privée » que représentaient les bases de données. Employant une
rhétorique qui emprunte chez Georges Orwell et Max Weber un appareillage
conceptuel faisant planer les dangers d'une « société du dossier » à
venir, des auteurs comme Alan Westin proposèrent au début des années
1970 une définition de le vie privée comme « une exigence à décider
soi-même comment et quand les informations qui nous concernent peuvent
être communiquées » (Westin 1967, 7). Peu à peu un courant de pensée se
construisit et s'internationalisa au fur et à mesure que
l'informatisation gagnait tous les secteurs économiques, que l'industrie
de l'informatique devint une industrie faite de multinationales
spécialisées, et que l'emploi des bases de données et des plateformes se
généralisait jusqu'à configurer ce que Foster et Chesney nomment le
*capitalisme de surveillance*, fruit de la jonction historique de
l'impérialisme américain, de l'économie politique et de l'industrie du
numérique (Foster and McChesney 2014).

Daniel J. Solove (Solove 2002), s'efforce en 2002 de synthétiser la
*privacy* en y incluant la définition « primaire » de la vie privée au
sens de la Common Law américaine (le droit d'être laissé tranquille) et
y ajoutant autant de contre-propositions à l'économie de la
surveillance : l'accès limité à la personne, le droit au secret, le
contrôle des renseignements personnels, etc. Et c'est en 2014 avec les
Révélation d'Edward Snowden que le monde apprit que malgré les nombreux
débats et lois promulguées durant des années, des agences publiques et
des entreprises multinationales et monopolistique de l'économie
numérique avaient organisé un vaste espionnage de masse à l'échelle de
la planète.

La construction de la *privacy* est le fruit de plus 50 ans de combats,
de revendications et de créativité juridique. Si bien que l'avènement
tant craint de la « société du contrôle » (de décider, d'agir, de
penser, d'être) est surtout le fruit d'un imaginaire collectif tandis
que la surveillance est devenue le moteur même de l'économie,
translation de la surveillance des process de production de l'entreprise
à la surveillance comportementale des consommateurs, ordonnancement des
modèles monopolistiques et exploitation des données dans dans un
contexte hautement concurrentiel dominé par le marketing et les
objectifs de rentabilité (Zuboff 2019).

Ces débats sur la surveillance donnèrent lieu à de nombreuses études.
Les *surveillances studies* plongent leurs racines dans cette histoire
des années 1970 où l'on commençait à interroger notre rapport à
l'informatique sous l'angle de l'exploitation de la donnée. Dans la
mesure où des données peuvent être extraites de nos comportements pour
être traitées de manière automatisée afin de créer de la valeur, les
comportements peuvent être alors considérés comme des ressources
primaires. Cette assimilation du comportement implique deux choses : la
première est un déplacement du concept de comportement qui n'est plus
seulement la manifestation, compréhension et l'anticipation des actions
pour un observateur, mais la transformation de cette manifestation en
indicateurs quantifiables et assimilables par la machine, c'est à dire
les *données personnelles*. La seconde, c'est l'idée que l'avancement
technologique permet la multiplication des indicateurs, l'accroissement
des données quantifiables, et le passage de l'anticipation à la
prédictibilité : le degré d'intrusion dans la vie privée ne s'évalue
plus seulement en fonction de l'observateur mais en fonction de la
technologie mobilisée pour cela.

En réponse à l'électronicisation de la vie privée et l'algorithmisation
du traitement des données personnelles, les combats pour la vie privée
ne se placent plus seulement sur terrain du droit, mais confrontent le
droit et la technologie selon deux tendances possibles. La première
consiste à maintenir une tradition contractualiste, celle que les
philosophes du droit comme John Rawls ont travaillé durant les années
1970 (Rawls 1997) en articulant éthique et justice, où la liberté
individuelle prévaut. C'est ce qui permet d'expliquer pourquoi les voix
les plus retentissantes comme Allan Westin utilisaient cette double
rhétorique du repoussoir orwellien (le totalitarisme contre les
libertés) et du repoussoir weberien (la société du dossier, la
bureaucratie contre une perspective américanisante d'un
national-libéralisme supposé de Max Weber (Draus 1995)). La seconde
constitue une réaction à la quantification par une institutionnalisation
de la vie privée, la *privacy* : la sanctuarisation de la vie privée
serait le fondement d'un nouvel équilibre entre la société et ses
institutions, le droit et une économie capitaliste qui implique l'usage
illimité des technologies au nom de la rentabilité et du profit
illimité.

À ceci près que pendant que les luttes pour la vie privée s'organisaient
sur de multiples fronts (l'État intrusif, les banques, le marketing,
etc.), on admettait que le questionnement sur la technologie était une
affaire de philosophie et non pas d'usages puisque le déterminisme
technologique ambiant appelait de ses vœux l'informatisation de la
société et sa projection vers un avenir prometteur. Face au « progrès
technologique », il y avait comme un parfum de défaite.

Ce rapport à la vie privée, est un trait caractéristique d'une société
moderne qui, dans son rapport à la technique, en vient à sacraliser ce
qui cristallise les transformations sociales. Ainsi l'État n'est pas
seulement un système de gouvernance fait de dispositifs juridiques,
moraux, ou policiers. L'État moderne est un État sacralisé dans la
mesure où il est peut être assimilé à une entreprise de sécularisation
des concepts religieux (Schmitt 1988 ; Aron 1944). Cet exemple est le
plus évident. Cependant, selon les points de vue, d'autres aspects de
notre modernité sont réputés sacralisés. Ainsi la fétichisation de la
marchandise selon Guy Debord, qui montre de quelle façon l'idéologie
capitaliste est communément cautionnée (Debord 1992). Il reprend ainsi
l'idée de Karl Marx qui montrait que le rapport entre recherche du
bonheur et bien matériel passait par l'illusion qu'une marchandise avait
une valeur par elle-même[^8]. Le thème traverse aussi toute l'œuvre de
Jacques Ellul qui, de manière rétroactive, montre que la technique en
désacralisant le monde (nature comme spiritualité) est devenue par un
effet de renversement le seul recours de l'homme au sacré, la technique
sacralisée (Ellul 1954).

Enfin, la vie privée comme ressource numérique relève aussi d'un autre
thème philosophique, plus ontologique, celui de l'arraisonnement
heideggerien (Heidegger 1958). Si on suit ce philosophe, la technique
moderne n'est plus l'art, elle est devenue ce rapport paradoxal entre la
volonté de puissance et l'exercice technique, soumission de l'homme à
son destin technicien, non plus sujet mais être technique, par
l'instrumentalisation, l'arraisonnement du monde qu'il ne peut plus voir
que comme une ressource. Les Jacques Ellul ou Guy Debord ont en réalité
cherché à montrer que l'homme pouvait encore échapper à ce destin
funeste en prenant conscience ici de la prégnance de l'idéologie
capitaliste et là d'une autonomie de la technique (hors de l'homme).
Cependant, on ne peut s'empêcher de penser que l'extraction de données à
partir de la vie privée est est une forme d'arraisonnement poussé à
l'extrême, sur l'homme lui même, désormais objet de la technique.

Le discours du déterminisme technologique qui implique que la société
doit s'adapter à la technique prend une dimension tout à fait nouvelle
dans le contexte de l'informatisation de la société. À travers
l'histoire de la « Révolution Informatique », ce cauchemar heideggerien
est né d'un discours aliénant, où l'ordinateur devient l'artefact
central, et contre lequel la simple accusation d'aliénation
technologique n'est plus suffisante pour se prémunir des dangers
potentiels de cette désormais profonde acculturation informatique. Les
combats pour la vie privée, loin d'être technophobes, sont autant de
manifestations de l'inquiétude de l'homme à ne pouvoir se réapproprier
la logique autonome de la technique.

## La grande conjecture


### Le déterminisme est une tradition


Si on a longtemps accusé à tort Jacques Ellul d'être déterministe, c'est
pour deux raisons. D'abord parce que c'est un trait caractéristique de
sa pensée que de montrer que la technique est déterminante dans les
transformations sociales. Cependant cela n'implique pas pour autant que
tout fait social soit explicable par la technique. Au contraire, ce qui
alarme Ellul c'est justement que cette technicisation de la société est
en train de se produire, et que la technique moderne devient autonome
(et l'est déjà depuis un certain temps au moment où il écrit).

L'autre raison, c'est l'adoption de la pensée de Ellul aux État-Unis par
une importante diffusion de son livre *La Technique ou l'enjeu du
siècle* (Ellul 1954) sous le titre *The technological society*, publié
en 1964 et introduit par l'éminent sociologue Robert K. Merton et sur
les conseils d'Aldous Huxley (Ellul 1964, sec. 1). Dans sa préface, R.
K. Merton le compare à Oswald Spengler et Lewis Mumford, et soutient
qu'Ellul « propose un système de pensée qui, moyennant quelques
modifications critiques, peut nous aider à comprendre les forces qui se
cachent derrière le développement de la civilisation technique qui est
la nôtre ». Certes la société se technicise, la délibération devient
rationalisation-quantification, l'économie devient pure concentration de
capital et planification forcenée où l'analyse laisse place à la
technique, tout ceci est contenu dans l'œuvre d'Ellul, et Merton le
remarque bien. Mais au-delà d'une mise en garde contre le réflexe
technophobe, c'est aussi d'une lecture techniciste de l'histoire
qu'Ellul nous invite à se méfier (même si la technique joue malgré tout
chez Ellul un rôle primordial et presque normatif dans sa propre lecture
de l'histoire). L'absorption de l'œuvre d'Ellul aux États-Unis a
certainement été en quelque sorte victime de cette lecture trop
techniciste.

L'historien Merritt Roe Smith, après avoir travaillé des années sur
l'histoire des techniques en Amérique, montre à quel point le
déterminisme technologique imprègne la culture américaine au moins
depuis le XVIIIe siècle (M. R. Smith 1994). Les visions technocratiques
issues des discours politiques impliquent ainsi depuis des centaines
d'années l'idée que les innovation technologiques ne sont pas seulement
des illustrations du « progrès en marche » mais sont autant de preuves
de cette marche du progrès, en particulier dans notre quotidien. Selon
M. R. Smith cette vision s'est disséminée dans toutes les sphères
intellectuelles, des politiques aux écrivains en passant par les grands
industriels. L'avènement de la publicité au début du XXe siècle marqua
un virage décisif : le progrès n'était plus simplement le synonyme de
l'espoir industriel, la technologie était « devenue la cause du
bien-être humain ». Face à ce refrain si profondément intégré, les
auteurs les plus critiques tel Henri D. Thoreau qui prétendait (avant
Heidegger) que les hommes étaient « devenus les outils de leurs
outils » (Thoreau 1922), ont pu laisser un héritage littéraire
pertinent. Cependant, d'après M. R. Smith, ce furent trois auteurs qui
produisirent aux États-Unis un appareillage critique central, bien que
contenant encore des bribes de déterminisme puisqu'ils reconnaissent
implicitement que la technique est une force agissante qui modèle
l'histoire : « Lewis Mumford, Jaques Ellul et Langdon Winner ».

Dès lors, que peut-on opposer à des siècles où le paradigme dominant
implique de penser toute avancée technique comme un déterminant social
décisif, central, voire absolu ? On sait bien depuis l'avènement des STS
que ce n'est pas le cas mais encore une fois nous cherchons ici à
comprendre le discours dominant qui n'est manifestement pas celui de
l'historien éclairé mais auquel il est finalement toujours soumis. De
fait, aux États-Unis comme en Europe, le mantra récité durant les trente
années qui suivent l'apparition de l'industrie informatique est celui
qui tient l'ordinateur comme une technologie clé de l'histoire. Ce peut
être le résultat, pour reprendre la pensée de Bernard Stiegler, que la
technologie a toujours été pensée d'après la tradition antique comme un
« dehors » auquel répondait le « dedans » de la philosophie, le savoir,
la connaissance dont la technique n'est au plus qu'un dérivé auxiliaire.
Or, avec l'avènement de l'ordinateur et de la mise en réseau des
machines, c'est la communication et donc le langage, véhicule de ce
« dedans », qui s'est mis à dépendre fondamentalement de la technique.

Les historiens dans leurs travaux plus récents ne furent pas exempts de
cette manière de considérer la technique comme un déterminant, certes
rarement unique ou exclusif, mais autonome, même lorsqu'ils attribuaient
un rôle décisif aux interactions sociales et aux acteurs des
transformations techniques. Par exemple l'historien Paul E. Ceruzzi,
dans la conclusion de sa grande histoire de l'informatique moderne,
revient sur cette modernité. Il l'exprime comme un processus autonome de
remplacement d'une technologie par une autre. Pour lui, l'histoire des
ordinateurs et des réseaux est celle de deux prises de contrôle
successives de l'analogique par le numérique. La première a été
identifiée au début des années 1970 lorsque les ordinateurs ont remplacé
les circuits électroniques analogiques par la programmation et la
miniaturisation. La seconde est Internet, mariage de tout le secteur des
communications avec l'informatique en lieu et place des
télécommunications analogiques. Une prise de contrôle validée « par tout
le spectre politique et culturel, par Newt Gingrich, Al Gore, Stewart
Brand, feu Timothy Leary, la 'génération X' et de nombreuses autres
personnes » (Ceruzzi 1998, 347).

Pourtant, au cours des années 1960 et 1970, la manière dont on concevait
la dynamique du changement technologique était le résultat de
l'observation de ce qui était en train de se passer dans l'industrie :
la spécialisation toujours plus croissante des entreprises, et en
particulier l'apparition des entreprises dites de « haute technologie »
ou « de pointe », et l'informatisation de cette industrie qui permettait
d'accroître la production et travailler en réseau (là où Ford maîtrisait
sa chaîne de A à Z suivant un vieux modèle, les secteurs les plus
technologiquement avancés travaillaient la coopération).

Ce changement permettait d'appréhender différemment le déterminisme
technologique. Ce fut le cas de Robert L. Heilbronner qui publia un
article de référence en posant la question frontalement : les machines
font-elles l'histoire (Heilbroner 1967) ? Il montre que si la
technologie influence l'ordre social, il y a comme un processus d'aller
et retour entre l'innovation et un aspect fondamental de l'organisation
sociale, la production. C'est pourquoi on peut qualifier le déterminisme
de R. L . Heilbronner de « déterminisme doux ». En effet, pour lui, ce
qui fait que les développements technologiques sont prévisibles, ce
n'est pas parce qu'ils sont les fruits d'une application mécanique de la
science sur la technique, mais le résultat d'une organisation de la
production adéquate pour atteindre un stade supérieur de technologie :
division du travail, spécialisation des industries et coopération. Et
cette organisation est elle-même dépendante du capital mobilisé pour
atteindre une congruence efficace entre les fonctions industrielles
diversifiées et coopérantes. Le capitalisme autant que la science
conditionne la production technologique, ce qui fait de la technologie
un médiateur social qui défini les caractéristiques de la société où
elle se développe : la composition de la population active
(spécialisations, compétences) et l'organisation hiérarchique du
travail. Si bien que dans l'histoire récente du capitalisme, là où J.
Ellul pointait une technique qui s'autonomise, R. L. Heilbronner montre
que si le capitalisme est le stimulant d'une technologie de production,
l'expansion de la technologie dans le système de marché prend un aspect
automatique à bien des égards parce que production devient synonyme de
production technologique dans un système concurrentiel et de course à
l'innovation.

Choisir entre antériorité ou postériorité de la technique par rapport au
changement social devient un problème qui a fait son temps. La question
est de savoir comment la technique est conçue comme un intermédiaire
entre capitalisme-productivisme et changement social. Cependant, hors de
l'ombre de ce tout technique, l'historien Lewis Mumford proposait
quasi-simultanément à la parution de l'article de R. L Heibronner,
encore une autre manière d'envisager la technique dans *Le mythe de la
machine* (Mumford 1973). Reléguant le couple capitalisme-machinisme au
rang d'épisode négligeable dans l'histoire de l'humanité, Mumford
resitue le rapport à la technique sur le temps long, depuis la
préhistoire et l'émergence des États et des régimes de gouvernement où
la technique apparaît comme le véhicule du pouvoir, une *mégamachine*
(c'est ce qu'un Serge Latouche affirmera plus tard (Latouche 1995) en
assimilant technique et culture[^9]). Mieux encore, pour L. Mumford, le
rôle de la technique avait largement été fantasmé et cette dissonance
entre société et technique, il l'avait déjà expliqué dans *Technique et
civilisation* en 1934 en ces termes (Mumford 1950, 35) :

> En fait, la nécessité de promouvoir sans cesse des changements et des
> améliorations --- ce qui est la caractéristique du capitalisme --- a
> introduit un élément d'instabilité dans la technique et empêché la
> société d'assimiler ces perfectionnements et de les intégrer dans des
> schémas sociaux appropriés.

Mais dans la (ou les) mégamachine(s) qu'il décrit en 1967, tout est déjà
compris : division du travail, exploitation des classes, pouvoir
centralisé, puissance militaire, logique impérialiste... sans pour
autant fermer la porte à une reconquête possible de la machine ou plutôt
cette « mégatechnologie » qui, aux mains d'une minorité de puissants
menace les individus en les enfermant dans un régime fonctionnel fait de
contrôle et d'automatismes. Au moment où étaient en train de naître les
réseaux de communication numérique, et alors même qu'on avait déjà
assimilé des modèles théoriques de tel réseaux, notamment grâce aux
travaux de Ted Nelson (qui invente l'hypertexte) en 1965, la possibilité
de mettre en relation les individus était en même temps la possibilité
de se sortir éventuellement de la dynamique de la mégamachine qui
instrumentalise la technique. L. Mumford déclarera ainsi en
1972 (Mumford 2014) :

> (...) l'objectif majeur de la technique n'est ni d'étendre encore le
> domaine de la machine, ni d'accélérer la transformation des
> découvertes scientifiques en inventions rentables, ni d'accroître la
> production de nouveautés technologiques changeantes et de modes
> dictatoriales; ce n'est pas non plus de placer toutes les activités
> humaines sous la surveillance et le contrôle de l'ordinateur -- en
> bref, ce n'est pas de riveter les parties de la mégamachine planétaire
> encore indépendantes de manière à ce qu'il n'y ait plus moyen de s'en
> échapper. Non: la tâche essentielle qui incombe aujourd'hui à tous les
> intermédiaires humains, et surtout à la technique, est de restituer
> les qualités autonomes de la vie à une culture qui, sans elles, ne
> pourra pas survivre aux forces destructrices et irrationnelles qu'ont
> déclenchées ses réalisations mécaniques initiales.

Cette possibilité entr'ouverte, elle fut sans doute comprise par bien
des acteurs de l'élaboration commune d'un agrégat technique qui sera
nommé Internet. C'est très certainement cette ré-appropriation de la
technologie qui a motivé les hackers du projet Community Memory et bien
d'autres initiatives du genre, et qui donnèrent lieu à une économie de
services et un choix qui fit de L. Mumford une Cassandre des temps
modernes.

### Réseaux, démocratie, capital


On attribue généralement la naissance d'Internet à la mise en œuvre
combinée de projets militaires et civils aux États-Unis à la fin des
années 1960. Sans revenir sur cette histoire maintes fois racontée, il y
a plusieurs aspects que nous devons prendre en compte dans la manière
dont l'usage des technologies de réseau s'étendit à travers la société.

En premier lieu, les réseaux informatiques sont le résultat d'une
combinaison d'ordinateurs, de systèmes d'exploitation et de protocoles
de communication. Des ordinateurs, avant tout, puisque le projet initial
de l'ARPA (pour l'exprimer simplement : construire un réseau résilient)
comportait une alliance très forte avec une entreprise issue du MIT
nommée BBN (créé par les professeurs Leo Beranek et Richard Bolt,
rejoints ensuite par un de leurs étudiants, Robert Newman) qui elle même
entretenait des liens historiques avec DEC (Digital Equipment
Corporation), productrice d'ordinateurs type PDP pour une grande partie
des instituts de recherche. Bien que DEC ne fut pas la seule entreprise
concernée, le développement ultérieur des mini-ordinateurs de type
PDP-11 puis le passage à des architectures 32 bits, la baisse des coûts
de production, jouèrent un rôle décisif dans le développement du marché
des mini-ordinateurs et leur mise en réseau. L'arrivée de systèmes
d'exploitation à temps partagé (dont CTSS -- Compatible Time-Sharing
System -- fut l'un des projets phare au MIT, avec pour successeur le
projet Multics) fut l'une des conditions à réunir pour permettre aux
utilisateurs de se connecter à plusieurs sur un ordinateur. Ces
créations furent associées à d'autres concepts théoriques et applicatifs
tels la commutation de paquets (un sujet que Paul Baran avait initié au
début des années 1960), ou, de manière structurée, la suite de
protocoles TCP/IP (travaillée par Bob Kahn au début des années 1970).

Ces exemples ne montrent qu'une petite partie de l'ensemble nommé
Internet, et qui n'est lui-même que la partie de réseaux de réseaux
accessible au public. Des premières recherches conceptuelles qui
s'agrégèrent jusqu'aux réseaux opérationnels, les technologies de base
d'Internet ont été développées sur une trentaine d'années et ne cessent
d'évoluer. De plus, cet agrégat n'est pas qu'un amalgame de
technologies, il est le fruit de bien des aspects relationnels entre
acteurs et groupes d'acteurs, institutions et entreprises. De ces
convergences découla la concrétisation de la notion de *computer
utility*, c'est-à-dire un modèle économique de service : mise à
disposition de ressources informatique (puissance de calcul) et gestion
d'infrastructures (Garfinkel 1999). Ce modèle fut pensé dès le début des
années 1960 mais l'arrivée de l'informatique en réseau lui donna corps,
et avec cela tout un discours sur la transformation sociale qui, en un
sens radicale, supposait que la technologie recelait en elle et de
manière positive les ingrédients de cette transformation, comme si elle
ne relevait d'aucune pensée préalable. Or, ce n'était évidemment pas le
cas. Nous l'avons vu avec la pensée hacker, et comme l'a brillamment
montré Fred Turner à travers des hommes comme Stewart Brand et *The
WELL*, il en va ainsi de la majorité des acteurs principaux qui ont
construit l'économie de la Silicon Valley sur l'idée que les
technologies apportent les réponses à tous les « grands problèmes du
monde ».

Les réseaux et leurs infrastructures ne sont pas seulement le substrat
technique sur lequel s'est développée l'économie numérique. Nombre
d'acteurs qui ont construit cette économie partageaient une vision
commune du rapport entre société et technique que l'on pourrait
qualifier de solutionniste. Ils marquèrent sur plusieurs décennies le
point de basculement principal dans le processus d'informatisation de la
société : les ordinateurs n'étaient pas qu'une extension du machinisme
dont l'objectif était d'augmenter notre production et améliorer notre
bien-être. Ils allaient plutôt nous permettre de changer le monde et
pour cela, il était impératif que la société accepte le rôle
calculatoire de l'ordinateur comme le maillon principal des processus de
décision et d'action.

Bien entendu, ce n'était pas le point de vue de tous les hackers.
Cependant, comme le montre Fred Turner, le déplacement de la
contre-culture hippie associé à la logique industrielle et au
libéralisme a créé un certain anti-autoritarisme qui s'est lui-même
structuré en ce que nous pouvons identifier comme un « libertarianisme
californien ». Le couple technologie-capital que mentionnait R. L.
Heilbronner a fini par captiver cette contre-culture au point que la
plupart de ces ingénieurs hétérogènes (si on les considère du point de
vue acteur-réseau) on fait le choix de l'aventure entrepreneuriale.
C'est au début des années 1980 que d'autres ont au contraire commencé à
y voir les déséquilibres économiques et éthiques et investirent alors le
modèle du logiciel libre, comme nous le verrons plus loin. Toujours
est-il qu'Internet devenait l'El-Dorado de l'économie de service, point
de jonction entre l'encapacitation sociale et le cercle vertueux du
capital et de l'innovation.

La fin des années 1970 et le début des années 1980 furent des moments
propices à la construction de cette représentation d'Internet et de
l'économie de services. Cette période connu non seulement l'arrivée de
l'ordinateur personnel sur le marché de la consommation de masse mais
aussi la consolidation des systèmes de gestion de bases de données, et
une assimilation unanime de l'informatique à la fois par les entreprises
de tous les secteurs d'activité et les administrations publiques. Ainsi,
en 1978, dans son *best seller* intitulé *The Wired Society* (Martin
1978) (récompensé du Pullizer), James Martin pouvait bénéficier d'un
terrain d'étude extrêmement favorable sur ce qu'il a nommé les
« autoroutes des télécommunications ». Il ne s'agissait plus de supposer
le développement futur des usages et de leurs implication dans la
société, mais de faire un premier point sur ce qu'avait changé la haute
disponibilité de l'information. Huit ans plus tôt, dans un précédent
ouvrage co-écrit avec Adrian Norman, *The Computerized Society* (Martin
and Norman 1970) il s'agissait de prévenir des dangers potentiel de
l'omniprésence des algorithmes dans les systèmes décisionnels : à partir
du moment où un système est informatisé, la quantification devient la
seule manière de décrire le monde. 0r la veille des années 1980, son
analyse n'avait guère changé : le danger ne peut provenir que de ce que
l'homme fera des techniques à sa disposition mais elles sont
libératrices par essence, leur utilité l'emporte sur l'adversité. Il
devenait le futurologue qui mettait fin aux craintes de l'avènement
d'une société Orwellienne : la diversité des informations et leur
accessibilité sur les autoroutes de télécommunication devait agir comme
une garantie à l'encontre de la manipulation mentale et de
l'autoritarisme. Les promesses de l'accès illimité aux contenus
éducatifs et du partages des connaissances devaient constituer
l'antithèse des régimes totalitaires qui restreignent et contrôlent les
communications.

Comment comprendre ce revirement qui, à nos yeux contemporains et
compte-tenu de ce que nous savons de la surveillance en démocratie,
passe aussitôt pour de la naïveté ? En démocratie, l'information et le
partage d'information est un élément décisif de l'épanouissement d'une
société. Pense-t-on pour autant que, développés en majeure partie dans
des pays démocratiques et propagés par les vertus de l'économie
libérale, les réseaux de communication numériques ne pourraient servir
que des objectifs démocratique ? Il est compréhensible que leur
avènement ait tant bousculé les organisations que cette révolution ai un
temps occulté le fait que l'analogie n'est pas fondée.

La même année, en 1978, Murray Turoff Roxanne Hiltz publient *The
Network Nation* (Turoff and Hiltz 1994), un livre aux conséquences
souvent mal connues mais qui consacra le domaine des communications en
ligne comme la technologie-clé censée révolutionner la vie sociale et
intellectuelle. M. Turoff avait inventé un système de conférence
électronique pour le gouvernement américain afin de communiquer en temps
de crise. Initiateur du concept de Communication médiée par ordinateur
(computer-mediated conferencing, CMC), il continua avec Roxanne Hiltz,
qui devint son épouse, à développer son système électronique d'échanges
d'informations jusqu'à aboutir à une présentation exhaustive de ce que
signifie vraiment communiquer à distance par ordinateurs interposés :
échanges de contenus (volumes et vitesse), communication
sociale-émotionnelle (les émoticones), réduction des distances et
isolement, communication synchrone et asynchrone, retombées
scientifiques, usages domestiques de la communication en ligne, etc.
Récompensés en 1994 par l'EFF Pioneer Award, on considère aujourd'hui
Murray Turoff et Roxanne Hiltz comme les « parents » des systèmes de
forums et de chat massivement utilisés aujourd'hui.

Pour eux, dans la mesure où les réseaux et leurs infrastructures
continuent de se développer, la Network Nation ne demande qu'à éclore,
aboutissant à un « village planétaire », concrétisation de la pensée de
Marshall McLuhan, où « la technologie en viendra probablement à dominer
la communication internationale » (Turoff and Hiltz 1994, 25). À l'aide
de nombreuses études de cas, ils démontrent combien l'organisation
sociale (dont l'éducation et le travail à distance) et les modèles de
décision collective peuvent être structurés autour des
télécommunications numériques. De l'extension du modèle de démocratie
participative asynchrone et obtention de consensus par conférence
téléphonique (Etzioni, Laudon, and Lipson 1975), la récolte de l'opinion
publique dans les Community Centers de Hawaï en 1978, jusqu'au test
grandeur nature du Public Electronic Network de Santa Monica, Murray
Turoff et Roxanne Hiltz montrent à quel point la démocratie
participative par réseaux de communication pourrait même bousculer
radicalement la démocratie représentative qui, elle, est basé sur une
participation indirecte. Sans apporter vraiment de réponse à cette
question, il reste que les auteurs livrent à ce moment-là une conception
selon laquelle la technique est à même de conditionner la vie publique.
Les choix relatifs à la régulation des usages sont désormais
écrits (Turoff and Hiltz 1994, 400) :

> Le domaine des communications numériques a atteint un point où ce
> n'est plus la technologie, mais les questions de politique, de droit
> et de réglementation qui détermineront le degré de bénéfice que la
> société en tirera. Si nous extrapolons les tendances actuelles, des
> aspects tels que les utilisations publiques peuvent être
> artificiellement retardés de plusieurs décennies (...)

Mais ce qui devra arriver arrivera, et cela tient intrinsèquement à la
nature même des technologies de communication (Turoff and Hiltz 1994,
401) :

> Dans la mesure où les communications humaines sont le mécanisme par
> lequel les valeurs sont transmises, tout changement significatif dans
> la technologie de cette communication est susceptible de permettre ou
> même de générer des changements de valeur.

Communiquer avec des ordinateurs, bâtir un système informatisé de
communication sociale-émotionnelle ne change pas seulement
l'organisation sociale, mais dans la mesure où l'ordinateur se fait de
plus en plus le support exclusif des communications (et les prédictions
de Turoff s'avéreront très exactes), la communication en réseau fini par
déterminer nos valeurs. C'est en cela que la conjecture selon laquelle
l'ordinateur est une technologie déterminante est une conjecture qui
prend une dimension heuristique à la fin des années 1970. Et c'est la
raison pour laquelle à cette époque l'ordinateur passe si difficilement
sous le feu des critiques : on lui préfère l'analyse du machinisme ou
les références anciennes à la domination de l'outil de travail.

Dans la sphère intellectuelle américaine, plus versée dans la
littérature que dans les modèles de systèmes d'information, on retrouve
l'adhésion à ce discours. Les enjeux humanistes qu'il soulève devenaient
parfaitement audibles aux oreilles des non-techniciens. C'est le cas de
Jay David Bolter qui, bien que formé à l'informatique, n'en était pas
moins professeur de littérature soucieux de l'avenir culturel à l'âge
informatique. En 1984, il écrivit un autre *best seller* intitulé
*Turing's Man: Western Culture in the Computer Age* (Bolter 1984). Pour
lui, si l'ordinateur est une technologie déterminante, ce n'est pas
parce que l'usage de la technique change les valeurs, mais c'est parce
que l'ordinateur est comparable à la machine à vapeur ou à l'horloge
qui, selon lui, on révolutionné notre représentation du monde en
redéfinissant notre rapport au temps, à la mémoire, à la logique, à la
créativité, au langage... Bien que nous puissions en dire autant de bien
d'autres techniques. En fait, si on se fie à J. D. Bolter, ces dernières
sont déterminantes parce qu'elle se situent haut sur une sorte
d'« échelle de la détermination technologique ». Comme toutes les
révolutions, la révolution informatique se mesure à l'aune de ses
dangers et de la pensée critique qui s'en empare. Pour J. D. Bolter,
l'âge informatique est un âge où le risque est grand de perdre
l'humanisme occidental en le noyant dans une quantification de nos vies
par le traitement des données informatiques. Et si la tradition
humaniste est à ce point en danger, ce serait parce que la technologie a
toujours déterminé la pensée. L'ordinateur suit la même voie
séculaire (Bolter 1984, 36) :

> Le scientifique ou le philosophe qui travaille avec de tels outils
> électronique pensera différemment que ceux qui travaillaient sur des
> bureaux ordinaires, avec du papier et des crayons, avec des stylets et
> des parchemins, ou du papyrus. Il choisira des problèmes différents et
> trouvera des solutions différentes.

Là encore, tout comme dans le livre de James Martin, on se situe bien
loin des craintes d'un futur Orwellien, mariage entre technologie et
totalitarisme : le côté rassurant de la technologie, aussi
révolutionnaire qu'elle soit, c'est qu'elle se présente désormais comme
un « déjà-là » et détermine ce que sera « l'homme de Turing ». Un état
de fait qu'on ne peut qu'accepter et en trouver les avantages.

Là où Lewis Mumford voyait entr'ouverte la porte de sortie du
déterminisme, J. D. Bolter la referme avec la plus grande candeur : il
appartiendrait à chacun de savoir conserver la part d'humanisme dans un
monde fait par la technique et pour la technique. Mais là encore, tout
comme beaucoup ont mal lu J. Elllul, beaucoup ont fait l'erreur de ne
pas comprendre que la mégamachine moderne n'est pas toute entière issue
de la technique ou de la pensée technocratique. Il faudra plusieurs
années entre la fin des années 1960 et le début des années 1990 pour
que, à travers le ronronnement permanent des discours sur les bienfaits
de l'innovation technoscientifique, émergent des dissonances qui ne
s'attachent plus uniquement au problème du déterminisme technologique et
au regret d'un humanisme perdu, mais sur les dynamiques sociales qui
conditionnent la mégamachine.

En 1966, la *Monthly Review* publiait l'ouvrage monumental de de Paul
Sweezy et Paul A. Baran, *Le capital monopoliste, un essai sur la
société industrielle américaine*. L'idée que le pouvoir n'est finalement
que secondaire dans une société capitaliste où la logique économique a
son cheminement propre, non pas essentiellement technique mais résultat
d'une tension entre production et coût de production d'où émergent des
tendances impérialistes, consuméristes, monopolistes. Pour survivre, les
entreprises sont condamnées à innover. Et c'est ce qu'en 1991 et 1995
l'économiste Serge Latouche nomme le principe du « maximine » (Latouche
1991  ; Latouche 1995) (maximisation du profit, minimisation des coûts),
selon lequel l'innovation technologique est le moteur qui génère nos
comportements modernes de consommateurs et d'accumulateurs. Pour Serge
Latouche, la mégamachine est en réalité la société toute entière qui
entretien une synergie entre logique technicienne et logique économique.

Et d'autres auteurs encore développèrent cette approche critique, y
compris de nos jours, via une démarche opérant une certaine filiation
américaine de la pensée « néo-marxiste ». Ainsi John Bellamy Foster et
Robert Mc Chesney qui, au lendemain des révélations Snowden, montrent
comment la logique monopoliste, impérialiste et militariste
conditionnent notre soumission à un capitalisme de surveillance.
C'est-à-dire un capitalisme technologiste qui, pour reprendre le mot de
Bernard Stiegler, contribue à *prolétariser* les individus dans leur
quotidien, leur vie, leur comportement, leurs relations, là où on
attendait justement des infrastructures technologiques des
communications l'espoir d'une émancipation démocratique qui manquait
déjà tant aux années 1970.

## Internet, dispositif indéterminé


Sommes-nous arrivés à un âge critique où, après un trop-plein
d'innovations techniques dont l'économie numérique serait la dernière
manifestation (à la suite de la révolution pharmaceutique ou de
l'automobile), une prise de conscience serait advenue faisant valoir un
discours réaliste sur les interactions entre les dynamique sociales et
les techniques ? Prendrait-on conscience d'un trait caractéristique de
notre modernité selon lequel la société est dominée non pas par la
technique mais par une logique d'accumulation qui détermine à son tour
les conditions de l'innovation et donc les techniques ? Marketing,
consommation, impérialisme militaro-économique, concurrence et
monopoles, décision publique favorable aux monopoles, tous ces leviers
(on peut ajouter le capitalisme d'État pour certaines puissances) en
faveur des objectifs de production des sociétés modernes montrent que si
on peut définir une société par ses technologies-clé, cette définition
est non seulement non-exclusive mais doit intégrer le fait qu'il s'agit
avant tout de choix collectifs, de stratégies de développement et non de
la technique en soi.

C'est l'argument retenu lorsqu'on envisage les changements
technologiques à une échelle plus globale et qu'on la confronte aux
enjeux environnementaux. D'un côté, la lutte contre le changement
climatique ou la prise de conscience des écueils de l'anthropocène et,
de l'autre, l'idée que ce sont nos choix techniques qui ont dirigé plus
ou moins consciemment les sociétés vers les impasses de l'urgence
climatique. Là ne se confrontent plus artefacts et nature (si tant est
que l'homme technicien se définisse comme « maître et possesseur ») mais
capital et *oïkoumène*, le milieu, notre commun naturel. Et nous
aboutissons aux même arguments de Bernard Stiegler : les technologies
numériques font partie d'un ensemble de choix techniques dont la
tendance générale consiste à prolétariser toujours plus l'homme
(exceptés ceux aux commandes). En séparant technique et savoir, l'homme
s'est privé de la critique scientifique de la technique et de là vient
la tendance déterministe alors que l'homme se voit dépossédé de ses
savoir-faire (automatisation) et savoir-être (algorithmisation de nos
comportements). Penser la technique aujourd'hui consiste alors à se
demander comment nous pouvons entamer une procédure de déprolétarisation
en construisant, selon B. Stiegler, une économie de la contribution,
c'est-à-dire une économie dont l'objectif n'est plus composé du moteur
accumulation-innovation-profits. Nous y reviendrons plus loin.

Face à cette critique, les forces qui opposent une vision déterministe
de la technique relèvent de l'intérêt particulier d'une vision
capitaliste dont la caractéristique est de livrer une description
monolithique des techniques. Pour qu'une technologie-clé soit considérée
comme telle, alors que de sa promotion dépendent les choix économiques,
il importe de la définir comme une technologie aboutie ou mature, et
surtout libérée des contraintes sociales, c'est-à-dire postuler une
neutralité de la technique, une suspension dans les vapeurs éthérée du
progrès. Qu'est-ce que cela signifie ? C'est le déploiement d'un
discours d'autojustification de la décision : l'utilité économique de la
technologie l'emporte sur sa valeur sociale, ou en d'autres termes le
besoin individuel (sublimé par l'acte de consommation) l'emporte sur
l'intérêt commun. L'autonomie de la technique annoncée par J. Ellul est
advenue et semble pleinement assumée par les discours dominants des
entreprises monopolistes de la Silicon Valley pour lesquelles il
n'existe aucun problème que la technologie ne puisse résoudre, et qui
guident la décision publique vers un régime de « start-up nation » comme
le souhaitait ardemment le président Macron.

Alors même que le mot « numérique » est sur toutes les lèvres pour
promouvoir ce projet global de soumission à un ordre technologique
capitaliste, Internet et le mouvement du logiciel libre démontrent
depuis le milieu des années 1980 qu'il est possible de se réapproprier
socialement la technique et la libérer des discours déterministes.
Lorsqu'Internet a commencé à être conçu comme un moyen ouvert de
télécommunication assistée par des ordinateurs en réseau, c'est-à-dire
comme un moyen de communication social-émotionnel en même temps qu'une
infinité d'offres de services et d'innovation dont le modèle économique
a explosé dans les années 1990, il a produit en réalité un dispositif
technique dont la prégnance sur la société contemporaine est si
importante qu'il semble déterminer nos comportements et nos relations
sociales. Mais ce n'est qu'une apparence. Nous avons vu qu'Internet est
en fait un amalgame de dispositifs techniques. Plus exactement, on peut
désigner Internet comme un méta-dispositif socio-technique :

-   Sa construction historique est une association d'infrastructures
    techniques différentes, de programmes et de protocoles différents,
    tous issus d'une dynamique d'échanges entre des groupes sociaux
    (issus de la recherche académique, d'entreprises ou des institution
    décisionnaires) ;
-   Son modèle de gouvernance est essentiellement basé sur l'ouverture,
    comme le montre l'Internet Engineering Task Force (IETF) et son
    système de requêtes de commentaires, ou comme la suite de protocole
    TCP/IP (depuis 1973) : l'ouverture s'oppose ici à la possibilité
    qu'un ayant droit, comme un constructeur, puisse décider des
    conditions d'usage. Tout le monde peut contribuer à l'avancement
    d'Internet et des protocoles de communication utilisés. Des
    organismes de normalisation tels l'IETF ou le W3C consolident
    juridiquement les innovations issues de cette dynamique d'ouverture
    (pour éviter, par exemple, les *patent troll*) ;
-   C'est en particulier grâce à cette liberté des échanges que le
    mouvement du logiciel libre, dont le concept a été défini au début
    des années 1980, a pu s'émanciper. L'ouverture d'Internet associée
    aux libertés logicielles a construit un modèle économique
    contributif, c'est-à-dire dont le niveau de coopération et de
    production ne dépendent pas du développement individuel des parties,
    mais du niveau de progression des communs numériques.

En 2020, les chercheuses Mélanie Dulong de Rosnay et Francesca
Musiani (Dulong de Rosnay and Musiani 2020) ont publié un article qui
interroge la pertinence aujourd'hui des solutions alternatives d'un
internet centralisateur et visiblement en tout point éloigné des
objectifs d'émancipation que les discours politiques ont tendance à
rabâcher. Face aux entreprises monopoliste et au capitalisme de
surveillance, elles questionnent les possibilités qu'offrent depuis des
années les technologies ouvertes tels le pair à pair (P2P) et autres
moyens de communication décentralisés, de pouvoir proposer des
alternatives tout en se rapprochant des nouveaux modes de production et
de partage qu'offrent les modèles basés sur les communs. Cette manière
de concevoir ce rapport entre technique et société permet aujourd'hui de
considérer une autre organisation de la société, d'autres modes de
production et d'échanges en alliant les architectures décentralisées et
la gouvernance des biens communs (Bauwens and Kostakis 2017).

Étant donné le peu de recherche à ce sujet avant les années 2010, nous
avons eu tendance à sous-estimer l'impact de la logique du logiciel
libre et des échanges P2P dans la représentation générale du rapport
entre société et technique. Ce n'est pas par hasard qu'au milieu des
années 1980 le concept de logiciel libre se déploie dans la culture en
appliquant ses principes à d'autres formes d'échanges et tout
particulièrement les connaissances. Les années 1980 correspondent aussi,
nous l'avons vu, à l'arrivée des *sciences studies*. Même si leur
naissance conceptuelle débute un peu auparavant (Pestre 2006), les
années 1980 furent un véritable âge d'or d'une modernisation de
l'histoire des sciences et des techniques. Et à cet impératif
d'interdisciplinarité répondait un changement général dans les discours
sur les techniques, celui de l'intégration entre science, technique et
société. Il ne pouvait plus être affirmé sans contradiction immédiate
que le développement des technologies informatiques et surtout les
programmes était une affaire d'innovation et de marché uniquement :
comme c'était le cas aux premiers âges des ordinateurs *mainframe*, le
logiciel libre, le développement mondial de GNU/Linux, l'arrivée de
Wikipédia, tout cela montrait que l'informatique était avant tout une
affaire de communautés d'utilisateurs et de créateurs. Les réseaux ont
accentué significativement la tendance au partage et ont fait de ces
communautés des illustrations évidentes des dynamiques de
co-construction entre techniques et organisation sociale.

Ne nous méprenons pas : il n'y a pas de lien de causalité entre
l'émergence de communautés mondiales d'utilisateurs et de programmeurs
communiquant avec Internet et le nouvel élan scientifique des *science
studies*. Les deux sont concomitants : des convergences conceptuelles
existent entre une vision sociale des techniques et un vécu social des
techniques. Le logiciel libre, formalisé juridiquement par les licences
libres, est une démonstration à grande échelle de ce que les communautés
de pratiques apportent comme apprentissages organisationnels et
dynamiques d'innovation. Les communautés ne sont pas que des collectifs
de partage mais des maillons décisifs dans l'organisation collective du
processus de création : conceptualisation, tests, validation (Cohendet,
Créplet, and Dupouët 2003).

Le mouvement du logiciel libre, parce qu'il est avant tout un mouvement
social, a suscité beaucoup d'espoirs au point de le considérer comme une
utopie plus ou moins concrète qui proposerait une alternative au modèle
capitaliste dominant. Il n'en est rien : comme le montre Sébastien
Broca (Broca 2018), ce mouvement est profondément ancré dans le modèle
capitaliste et notre modernité. Certes, il propose de nouveaux
équilibres face aux impasses de la propriété intellectuelle, de la
concentration des savoirs, de l'accès aux technologies, et de
l'organisation du travail (opposer verticalité et horizontalité,
cathédrale ou bazar). Pour autant, les libertés que garanti le logiciel
libre ne s'opposent à aucun modèle économique en particulier. Dans la
mesure où le secteur de la programmation et des services est aujourd'hui
un secteur soumis à de très fortes tensions concurrentielles, il n'y a
pas de raison pour que les entreprises spécialisées (les sociétés de
services en logiciels libres, SSLL) échappent au modèle dominant, même
si elles portent en elles des messages éthiques auxquels adhèrent les
utilisateurs. L'éthique du logiciel libre apporte une régulation du
marché, sans doute la meilleure, en s'appuyant sur des arguments solides
et qui engagent utilisateurs et concepteurs dans un avenir cohérent et
durable.

Ce qui a fondamentalement changé la donne, en revanche, c'est
l'intégration du concept de pair à pair dans les structures mêmes des
réseaux de communication. Et parce que les protocoles qui le permettent
sont ouverts et basés sur les mêmes libertés numériques que promeut le
mouvement pour le logiciel libre, alors le méta-dispositif
socio-technique qu'est Internet peut proposer des modèles de production
de valeur indépendants des marchés dominants et des politiques
économiques. Historiquement cette idée fait écho à la Déclaration
d'Indépendance du Cyberespace écrite en 1996 par John Perry Barlow.
Cette dernière été comprise comme le rêve libertarien d'un « village
global » de communications et d'échanges sans État ni pouvoir central. À
bien des égards, c'était le cas, car en 1996 la Déclaration était
d'abord une réaction au Telecommunications Act instauré par le
gouvernement de Bill Clinton aux États-Unis, qui ouvrait la concurrence
dans les services et équipements de télécommunication et contribua
finalement à la bulle capitaliste qui explosa en 2000. En revanche, la
Déclaration oppose deux approches de la communication : l'une soumise à
un ordre de gouvernement qui impose son modèle par le haut, et l'autre
qui promeut, en « défense de l'intérêt commun », une « conversation
globale de *bits* », des échanges selon un ordre collectivement
déterminé. Face à un ordre économique profondément inégalitaire, un
système différent basé sur des échanges égalitaires permet une
mutualisation de l'information et structure la gouvernance des communs.
Une gouvernance décidée, orientée, régulée collectivement.

Quels communs ? En premier lieu des biens communs informationnels, dont
la caractéristique est d'être non rivaux. Mais d'autres communs peuvent
être ainsi gouvernés collectivement de manière à ce que les échanges
informationnels de pair à pair permettent de structurer la décision et
l'organisation, tout comme le logiciel libre permettait des
apprentissages organisationnels. Les modèles de production et d'usage
sont alors des modèles sociotechniques : décision et processus de
décision, le substrat technique sur lequel la décision se structure,
c'est-à-dire les outils communicationnels de pair à pair, autant que les
échanges de pair à pair de biens et services. Il peut s'agir de modèle
bénévoles ou de modèle marchands. Dans tous les cas, le principe de pair
à pair empêche fondamentalement les déséquilibres que cause la prédation
d'une économie extractive qui ronge les communs et les individus, qu'il
s'agisse de l'environnement, de la connaissance ou encore de nos données
personnelles.

Dans une économie pair à pair, il est aussi important de structurer les
échanges que de les pratiquer. C'est pourquoi, lorsqu'on se concentre
sur le rôle que joue Internet (et le *cyberespace*) il est important là
encore d'éviter toute approche déterministe. La proposition selon
laquelle l'innovation venue d'un monde « d'en haut », imposée comme une
technique autonome à laquelle nous devrions nous adapter, est
fondamentalement une proposition fallacieuse  : s'adapter à Google ou à
Facebook comme les seuls pourvoyeurs de solutions de communication sur
Internet, c'est accepter des modèles d'échanges qui sont
structurellement inégalitaires. Certes des échanges auront bien lieu,
mais ils se soumettent à une logique extractive et, comme l'a montré
l'affaire Cambridge Analytica, ils imposent aussi leur propre ordre
politique. L'autre face de cette proposition fallacieuse consiste à
séparer technique et société, en faisant croire qu'Internet est avant
tout un ensemble de services techniques précis dont l'avenir ne dépend
pas des utilisateurs mais des capacités d'innovation des entreprises qui
construisent pour nous un cyberespace mesuré et régulé... alors qu'il
est essentiellement conformé à leurs propres intérêts, ainsi qu'en
témoigne la tendance concentrationnaire des conditions de l'innovation
technique (propriété intellectuelle, puissance financière, possession
des infrastructures).

Lorsque le philosophe des techniques Andrew Feenberg s'interroge à
propos d'Internet et de son pouvoir d'encapacitation
démocratique (Feenberg 2014), il commence par montrer combien Internet
est un dispositif qui échappe aux analyses classiques. Historiquement,
la stabilisation d'une technologie dans la société vient du fait qu'elle
entre dans une dynamique de production et d'innovation qui lui est
propre : ce n'est qu'après que le moteur à explosion soit choisi de
préférence par les constructeurs que l'automobile passa du statut de
prototype à objet de luxe puis à un objet de consommation qui modifia
l'organisation sociale en tant que mode de transport. Il n'y a pas
d'« artefact Internet » et encore moins qui puisse répondre à ce schéma.
C'est pourquoi le fait qu'Internet soit un espace d'échanges qui
favoriserait la démocratie est une affirmation qui, selon A. Feenberg
doit d'abord prendre en considération qu'Internet est un dispositif
sociotechnique qui est encore indéterminé. Pour notre part, nous pensons
qu'Internet est potentiellement un dispositif *à jamais* indéterminé à
condition de maintenir le pair à pair comme principe fondamental.

On peut considérer le dispositif dans un sens philosophique.
Essentiellement en référence à Michel Foucault, plusieurs penseurs ont
travaillé cette notion de dispositif. Giogio Agamben répond à la
question *Qu'est-ce qu'un dispositif ?* en ces termes (Agamben 2014  ;
Agamben 2006) :

> (...) tout ce qui a, d'une manière ou d'une autre, la capacité de
> capturer, d'orienter, de déterminer, d'intercepter, de modeler, de
> contrôler et d'assurer les gestes, les conduites, les opinions et les
> discours des êtres vivants. Pas seulement les prisons donc, les
> asiles, le *panoptikon*, les écoles, la confession, les usines, les
> disciplines, les mesures juridiques, dont l'articulation avec le
> pouvoir est en un sens évidente, mais aussi, le stylo, l'écriture, la
> littérature, la philosophie, l'agriculture, la cigarette, la
> navigation, les ordinateurs, les téléphones portables, et, pourquoi
> pas, le langage lui-même (...)

Définir un dispositif, c'est le faire entrer dans une liste dont
l'objectif est de rassembler des éléments certes hétérogènes
(dispositifs institutionnels, cognitifs, techniques, etc.) mais dont, à
un moment donné la cohérence défini l'action. Par exemple l'action de
gouverner, d'imposer un pouvoir, ou pour ce qui concerne un dispositif
technique, le fait de déterminer des usages ou configurer plus ou moins
significativement l'organisation sociale. Envisager Internet comme un
dispositif, au sens philosophique, amène une dimension supplémentaire à
notre analyse : si Internet est un dispositif indéterminé, il peut
devenir l'infrastructure au sens métaphorique, ou la matrice matérielle
et logicielle, pratique et théorique, des relations de pair à pair à la
source d'alternatives plurielles et néanmoins cohérentes au modèle
privateur dominant, et donc matrice d'apprentissages organisationnels
ouvrant une pluralité de choix sociaux, économiques, technologiques,
politiques.

Internet est ce que nous en ferons. Et pour cela il n'y a aucune
ambiguïté dans les projets communautaires consistant à bâtir des réseaux
dont les caractéristiques intègrent techniquement la logique des
échanges pair à pair grâce au développement de logiciels libres. C'est
le cas de ce qui a été nommé depuis quelques années le *Fediverse*. Il
s'agit d'un ensemble de serveurs fédérés entre eux de telle sorte que
chacun d'entre eux puisse échanger avec les autres de manière
transparente afin de former des réseaux décentralisés de publications de
contenus. Chaque serveur propose au moins une instance d'un logiciel
libre dont le but est de permettre aux utilisateurs de communiquer entre
eux et à travers toutes les instances, par exemple pour des activités de
micro-blogage, de blog, de partage de vidéo, de musique, etc. Les
logiciels sont libres (le code est connus de tous) et les protocoles
ouverts sont standardisés (depuis 2008 avec OStatus jusqu'au protocole
ActivityPub recommandé par le W3C). Les seules limites au Fediverse sont
celles de la créativité de ses utilisateurs et concepteurs ainsi que
l'impossibilité technique de centraliser et capter des ressources,
surtout dans une logique de profit, sans sortir du Fediverse. Quant aux
usages, et le fait qu'une instance puisse être acceptée par les autres,
ils reposent eux aussi sur les notions de partage et de réciprocité. Le
Fediverse est sans doute la plus brillante démonstration de
l'indétermination d'Internet en tant que dispositif sociotechnique :
nonobstant la législation en vigueur dans le pays où une instance est
installée, toute tentative de régulation par le haut afin de défendre
des intérêts particuliers est vouée à l'échec (de l'un ou de l'autre ou
des deux).

## Conclusion : portes de sortie


Nous avons vu qu'une tradition du déterminisme technologique
transparaissait dans les représentations d'une société informatisée,
mêlant ici les espoirs de l'encapacitation générale à la démocratie, et
là la nécessité d'un modèle productiviste et innovateur. L'économie
numérique s'est imposée comme un nouvel ordre face auquel les critiques
« anti-déterministes » avaient tendance à focaliser sur les critiques du
pouvoir que cet ordre imposait, ou sur les manquements éthiques d'une
technique prétendue autonome. Cependant, si l'avancement technologique
est bien le jeu d'interactions entre sciences, techniques et société, il
pouvait y avoir la possibilité pour que certaines techniques intègrent
en elles *by design* une réaction à ce nouvel ordre économique. Par
conséquent, il existe bien une possibilité de se sortir du modèle
productiviste et d'innovation capitaliste. Néanmoins ce modèle est
tellement dominant que, du point de vue des utilisateurs, c'est le
discours déterministe qui reste encore le plus audible sur le mode d'une
idolâtrie de la machine pour reprendre les mots de Simondon (Simondon
1969).

Une grande partie des usages publics d'Internet sont dépendants de
sociétés de services qui centralisent l'information et conditionnent les
usages de manière déloyale afin d'extraire des données personnelles des
utilisateurs. Longtemps dénoncé, ce système fait d'internet un
dispositif d'assujettissement, ainsi que le montre S. Zuboff, en
surveillant nos quotidiens, en captant nos expériences humaines et en
les instrumentalisant pour orienter nos comportements. C'est pourquoi la
critique de ce dispositif a fait un lien direct avec un autre
dispositif, le panoptique de J. Bentham dont s'inspire Michel Foucault.
En effet, pour ce dernier, tout dispositif, surtout parce qu'il a une
finalité stratégique, contient une relation de pouvoir (Foucault 1994b,
300). La surveillance continue, exercée tant par des entreprises
monopolistes que par des États, a éclaté au grand jour à travers le
scandale soulevé par les Révélations Snowden en 2014. Or, ce fut là le
point de convergence où l'on trouva les preuves indiscutables que les
dispositifs de surveillance avaient des objectifs de contrôle et donc
d'assujettissement. La clairvoyance des penseurs postmodernes pouvait
être convoquée : si Michel Foucault avait bien anticipé le caractère
transitoire d'une société disciplinaire où l'enfermement permet la
surveillance et donc l'équilibre du contrat social, l'avenir d'une telle
société, nous disait Gille Deleuze (Deleuze 1990), était de passer à un
mode ouvert où le contrôle s'exercerait de manière instantanée,
permanente et diffuse. À leur tour Michael Hardt et Antonio Negri (Negri
and Hardt 2004) montrent que le propre d'une société de contrôle
consiste à se servir des dispositifs les plus démocratiques, les plus
socialement intégrés pour exercer un contrôle cognitif et biologique.
Aujourd'hui ces formes de contrôles ne font aucun doute lorsqu'on
considère les scandales sur la sécurité des données personnelles et la
main-mise des courtiers de données sur l'analyse comportementale et le
modelage de nos comportements au marché ou les techniques de
surveillance de masse des populations par les États (et le marché
hautement lucratif de ces techniques).

Nous savons tout cela. Mais à ne voir qu'un assujettissement, on ne
pense plus une critique d'Internet qu'à travers ce prisme. Or, comme le
montre le Fediverse, il existe des stratégies qui, depuis longtemps,
imbriquent dans les dispositifs techniques des logiques en tout point
opposées au contrôle et au pouvoir. D'où viennent ces stratégies ?

On pourrait les rapprocher des pratiques que déploient les individus en
société, la consolidation des relations sociales à travers le *souci de
soi*. Non pas un réflexe individualiste, mais à l'instar de la manière
dont Michel Foucault en comprend le déploiement à partir de la cité
antique (Foucault 1984), c'est-à-dire les « arts de l'existence », un
travail sur soi que l'on fait à travers des pratiques permettant de se
transformer afin de vivre avec les autres, partager des règles communes,
un « connais-toi toi-même -- mais aussi prend soin de toi », comme le
conseille Socrate à Alcibiade, pour élever le citoyen (peut-être amené à
gouverner) vers l'exemplarité, l'esthétique de soi. Le souci de soi est
d'abord la capacité de prendre la mesure de l'autre, en ce sens c'est un
rapport éthique  : « le souci de soi est éthiquement premier, dans la
mesure où le rapport à soi est ontologiquement premier » (Foucault
1994a). C'est un principe d'accomplissement et en cela, le souci de soi,
individuellement et collectivement, est la tendance à mettre en œuvre
des pratiques de liberté. Le principe du pair à pair et son inclusion
dans des dispositifs techniques tels le Fediverse, élaborés
collectivement, de manière contributive, apparemment spontanée mais dans
l'effort réfléchi d'une stratégie altruiste, relève de ce souci de soi
en structurant les conditions communes de communication et de production
de contenus.

Ces pratiques de libertés se retrouvent dans l'œuvre de Michel de
Certeau (Certeau 1990). Ce dernier oppose au principe d'assujettissement
une lecture beaucoup plus fine du quotidien de l'homme ordinaire face à
un ordre organisateur de la technologie. M. de Certeau s'interroge
d'abord sur notre état de consommateur dans une économie de production
où nous serions docilement soumis à la manipulation par dispositifs
communicationnels interposés de marketing ou de télévision. Comment
comprendre que la masse de nos singularités puisse être comprise comme
un tout uniforme soumis au pouvoir des dispositifs sociotechniques de
l'économie ou de l'État ? Au contraire, dans les messages qu'il reçoit,
dans ses manières de consommer ou d'agir avec les techniques qu'on lui
vend ou qu'on lui impose, il y a toujours chez l'individu un moment de
réappropriation *tactique* qui est en soi un mode de production selon le
contexte, un réemploi qui est quelque chose de plus que le seul usage,
c'est-à-dire une manière de faire et une manière d'être qui sont à
nulles autres pareilles. Or, alors même que les critiques des GAFAM
montrent à quel point l'économie numérique influe sur nos quotidiens au
point de s'en emparer en pillant notre vie privée, il s'avère que
l'équation n'est peut-être pas aussi simple du point de vue individuel.
L'approche du quotidien comme celle de Michel de Certeau peut sans doute
édulcorer l'idée d'un assujettissement général aux dispositifs de
pouvoir.

Alan Westin, l'une des figures intellectuelles des débats publics sur la
vie privée au début des années 1970, a longuement travaillé avec de
grands instituts de sondage sur la manière dont la population américaine
perçoit les risques liés à l'usage des données personnelles dans
l'économie numérique. Il dirigea pas moins de 45 sondages entre 1979 et
2001. L'une de ses conclusions est que, si dans les années 1980 une
petite partie seulement des personnes se sentait concernée par la
question de la vie privée, les années 1990 connurent une multiplication
des pratiques et stratégies des consommateurs pour veiller sur cette
question. Il résume lors d'une intervention au Congrès en 2001 (Westin
2001) :

> Au fil des ans, les sondages que j'ai menés auprès de Harris et de
> Opinion Research Corporation m'ont permis de dresser un profil de
> trois segments du public américain. Premièrement, il y a ce que nous
> appelons les « fondamentalistes de la protection de la vie privée »,
> soit environ 25 % de la population. Il s'agit de personnes très
> préoccupées par la protection de la vie privée, qui rejettent
> généralement les avantages qui leur sont offerts par les entreprises
> ou qui sont sceptiques quant au besoin d'information du gouvernement,
> et qui veulent voir une loi pour contrôler la collecte et
> l'utilisation des renseignements personnels par les entreprises.
>
> À l'opposé, il y a ce que j'appelle les « non-concernés », qui étaient
> auparavant de 20 %, mais qui sont maintenant tombé à 12 %, et qui ne
> savent vraiment pas de quoi il s'agit. J'aime à penser que pour 5
> cents de rabais, ils vous donneront toutes les informations que vous
> voulez sur leur famille, leur style de vie, leurs plans de voyage,
> etc.
>
> Entre les deux, il y a ce que nous appelons les « pragmatiques de la
> protection de la vie privée », soit 63 %, ce qui représente environ
> 126 millions d'adultes américains et, essentiellement, ils passent par
> un processus très structuré lorsqu'ils se préoccupent de leur vie
> privée.

Cet état des lieux montre à quel point l'écrasante majorité des
personnes élabore des stratégies (et Michel de Certeau aurait parlé de
*tactiques*) en réponse aux dispositifs de surveillance, alors même que
la critique dominante des pratiques des entreprises et des États a
tendance à présupposer la passivité chronique de toute la population. Et
le problème ne cesse de s'aggraver. Dans un documentaire de 2020
intitulé *The Social Dilemma*, des « repentis » des grandes entreprises
telles Facebook, témoignent à l'encontre des pratiques de captation de
l'attention et d'extraction des données personnelles dont ils ont
participé à la mise en œuvre. Au long du documentaire, le présupposé
consiste à démontrer que seuls les experts des technologies numériques
qui ont effectivement travaillé dans ces entreprises seraient à même de
développer un discours légitime à leur propos. L'idée sous-jacente
serait qu'il n'y peut y avoir de critique des dispositifs techniques en
dehors de ces dispositifs. La société ne peut qu'être soumise à ce
déterminisme technologique dans la mesure où l'expertise se concentre à
l'intérieur du modèle économique qui crée la technologie.

On voit à quel point cette idée est décalée de la réalité car non
seulement les utilisateurs créent dans leurs quotidien des stratégies de
réappropriation mais, de plus, ces stratégies ont toujours existé,
depuis les *phreakers* jusqu'au détournement des services de catalogue
commerciaux Prodigy en salons de *chat* à la fin des années 1980 jusqu'à
la création de logiciels et services libres de réseaux sociaux. Des
espaces de résistance se sont formés et n'appartiennent pas uniquement
aux experts. Ces tactiques du quotidien sont diffuses et universelles.
La logique et l'éthique *hacker* ont su gagner les mentalités et la
culture.

On pourra néanmoins rétorquer qu'une analyse sociologique montrerait que
l'adoption des dispositifs alternatifs a toujours besoin d'une
expertise, et que par conséquent se développent là aussi des jeux de
pouvoir entre spécialistes et non spécialistes. C'est d'autant moins
vrai que de plus en plus de collectifs ouverts aux non-spécialistes
proposent une organisation contributive des dispositifs techniques,
c'est-à-dire la possibilité pour tous les utilisateurs d'accomplir leurs
actes de réappropriation dans des espaces communs et dans un engagement
éthique. Par exemple : ne pas exiger un service de courrier électronique
à la hauteur de ce que les grandes firmes proposent, mais en retour
avoir la possibilité d'utiliser un service de confiance, dont on connaît
les responsables de maintenance. Répondre à des offres de services
globaux et basées sur des logiques de profits en initialisant de
nouvelles chaînes de confiance dans un système de pair à pair, local et
contributif, telle est la réponse « populaire » à la défaite de
l'assujettissement, de l'aliénation ou du refuge spirituel.

En fin de compte, il ne suffit pas de s'approprier ou de transformer les
techniques (la perversion de l'éthique *hacker* par les chantres de la
Silicon Valley l'a bien montré). La déprolétarisation pensée par Bernard
Stiegler ne se suffit pas à elle-même : c'est dans un système
contributif qu'il faut créer des techniques qui n'ont pas de prise
aliénante sur l'homme. L'éthique du logiciel libre non plus ne se suffit
pas : il faut pour lui donner corps des collectifs qui mettent en œuvre
concrètement des systèmes de pair à pair producteurs d'organisation, de
pratiques mais aussi de biens et services. Ces collectifs *préfigurent*
des modèles organisationnels de demain, c'est-à-dire que les
apprentissages organisationnels, les procédures démocratiques qu'ils
mettent en œuvre tout autant que les services et les biens qu'ils
produisent s'élaborent collectivement et simultanément, sans obéir à des
schémas préexistants mais en s'adaptant aux contextes divers dans
lesquels ils se déploient, et afin de refléter une ou plusieurs idées de
la société future (voir à ce propos des mouvements préfiguratifs les
travaux de Carl Boggs, David Graeber et Mariane Maeckelbergh).

Pour terminer cette conclusion, une proposition déclarative. Il est
troublant de constater que dans la conclusion de leur article, M. Dulong
de Rosnay et de F. Musiani proposent, pour que les alternatives au
capitalisme numérique puissent aboutir, un triptyque composé d'un
environnement de partage (juridiquement sécurisé), de la recherche et la
diffusion des connaissances historiques et techniques. Nous établissons
un parallèle avec le triptyque de David Graeber et Andrej
Grubacic (Graeber and Grubacic 2004) à propos de l'avenir des mouvements
anarchistes au XXIe siècle qui, pour se déployer au mieux (et dans un
mouvement que nous pouvons qualifier de préfiguratif, là aussi) ont
besoin de la jonction entre militants, chercheurs et organisations
populaires. Les mouvements collectifs du futur Internet établiront ces
jonctions sur les déclinaisons suivantes des dispositifs
socio-techniques communicationnels :

-   indétermination du dispositif (logique d'ouverture et innovation
    collective permanente),
-   neutralité et communalité assurées par le design matériel et
    logiciel,
-   interopérabilité et compatibilité,
-   responsabilité juridique et indépendance institutionnelle,
-   pérennité des protocoles et contributions (RFC, copyleft)
-   décentralisation, déconcentration des ressources (matérielles et
    financières)
-   fédération des instances et gouvernementalité
    multiorganisationnelle,
-   inclusion de tous et refus des formes de pouvoirs politiques,
    religieux, de classe ou capitalistes,
-   opposition radicale à toute forme de surveillance à des fins de
    profits privés.

## Bibliographie


Abbate, Janet. 2012. "L'histoire de l'Internet Au Prisme Des STS." *Le
Temps Des médias* 18 (1): 170. <https://doi.org/10.3917/tdm.018.0170>.

—. 2018. "Code Switch: Alternative Visions of Computer Expertise
as Empowerment from the 1960s to the 2010s." *Technology and Culture* 59
(4): S134--59. <https://doi.org/10.1353/tech.2018.0152>.

Agamben, Giorgio. 2006. "Théorie des dispositifs." *Po&sie* 115 (1):
25--33. <https://www.cairn.info/revue-poesie-2006-1-page-25.htm>.

—. 2014. *Qu'est-ce qu'un dispositif ?* Paris: Rivages.

Agre, Philip E. 2002. "Cyberspace As American Culture." *Science as
Culture* 11 (2): 171--89. <https://doi.org/10.1080/09505430220137234>.

Akrich, Madeleine. 2006. "La Description Des Objets Techniques." In
*Sociologie de La Traduction : Textes Fondateurs*, edited by Michel
Callon and Bruno Latour, 159--78. Sciences Sociales. Paris: Presses des
Mines. <http://books.openedition.org/pressesmines/1197>.

Aron, Raymond. 1944. "L'avenir Des Religions séculières I." *La France
Libre* 8 (45): 210--17.

Atten, Michel. 2013. "Ce Que Les Bases de Données Font à La Vie Privée.
L'émergence d'un Problème Public Dans l'Amérique Des Années 1960."
*Réseaux* 178 (2): 21--53. <https://doi.org/10.3917/res.178.0021>.

Babbage, Charles. 1963. *On the Economy of Machinery and Manufactures
(1832)*. (1832) ed. New York: Kelley.

Baran, Paul. 1965. "Communications, Computers and People." In *AFIPS,
Fall Joint Comput. Conf.*, 27:45--49. Thompson Book Co.

Bauwens, Michel, and Vasilis Kostakis. 2017. *Manifeste Pour Une
véritable économie Collaborative. Vers Une Société Des Communs*. Paris:
Éditions Charles Léopold Mayer.
<https://www2.eclm.fr/livre/manifeste-pour-une-veritable-economie-collaborative/>.

Bolter, J. David. 1984. *Turing's Man: Western Culture in the Computer
Age*. Chapel Hill: The University of North Carolina Press.

Broca, Sébastien. 2018. *Utopie du logiciel libre*. Lyon, France:
Éditions le Passager clandestin.

Bødker, Susanne, Pelle Ehn, Dan Sjögren, and Yngve Sundblad. 2000.
"Cooperative Design --- Perspectives on 20 Years with 'the Scandinavian
IT Design Model'." In *Proceedings of NordiCHI 2000*. Stockholm.
<https://www.lri.fr/~mbl/ENS/DEA-IHM/papers/Utopia.pdf>.

Callon, Michel. 2006. "Sociologie de l'acteur réseau." In *Sociologie de
La Traduction : Textes Fondateurs*, edited by Madeleine Akrich and Bruno
Latour, 267--76. Sciences Sociales. Paris: Presses des Mines.
<http://books.openedition.org/pressesmines/1201>.

Certeau, Michel de. 1990. *L'invention du quotidien*. 2 vols. Paris,
France: Gallimard, DL 1990-1994.

Ceruzzi, Paul E. 1998. *A History of Modern Computing*. MIT Press.
Cambridge, Mass.

Charnes, Abraham, William W. Cooper, J. DeVoe, and D. B. Learner. 1968a.
"DEMON: A Management Model for Marketing New Products." *California
Management Review* 11 (1): 31--46.

—. 1968b. "DEMON, MARK II : An External Equation Approach to New
Product Marketing." *Management Science* 14 (8): 513--24.

Cohendet, Patrick, Frédéric Créplet, and Olivier Dupouët. 2003.
"Innovation Organisationnelle, Communautés de Pratique Et Communautés
épistémiques: Le Cas de Linux." *Revue Française de Gestion* 29 (146):
99--121. <https://doi.org/10.3166/rfg.146.99-121>.

Colstad, Ken, and Efrem Lipkin. 1976. "Community Memory: A Public
Information Network." *ACM SIGCAS Computers and Society* 6 (4): 6--7.
<https://doi.org/10.1145/958785.958788>.

Conner, Clifford D. 2011. *Histoire populaire des sciences*. Montreuil,
France: Éditions L'Échappée.

Cooley, Mike. 1980. "Computerization -- Taylor's Latest Disguise."
*Economic and Industrial Democracy* 1: 523--39.
<https://doi.org/10.1177%2F0143831X8014004>.

Debord, Guy. 1992. *La société du spectacle*. Paris, France: Gallimard.

Deleuze, Gilles. 1990. "Post-Scriptum Sur Les Sociétés de Contrôle." In
*Pourparlers 1972-1990*, 240--47. Paris: Éditions de minuit.

Doub, Bob. 2016. *Community Memory: Precedents in Social Media and
Movements*. Computer History Museum. Mountain View.

Draus, Franciszek. 1995. "Max Weber Et La Liberté." *Revue Européenne
Des Sciences Sociales* 33 (101): 123--43.
<https://www.jstor.org/stable/40370104>.

Dulong de Rosnay, Mélanie, and Francesca Musiani. 2020. "Alternatives
for the Internet: A Journey into Decentralised Network Architectures and
Information Commons." *tripleC: Communication, Capitalism & Critique*,
August, 622--29. <https://doi.org/10.31269/triplec.v18i2.1201>.

Ellul, Jacques. 1954. *La technique ou l'enjeu du siècle*. Paris,
France: Armand Colin.

—. 1964. *The Technological Society*. New York: Vintage Books.

Etzioni, Amitai, Kenneth Laudon, and Sara Lipson. 1975. "Participatory
Technology: The MINERVA Communications Tree." *Journal of Communication*
25 (2): 64--74. <https://doi.org/10.1111/j.1460-2466.1975.tb00581.x>.

Fabius, Laurent. 1985. *Brochure : Informatique Pour Tous*. Paris:
Éditions CNDP.

Feenberg, Andrew. 2014. "Vers une théorie critique de l'Internet."
*Tic&Société* 8 (1). <https://doi.org/10.4000/ticetsociete.1382>.

Feintrenie, Aurélie. 2003. *Chronique de la grève de 1974 au Crédit
lyonnais*. Librairie Droz.
<https://www.cairn.info/le-credit-lyonnais-1863-1986--9782600008075-page-153.htm>.

Foster, John Bellamy, and Robert W. McChesney. 2014. "Surveillance
Capitalism. Monopoly-Finance Capital, the Military-Industrial Complex,
and the Digital Age." *Monthly Review* 66 (July).
<https://monthlyreview.org/2014/07/01/surveillance-capitalism/>.

Foucault, Michel. 1984. *Histoire de La Sexualité. Tome 3 \-- Le Souci
de Soi*. Paris: Gallimard.

—. 1994a. "L'éthique Du Souci de Soi Comme Pratique de La
Liberté." In *Dits Et Écrits IV*, 708--29. Paris: Gallimard.

—. 1994b. "Le Jeu de Michel Foucault." In *Dits Et Écrits III*,
298--329. Paris: Gallimard.

Garfinkel, Simson. 1999. *Architects of the Information Society:
Thirty-Five Years of the Laboratory for Computer Science at Mit*. MIT
Press. Cambridge, Mass.

Graeber, David, and Andrej Grubacic. 2004. "L'Anarchisme, Ou Le
Mouvement révolutionnaire Du Vingt Et Unième Siècle." *The Anarchist
Library*.
<https://fr.theanarchistlibrary.org/library/david-graeber-andrej-grubacic-l-anarchisme-ou-le-mouvement-revolutionnaire-du-vingt-et-unieme-s>.

Haigh, Thomas. 2001. "Inventing Information Systems: The Systems Men and
the Computer, 1950--1968." *Business History Review* 75 (1): 15--61.
<https://doi.org/10.2307/3116556>.

Heidegger, Martin. 1958. "La Question de La Technique." In *Essais Et
Conférences*, 9--48. Paris: Gallimard.

Heilbroner, Robert L. 1967. "Do Machines Make History?" *Technology and
Culture* 8 (3): 335--45. <https://doi.org/10.2307/3101719>.

Husson, Édouard. 2018. "Peter Thiel, Les Libertariens Et La Silicon
Valley : Comme Une méchante Ombre Sur Les démocraties Occidentales."
*Atlantico*, January 7, 2018.

Kristal, Tali. 2013. "The Capitalist Machine: Computerization, Workers'
Power, and the Decline in Labor's Share Within U.S. Industries:"
*American Sociological Review*, May.
<https://doi.org/10.1177/0003122413481351>.

Latouche, Serge. 1991. *La Planète Des Naufragés. Essai Sur
l'après-développement*. Paris: La Découverte.

—. 1995. *La mégamachine. Raison Techno-Scientifique, Raison
économique Et Mythe Du Progrès*. Paris: La Découverte \-- Bibliothèque
du MAUSS.

Law, John. 1987. "On the Social Explanation of Technical Change: The
Case of the Portuguese Maritime Expansion." *Technology and Culture* 28
(2): 227--52. <https://doi.org/10.2307/3105566>.

Lundin, Per. 2011. "Designing Democracy: The UTOPIA-Project and the Role
of the Nordic Labor Movement in Technological Change During the 1970s
and 1980s." In *History of Nordic Computing 3*, edited by John
Impagliazzo, Per Lundin, and Benkt Wangler, 187--95. IFIP Advances in
Information and Communication Technology. Berlin, Heidelberg: Springer.
<https://link.springer.com/book/10.1007/978-3-642-23315-9>.

Maire, Edmond. 1977. "Préface." In *Les dégâts Du Progrès Les
Travailleurs Face Au Changement Technique*, edited by Jean-Louis
Missika, Jean-Philippe Faivret, and Dominique Wolton. Paris: Seuil.

Martin, James. 1978. *The Wired Society*. Englewood Cliffs, N.J:
Prentice-Hall.

Martin, James, and Adrian R. D. Norman. 1970. *The Computerized Society.
An Appraisal of the Impact of Computers on Society over the Next 15
Years*. Englewood Cliffs: Prentice-Hall.

Marx, Karl. 1896. *Misère de La Philosophie*. Paris: Giard et Brière.

Masutti, Christophe. 2020. *Affaires Privées. Aux Sources Du Capitalisme
de Surveillance*. Paris: C&F Éditions.

Missika, Jean-Louis, Jean-Philippe Faivret, and Dominique Wolton. 1977.
*Les dégâts Du Progrès Les Travailleurs Face Au Changement Technique*.
Paris: Seuil.

Morozov, Evgeny. 2014. *Pour Tout résoudre Cliquez Ici : L'aberration Du
Solutionnisme Technologique*. Paris: Éditions FYP.

Moussy, Jean-Pierre. 1988. "L'emploi au cœur de la mutation bancaire."
*Revue d'économie financière* 7 (3): 49--67.
<https://doi.org/10.3406/ecofi.1988.1547>.

Mumford, Lewis. 1950. *Technique Et Civilisation*. Paris: Seuil.

—. 1973. *Le mythe de la machine*. Translated by Léo Dilé. 2
vols. Paris, France: Fayard.

—. 2014. "L'héritage de l'homme." Translated by Annie Gouilleux.
*Notes & Morceaux Choisis \-- Bulletin Critique Des Sciences, Des
Technologies Et de La Société Industrielle* 11.

Narcy, Michel. 1986. "Au Terme de Deux Enquêtes." *Informatique Et
pédagogie \-- Numéro Spécial de l'EPI* 42: 53--60.
<https://www.epi.asso.fr/revue/histo/h85-gredip.htm>.

Negri, Antonio, and Michael Hardt. 2004. *Empire*. Paris: 10/18.

Nelsen, Arvid. 2016. "Concern for the 'Disadvantaged': ACM's Role in
Training and Education for Communities of Color (1958--1975)." In
*Communities of Computing: Computer Science and Society in the ACM*, by
Thomas J. Misa, 229--58. New York: ACM Books.

Panzieri, Raniero. 1968. *Quaderni Rossi. Luttes Ouvrières Et
Capitalisme d'aujourd'hui*. Paris: Maspéro.

Pestre, Dominique. 2006. *Introduction Aux Science Studies*. Paris: La
Découverte.

Pfaffenberger, Bryan. 1988. "The Social Meaning of The Personnal
Computer : Or Why the Personal Computer Revolution Was Not a
Revolution." *Anthropological Quarterly* 61 (1): 39--47.
<https://doi.org/doi:10.2307/3317870>.

Rawls, John. 1997. *Théorie de la justice*. Translated by Catherine
Audard. Paris, France: Éditions du Seuil.

Rosenbrock, Howard Harry. 1977. "The Future of Control." *Automatica
(1975)* 13: 389--92. <https://doi.org/10.1016/0005-1098(77)90022-X>.

Rossman, Michael. 1976. "Implications of Community Memory." *ACM SIGCAS
Computers and Society* 6 (4): 7--10.
<https://doi.org/10.1145/958785.958789>.

Schmitt, Carl. 1988. *Théologie politique*. Translated by Jean-Louis
Schlegel. Paris, France: Gallimard.

Simondon, Gilbert. 1969. *Du Mode d'existence Des Objets Techniques*.
Paris: Aubier-Montaigne.

Smith, Merritt Roe. 1994. "Technological Determinism in American
Culture." In *Does Technology Drive History?*, edited by Merritt Roe
Smith and Leo Marx, 1--35. Cambridge: MIT Press.

Smith, Steve. 1989. "Information Technology in Banks: Taylorization or
Human-Centered Systems?" In *Computers in the Human Context*, edited by
Tom Forester, 377--90. Cambridge: MIT Press.

Solove, Daniel J. 2002. "Conceptualizing Privacy." *California Law
Review* 90 (4): 1132--40. <https://ssrn.com/abstract=313103>.

Sundblad, Yngve. 2011. "UTOPIA: Participatory Design from Scandinavia to
the World." In *History of Nordic Computing 3*, edited by John
Impagliazzo, Per Lundin, and Benkt Wangler, 176--86. IFIP Advances in
Information and Communication Technology. Berlin, Heidelberg: Springer.
<https://link.springer.com/book/10.1007/978-3-642-23315-9>.

Thoreau, Henri David. 1922. *Walden Ou La Vie Dans Les Bois*. Translated
by Louis Fabulet. Paris: La Nouvelle Revue Française.

Turner, Fred. 2012. *Aux Sources de l'utopie Numérique : De La Contre
Culture à La Cyberculture. Stewart Brand, Un Homme d'influence*. Caen:
C&F Editions.

Turoff, Murray, and Starr Roxane Hiltz. 1994. *The Network Nation: Human
Communication via Computer*. Cambridge: MIT Press.

Vinck, Dominique. 2012. "Manières de Penser l'innovation." In *Les
Masques de La Convergence. Enquêtes Sur Sciences, Industries Et
Aménagements*, by Bernard Miège and Dominique Vinck, 125--48. Paris:
Éditions des Archives contemporaines.

Westin, Alan F. 1967. *Privacy and Freedom*. New York: Atheneum.

—. 2001. "Opinion Surveys: What Consumers Have to Say About
Information Privacy." In *Hearing Before the Subcommittee on Commerce,
Trade and Consumer Protection of the Committee on Energy and Commerce,
107th Congress*.
<https://www.gpo.gov/fdsys/pkg/CHRG-107hhrg72825/html/CHRG-107hhrg72825.htm>.

Wyatt, Sally. 2008. "Technological Determinism Is Dead; Long Live
Technological Determinism." In *The Handbook of Science and Technology
Studies. Third Edition*, edited by Edward J. Hackett, Olga Amsterdamska,
Michael Lynch, and Judy Wajcmann, 165--80. Cambridge: MIT Press.

Zuboff, Shoshana. 1988. *In The Age Of The Smart Machine: The Future Of
Work And Power*. New York: Basic Books.

—. 2019. *The Age of Surveillance Capitalism: The Fight for a
Human Future at the New Frontier of Power*. New York: Public Affairs.


## Notes


[^1]: La citation : « Le moulin à bras vous donnera la société avec le suzerain ; le moulin à vapeur, la société avec le capitaliste industriel ».

[^2]: Publicité Apple parue dans le magazine *Byte* en décembre 1977, page 2. La reproduction est disponible sur *commons.wikimedia.org*. [URL](https://commons.wikimedia.org/wiki/File:Apple_II_advertisement_Dec_1977_page_2.jpg).

[^3]: Voir la définition de *hacker* dans le Jargon File. [URL](http://www.catb.org/jargon/html/H/hacker.html)

[^4]: Trades Union Congress Report 1956, [TUC history online](http://www.unionhistory.info/reports/), p. 518-519.

[^5]: Trades Union Congress Report 1965, [TUC history online](http://www.unionhistory.info/reports/), p. 136.

[^6]: United States National Commission on Technology, Automation, and Economic, [*Technology and the American Economy: Report*](https://catalog.hathitrust.org/Record/007424268), vol. III, p. 57.

[^7]: Écouter l'émission La Fabrique de l'Histoire (France Culture) : Quand les banquiers criaient « À bas les profits ! », 13 mars 2012.
    [URL](https://www.franceculture.fr/emissions/la-fabrique-de-l-histoire/quand-les-banquiers-criaient-bas-les-profits).

[^8]: Karl Marx, *Le Capital*, livre I, ch. 4 --- Le caractère fétiche de la marchandise et son secret.

[^9]: Pour citer Serge Latouche: « la technique n'est pas un instrument au service de la culture, elle est la culture ou son tenant lieu ».
