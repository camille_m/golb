---
title: "Les nouveaux Léviathans IV. La surveillance qui vient"
date: 2018-01-06
author: "Christophe Masutti"
image: "/images/postimages/ai.png"
description: "Poursuite du projet Leviathan"
tags: ["Libres propos", "Histoire", "Libertés", "Capitalisme de surveillance"]
categories:
- Libres propos
---


Je continue la série des Léviathans commencée en 2016 sur le Framablog. Pour ce nouveaux numéro,  je propose de voir dans quelle mesure le modèle économique a développé son besoin vital de la captation des données relatives à la vie privée. De fait, nous vivons dans le même scénario dystopique depuis une cinquantaine d'années. Nous verrons comment les critiques de l'économie de la surveillance sont redondantes depuis tout ce temps et que, au-delà des craintes, le temps est à l'action d'urgence.

Petit extrait :

> L'incertitude au sujet des dérives du capitalisme de surveillance n'existe pas. Personne ne peut affirmer aujourd'hui qu'avec l'avènement des <em>big data</em> dans les stratégies économiques, on pouvait ignorer que leur usage déloyal était non seulement possible mais aussi que c'est bien cette direction qui fut choisie d'emblée dans l'intérêt des monopoles et en vertu de la centralisation des informations et des capitaux. Depuis les années 1970, plusieurs concepts ont cherché à exprimer la même chose. Pour n'en citer que quelques-uns : computocracie (M. Warner et M. Stone, 1970), société du dossier (Arthur R. Miller, 1971), surveillance de masse (J. Rule, 1973), dataveillance (R. Clarke, 1988), capitalisme de surveillance (Zuboff, 2015)… tous cherchent à démontrer que la surveillance des comportements par l'usage des données personnelles implique en retour la recherche collective de points de rupture avec le modèle économique et de gouvernance qui s'impose de manière déloyale. Cette recherche peut s'exprimer par le besoin d'une régulation démocratiquement décidée et avec des outils juridiques. Elle peut s'exprimer aussi autrement, de manière violente ou pacifiste, militante et/ou contre-culturelle.


Tous les articles rassemblés en un fichier ``.epub`` [sur mon dépot Gitlab](https://framagit.org/Framatophe/articlesenvrac/blob/master/Leviathans/lev.epub)

**La série d'articles sur le framablog :**

- [Les nouveaux Léviathans Ia](https://framablog.org/?p=6617)
- [Les nouveaux Léviathans Ib](https://framablog.org/?p=6626)
- [Les nouveaux Léviathans IIa](https://framablog.org/?p=6629)
- [Les nouveaux Léviathans IIb](https://framablog.org/?p=6631)
- [Les nouveaux Léviathans III](https://framablog.org/?p=9897)
- [Les nouveaux Léviathans IV](https://framablog.org/?p=11179)
- [Les anciens Léviathans I](https://framablog.org/?p=6637)
- [Les anciens Léviathans II](https://framablog.org/?p=6661)
=
