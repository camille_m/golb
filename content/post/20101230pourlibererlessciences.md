---
title: "Pour libérer les sciences"
date: 2010-12-30
image: "/images/postimages/say.png"
author: "Christophe Masutti"
tags: ["Libres propos", "Sciences", "Communs", "Droit d'auteur", "Édition"]
description: "Comment libérer au mieux les connaissances scientifiques ?"
categories:
- Libres propos
---


L'objectif de ce texte est de faire valoir l'intérêt d'une diffusion décentralisée et libre des connaissances scientifiques. En partant de l'idée selon laquelle l'information scientifique n'a d'autre but que d'être diffusée au plus grand nombre et sans entraves, je montrerai les limites du système classique de publication à l'ère du format numérique, ainsi que les insuffisances des systèmes d'archives «&nbsp;ouvertes&nbsp;». J'opposerai le principe de la priorité de la diffusion et à l'aide de quelques exemples, j'aborderai la manière dont les licences libres [Creative Commons](http://fr.creativecommons.org/) permettent de sortir de l'impasse du modèle dominant.


![Contrat Creative Commons](http://i.creativecommons.org/l/by-sa/2.0/fr/88x31.png)

«&nbsp;Pour libérer les sciences&nbsp;» by [Christophe Masutti](http://christophe.masutti.name) est mis à disposition selon les termes de la [licence Creative Commons Paternité - Partage des Conditions Initiales à l'Identique 2.0 France](http://creativecommons.org/licenses/by-sa/2.0/fr/).

- [Pour libérer les sciences (PDF et sources)](https://statium.link/bazaar/pourlibererlessciences/)

