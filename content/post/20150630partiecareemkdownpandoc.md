---
title: "Partie carrée avec Pandoc, Markdown, HTML et LaTeX"
date: 2015-06-30
lastmod: 2018-03-28
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
tags: ["Logiciel libre", "LaTeX", "Markdown", "Bidouillage", "Édition"]
description: "Écrire en markdown et choisir son format de sortie, c'est une une mini chaîne éditoriale"
categories:
- Logiciel libre
---

Pour ceux qui en douteraient encore, les blagues graveleuses à propos de LaTeX n'ont pas cours ici. Il s'agit dans ce billet de proposer une mini chaîne éditoriale qui vous permettra de composer rapidement des documents en plusieurs formats tout en les écrivant avec le minimum de contraintes. Qu'il s'agisse de n'importe quel logiciel de traitement de texte ou d'un simple éditeur dans lequel on formate directement le document source, on passe bien trop de temps à cliquer ou entrer des commandes, toujours au détriment du contenu. Avoir une solution automatisée ne résoudra jamais toutes les situations, mais notre objectif est de tâcher de séparer le plus possible l'entrée de contenu et la mise en page. Pour cela nous allons utiliser deux outils, à savoir le formatage [Markdown](https://fr.wikipedia.org/wiki/Markdown) et le logiciel de conversion [Pandoc](http://pandoc.org/).

## Prérequis

Pour accomplir toutes les opérations mentionnées ci-dessous, quelques conditions doivent être réunies :


- Pour la plus grande partie de ce billet, il faut avoir compris (juste «&nbsp;compris&nbsp;») ce qu'est un [langage de balisage](https://fr.wikipedia.org/wiki/Langage_de_balisage) comme le HTML ou le LaTeX, et savoir se servir d'un terminal (entrer des lignes de commandes, au demeurant fort simples à comprendre) ;
- Avoir Pandoc installé sur sa machine ;
- Avoir un bon éditeur de texte, si possible capable d'afficher une coloration syntaxique propre au <a href="https://fr.wikipedia.org/wiki/Markdown">Markdown</a> (voir capture d'écran) ;
- Pour obtenir un document LaTeX et produire un PDF, il faut avoir une distribution LaTeX installée, comme TeXlive.

![Éditeur de texte utilisant du markdown](/home/masuttic/Téléchargements/pourblog/images/screen_editeurmd.png)

## Objectifs

À quel genre de situation sommes-nous censés faire face ?

Vous souhaitez rédiger rapidement un document, cependant, avec votre logiciel de traitement de texte, vous supportez de moins en moins d'avoir à cliquer sans cesse pour formater des styles de titres et de paragraphes et vous aimeriez pouvoir rédiger avec une interface d'éditeur de texte la moins chargée possible.

Il vous est peut-être arrivé de rédiger un document en LaTeX et, malgré les filtres disponibles, avoir toutes les peines du monde à le transformer en HTML parce qu'en fin de compte vous souhaitiez l'afficher en tant que page web ou réaliser un e-pub.

Vous avez essayé plusieurs systèmes de conversion automatisés, mais jamais vous n'avez fini par obtenir de document proprement formaté.

D'autres situations ont pu se produire et vous allez certainement trouver des failles à la méthode proposée ici. En revanche, en plus de la méthode, je propose un principe auquel on pense pas toujours : *si vous voulez transformer un document, rédigez-le d'abord au format le plus simple*. C'est depuis cette source qu'il faut tenter les conversions et éviter de le faire depuis un format complexe. Quoi que vous fassiez, pour être parfait, votre document final devra sans doute faire l'objet de retouches, mais elles seront un moindre mal car la majeure partie du document sera alors traitée proprement.

Qu'est-ce que j'entends par «&nbsp;proprement&nbsp;»&nbsp;? C'est vraiment un conseil d'hygiène&nbsp;: certaines solutions de formatage ou de conversion utilisent des solutions plus ou moins adaptées à la complexité du code de départ. Cela se traduit souvent par l'adjonction de styles prédéfinis par le logiciel de conversion. Ainsi, si vous transformez un document LaTeX en HTML, la malléabilité du document final laissera toujours à désirer, en particulier si vous voulez adapter votre propre feuille de style, à moins de la définir auparavant ce qui nécessite en retour beaucoup d'efforts. Faire les choses simplement dès le départ vous permettra de complexifier par la suite à votre propre initiative, mais en aucun cas vous ne serez contraints d'adapter votre projet au format&nbsp;: c'est l'inverse qui doit se produire.

Les objectifs seront successivement&nbsp;:


- de produire un document au format [Markdown](https://fr.wikipedia.org/wiki/Markdown) adapté à [Pandoc](http://pandoc.org/), 
- le transformer en utilisant Pandoc, de manière à produire un document au format .odt (ou .docx), un document au format HTML, 
- et un document .pdf (re)formaté [LaTeX](https://fr.wikipedia.org/wiki/LaTeX) et compilé avec [pdfTeX](https://fr.wikipedia.org/wiki/PdfTeX) ou [Xe(La)TeX](https://fr.wikipedia.org/wiki/XeTeX).


## Créer un document au format Markdown

Le format Markdown est tellement simple qu'il n'y a pas besoin de créer des macros pour pouvoir l'utiliser. En d'autres termes, tout ce dont vous avez besoin pour écrire dans ce format est d'un éditeur de texte et d'un peu de mémoire. Si, en plus, vous utilisez votre éditeur en plein écran, sans autre affichage que votre curseur pour entrer des caractères, vous pourrez vous concentrer pleinement sur votre contenu et écrire sans vous soucier d'autre chose.

L'entrée des éléments de formatage en Markdown s'effectue au long de la frappe. Ainsi, pour un titre de niveau 1, vous n'avez qu'à entrer un croisillon (``# Titre 1``) en début de ligne ; pour un titre de niveau 2, deux croisillons feront l'affaire (``## Titre 2``) ; pour mettre en valeur une partie de votre texte, par exemple en italique, il suffit d'entrer le caractère tiret bas (``_italique_``) ou une astérisque (``*italique*``) au début et à la fin de la partie ; etc.

Pour reconnaître votre document en markdown, vous pouvez lui ajouter l'extension ``.markdown``, ou ``.md``, par exemple.

Par ailleurs, il existe certains éditeurs de texte qui vous proposent d'afficher directement le rendu HTML de votre document. Ils utilisent parfois une double fenêtre. Voici trois éditeurs sympathiques : [Ghostwriter](https://wereturtle.github.io/ghostwriter/), [ReText](http://sourceforge.net/p/retext/home/ReText/), [StackEdit](https://stackedit.io/) (fonctionne dans votre navigateur). Ces éditeurs proposent aussi des filtres d'export vers ``odt``, ``HTML`` et ``PDF``.

La liste des commandes de formatage Markdown est très courte. Vous trouverez sur [le site de son créateur](http://daringfireball.net/projects/markdown/), John Gruber, les éléments de syntaxe utilisables dans ce format. En revanche, comme nous allons le voir, Pandoc propose quelques fonctionnalités spécifiques, aussi il sera préférable de se référer à [cette partie du manuel de Pandoc](http://pandoc.org/README.html#pandocs-markdown). En réalité, il suffit de quelques minutes de pratique pour en maîtriser l'essentiel. La seule difficulté du Markdown, c'est qu'il n'est pas (encore) standardisé, si bien que tout le monde s'accorde à reconnaître l'essentiel mais de petites variations peuvent apparaître ici et là.

Dans notre cas, il y a une manière d'utiliser le Markdown [à la sauce Pandoc](http://pandoc.org/README.html#Pandocs-markdown). En voici quelques exemples :

### Les notes de bas de page

Le Markdown est si simple que des éléments pourtant essentiels à la rédaction en sont absents. C'est le cas de la gestion des notes de bas de page. Pandoc permet de gérer cela de manière très simple : il suffit d'entrer le numéro de la note à l'endroit de l'appel de note, puis, en fin de texte, rédiger la note en y faisant référence.

<pre>Ceci est un appel de note[^1].

(en fin de texte)

[^1]: Ceci est le contenu de la note de bas de page.
</pre>

La question des notes de bas de page est loin d'être triviale. Si vous devez convertir votre document à la fois en HTML et en LaTeX vous constaterez bien vite que les méthodes dont ces deux langages balisés gèrent les notes de bas de page n'ont quasiment rien à voir entre elles, si bien que convertir de l'un à l'autre un document contenant des notes de bas de page est souvent un vrai casse-tête, tout particulièrement si vous avez beaucoup de notes.

### Les métadonnées

Les informations du document comme le titre, la date et l'auteur, pourront être entrées en début de document en les introduisant simplement avec le signe ``%``.

<pre>% Titre
% Auteur
% Date
</pre>

Là encore, c'est une fonctionnalité qui est proposée : vous verrez que, lorsque nous utiliseront un modèle de conversion, nous pourrons utiliser ces données rendues exploitables par Pandoc.

### La table des matières

Pour créer une table des matières… hé bien, non, il n'y a rien à faire ! Il suffira, lors de la conversion, de demander gentiment à Pandoc de se charger de créer une table des matières selon le format de sortie désiré.

### Autres aspects du Pandoc's Markdown

Vous [trouverez dans le manuel](http://pandoc.org/README.html#pandocs-markdown) toutes les possibilités d'interprétation du Markdown par Pandoc, par exemple pour faire des tableaux, insérer des mathématiques, faire des listes spéciales, etc. Sachez toutefois que si vous voulez que votre document reste interprétable par d'autres convertisseurs que Pandoc, il faudra aussi faire un effort de simplification. L'autre solution est que, quoiqu'il arrive, selon le format de conversion visé, vous pouvez insérer directement des éléments de code dans votre document Markdown. Ainsi, si vous voulez transformer vers LaTeX, vous pouvez insérer directement du code LaTeX qui sera interprété tout à fait normalement. Idem pour le HTML. Et même mieux : vous pouvez insérer du code HTML dans du Markdown et interpréter le tout vers LaTeX !

## À propos de Pandoc

Dans la suite de ce billet, je vais vous montrer comment utiliser certaines commandes de Pandoc. Mais les possibilités de ce dernier sont très vastes. Il est possible, par exemple, de lui faire traiter de la bibliographie en travaillant avec LaTeX et BibLaTeX, ou encore rédiger un script qui permettra d'automatiser certaines tâches avec Pandoc. Faisons simple pour commencer : Pandoc propose des options, utilisons-les.

Par ailleurs, Pandoc permet de transformer de multiples formats. Nous nous focaliserons ici sur le Markdown, mais sachez que les applications de Pandoc excèdent largement ce cadre.

### Comment utiliser Pandoc ?

Pandoc s'utilise en ligne de commande. Non, attendez ! ne partez pas tout de suite. D'accord, il s'agit d'ouvrir un terminal mais il ne s'agit pas non plus d'incanter des formules réservées à un caste de moines guerriers initiés. Il faut passer outre vos appréhensions et vous allez voir, c'est vraiment simple.

Première chose à faire : ouvrir un terminal et vous rendre dans le dossier où vous avez stocké votre document en Markdown. Dans les commandes que je citerai plus bas, tout se passera dans ce dossier : la production de sortie sera enregistré à cet emplacement (mais vous pouvez toujours en choisir un autre, il suffit pour cela de spécifier à chaque fois le chemin).

Les commandes se rédigent toujours de manière logique. On « dit à la machine », quelque chose comme « Tu vas utiliser Pandoc sur ce document au format Bidule pour produire ce nouveau document au format Machin, en utilisant telle et telle option ». Les options sont toujours introduites avec un double tiret (``--``) ou leur diminutif par un tiret simple (``-``).

Voici un exemple simple que je commente :

<pre>pandoc mondocument.md -o masortie.html</pre>

Pandoc est lancé, il traitera un document au format markdown pour produire (``-o``, ou bien ``-output``) un autre document au format HTML. À chaque fois j'aurais pu préciser un chemin différent pour chaque document, ainsi pour traiter un document d'un dossier à l'autre, on peut écrire :

<pre>pandoc chemin/redaction/mondocument.md -o chemin/production/masortie.html</pre>

Cerise sur le gâteau, si vous  avez plusieurs document markdown que vous désirez convertir pour produire un document unique dans un autre format, il suffit de les entrer dans l'ordre&nbsp;:

<pre>pandoc doc1.md doc2.md doc3.md -o docfinal.odt</pre>

Ceci est d'autant plus pratique si vous rédigez un livre, par exemple, avec un fichier par chapitre.

## Passons à la pratique

Vous avez donc rédigé un document au format Markdown. Qu'allez-vous en faire&nbsp;?

### Tel quel

Vous pouvez tout simplement utiliser votre document tel quel. Le markdown, c'est d'abord du texte. Par ailleurs, ce format a été conçu spécifiquement au départ pour envoyer des courriels en mode texte sans avoir à perdre du temps dans une mise en page dont le résultat est différent selon le paramétrage du client de courriel du destinataire.

De plus, vos documents en Markdown seront lisibles par tout éditeur de texte dans l'avenir. Vous pouvez être certain de leur pérennité. Si vous ouvrez vos documents Markdown avec un éditeur permettant le rendu HTML, vous avez en plus de cela une mise en page toute prête si l'aspect «&nbsp;texte&nbsp;» ne vous convient pas.

### Vers un logiciel de traitement de texte

Si vous avez besoin d'éditer votre document avec un logiciel de traitement de texte comme LibreOffice Writer ou MSWord, en particulier si vous avez des modèles de mise en page à y appliquer, la syntaxe de la transformation est la plus simple :

<pre>pandoc source.md -o produitfinal.odt</pre>

ou bien

<pre>pandoc source.md -o produitfinal.doc</pre>

Le document produit sera mis en page avec les styles par défaut de votre logiciel de traitement de texte, il suffit ensuite d'y appliquer vos propres styles si besoin.

### Vers le HTML

S'agissant d'un langage balisé, la conversion vers le HTML permet d'intégrer des exigences de mise en page directement au moment de le traiter avec Pandoc. Ainsi, vous pouvez préparer une feuille de style ``.css`` en vue de l'intégrer (ou pas) directement dans le document de sortie, ce dernier pouvant de même obéir à un modèle (*template*) préparé à l'avance.

<pre>pandoc -s -S --toc --template=modele.html -H monstyle.css --number-section source.md -o produitfinal.html</pre>

Cette ligne de commande permet de faire ces opérations (vous pouvez ôter celles qui nous intéressent pas) :


- Produire un document « standalone » (``-s`` ou ``--standalone``), c'est-à-dire qu'il intégrera toutes les informations pour qu'il s'affiche dans le navigateur sans faire appel à d'autres fichiers.
- Le document aura une typographie acceptable, grâce à l'option ``-S`` (ou ``--smart``). Pandoc convertira par exemple les doubles tirets (``--``) par des demi-cadratins ou les triples tirets (``---``) par des cadratins
- Il intégrera une table des matières au début, grâce à l'option ``--toc`` (ou ``--table-of-contents``), notez que la profondeur de la TOC peut se régler avec l'option ``--toc-depth=nombre``.
- l'option ``--number-section`` permet de numéroter les sections, sous-sections, etc.
- Le template est défini à l'avance, on le renseigne dans la ligne de commande avec ``--template=fichier``.
- Dans son en-tête (header, ``-H`` ou en version longue ``--include-in-header=fichier``), il intégrera la feuille de style que vous avez créée à cet effet, et que l'on renseigne en donnant le nom du fichier (mais on pourrait aussi bien intégrer le renvoi à la feuille de style dans le template lui-même ; ici vous avez la possibilité de choisir parmi plusieurs feuilles de style si besoin).


Le document final affichera, en plus du titre, de la date et de l'auteur, une table des matières et toutes les informations de mise en page que vous avez intégrées à votre document Markdown.

Vous noterez la gestion des notes de bas de page, par exemple, et la manière dont la table des matières s'affiche. Si vous aviez dû entrer toutes les références internes de la page, cela vous aurait certainement pris beaucoup plus de temps.

### Vers LaTeX (pdf)

C'est vers ce format qu'on s'amuse le plus, à mon humble avis. Toutes les options sont applicables mais nous pouvons aller beaucoup plus loin. En effet, nous cherchons à automatiser la création du format de sortie le plus courant de LaTeX, à savoir un beau document PDF. Tout l'intérêt de la démarche consiste donc à créer un style comme nous l'avons fait pour le rendu HTML, mais beaucoup plus fin puisque nous pourrons paramétrer une mise en page destinée à l'impression. Il s'agira d'un document LaTeX contenant quelques éléments conditionnels, les packages que vous souhaitez utiliser et la configuration de ceux-ci. En somme, c'est de toute l'en-tête du document que nous avons besoin. Si bien qu'une fois celle-ci définie, il vous restera à l'avenir à rédiger vos documents en Markdown sans passer par LaTeX (ou alors pour quelques retouches si besoin), tout en utilisant les options de Pandoc. Ce qui produit finalement un double outil très puissant.

Dans l'exemple donné, il s'agit de créer un document au format A4, présenté comme un rapport avec un en-tête, etc. Mais ce n'est qu'un exemple : à vous de modifier le modèle pour le conformer à vos besoins.

Par défaut, c'est-à-dire si vous ne spécifiez aucun style, la sortie PDF se fera sous la simple classe ``[article]`` sans style particulier. Et n'oubliez pas : nous cherchons à produire directement un PDF en utilisant le moteur LaTeX, rien ne vous empêche de transformer votre document en LaTeX pour le travailler ensuite.

Pour notre exemple, la syntaxe est la suivante :

<pre>pandoc source.md -o produitfinal.pdf --toc --template=monstyle.latex --number-section</pre>


- l'option ``--toc`` permet l'insertion d'une table des matières
- l'option ``--number-section`` permet de numéroter les sections, sous-sections, etc.
- l'option ``--template=xxx`` permet de spécifier le style, c'est-à-dire l'en-tête du document LaTeX qui sera compilé pour produire le pdf final, cet en-tête se rédige dans un document LaTeX auquel on fait appel lors de la compilation.


## Mais enfin, comment se construisent ces templates pour Pandoc?

Il s'agit en réalité de modèles de composition qui intègrent toute une série de variables. Par exemple, pour la date à intégrer dans un document LaTeX, on peut utiliser ``$if(date)$\date{$date$}$endif``, c'est à dire «&nbsp;si la date est renseignée (dans les métadonnées), alors tu utilises cette valeur dans la commande indiquée&nbsp;».

On peut construire ses propres modèles, dans la mesure où les valeurs peuvent être utilisées de cette manière. Mieux encore, certaines données peuvent aussi être entrées dans un fichier en YAML, mais cela fera l'un des objets d'un futur billet.

Pour commencer à bâtir des modèles, vous pouvez vous reporter à <a href="http://pandoc.org/README.html#templates">cette partie de la documentation</a> et surtout regarder comment sont bâtis les <a href="https://github.com/jgm/pandoc-templates">templates par défaut</a> de Pandoc, de manière à vous en inspirer.

## La bibliographie (et production finale)

La bibliographie  peut être traitée différemment selon votre format de sortie. Quoiqu'il en soit, pour commencer, vous devez produire un fichier de bibliographie au format bibtex (``.bib``).

Dans les commandes pandoc vous devrez renseigner ce fichier avec

<pre>--bibliography=nomdufichier.bib</pre>

Et dans votre document Markdown, vous passez les références ainsi, avec leurs identifiants&nbsp;:

<pre>
Comme le dit Dupont [@dupont1950, p. 15].
Si nous marchons nous ne dormons pas [@Martin1985; @Dupont1950].
</pre>

Si vous produisez un document LaTeX, le mieux sera d'utiliser l'option  ``--biblatex`` ce qui aura pour effet de générer la commande biblatex pour traiter la bibliographie. Par exemple&nbsp;:

<pre>pandoc --bibliography=nomdufichier.bib --latex-engine=xelatex mondoc.md -o sortie.tex</pre>

Mais l'objet de cet article est de vous aider à produire des documents de manière automatisée. Voici comment procéder pour produire un document PDF ou HTML disposant d'une bibliographie (et donc d'une liste bibliographique).

Il faut pour cela utiliser l'interpréteur ``citeproc`` (à installer en plus de pandoc) et un fichier CSL (Citation Style Language). Le premier permet d'interpréter les fichiers CSL qui sont en fait des styles de bibliographie. Vous pouvez trouver [un répertoire](https://www.zotero.org/styles) de ces fichiers CSL (prêts à l'emploi ou que vous [pouvez modifier](http://editor.citationstyles.org/about/)) dans le dépôt Zotero, un logiciel de gestion bibliographique.

Une fois votre fichier CSL prêt, vous pouvez produire&nbsp;:

<pre>
 pandoc --filter pandoc-citeproc --bibliography=mabiblio.bib --csl=monstylebiblio.csl -s --toc --number-section --template template.html --css template.css masource.md -o sortie.html
</pre>

Ce qui permettra de produire un document HTML (standalone), avec une table des matières, des sections numérotées, basé sur un modèle HTML et un fichier de style CSS, une bibliographie traitée dans le texte et présente à la fin selon le fichier de style bibliographique choisi.

Astuce : pour introduire la bibliographie, laissez à la fin de votre document markdown un titre comme ``# Bibliographie``.

<pre>
pandoc --filter pandoc-citeproc --bibliography=mabiblio.bib --csl=monstylebiblio.csl -s --latex-engine=xelatex --toc --number-section -colorlinks masource.md -o sortie.pdf
</pre>

Ce qui permettra de produire un document PDF, traité par XeLaTeX, avec une table des matières, des sections numérotées, des liens colorés, une bibliographie traitée dans le texte et présente à la fin selon le fichier de style bibliographique choisi.

## Production finale

Si vous n'avez pas de bibliographie à gérer, vous pouvez vous lancer dans la production de documents différents à partir d'un seul fichier Markdown.

Produisez un fichier .ODT (ou Docx) puis appliquez vos styles avec LibreOffice (ou MSWord). C'est la solution la plus simple : en fait pandoc produit des styles que vous pouvez modifier ou carrément écraser avec un modèle (LibreOffice ou Word) par la suite.

<pre>pandoc masource.md -o sortie.odt</pre>

Produisez un fichier .tex prêt à l'emploi.

<pre>pandoc masource.md -o sortie.tex</pre>

Produisez un document HTML avec un modèle et un fichier de style, une table des matières, des sections numérotées.

<pre>
 pandoc -s --toc --number-section --template template.html --css template.css masource.md -o sortie.html
</pre>

Produisez un beau PDF (travaillé avec LaTeX), avec un modèle préparé à l'avance (et dans lequel vous aurez pris le temps d'inclure les mackages à utiliser, et tout ce que vous voulez dans l'en-tête) :

<pre>
pandoc -s --latex-engine=xelatex --template template.latex --toc --number-section -colorlinks masource.md -o sortie.pdf
</pre>

En somme, il peut être particulièrement utile de vous préparer à l'avance quelques modèles (en partant des modèles par défaut de pandoc), même sans forcément y intégrer toutes les options. Selon les types de documents que vous produisez, vous pourrez les générer très vite.

Pour ce qui concerne les sorties destinées à un format comme ODT ou Docx, le mieux est encore de préparer un modèle qui écrasera les styles du fichier de sortie.

## Conclusion

Même sans connaître toutes les possibilités offertes par Pandoc, vous voici désormais en mesure de produire un document censé être travaillé sous différents formats. Ceci est particulièrement utile si vous êtes censé récupérer des documents en provenance de différentes sources pour les mettre en page et les diffuser&nbsp;: il est plus facile de demander à vos contributeurs de rédiger en Markdown voire de rédiger un texte sans style quitte à ce que vous les repassiez vous-même en Markdown. De cette manière il vous sera ensuite très facile de publier différentes versions de ces textes adaptées aux mode de diffusions : pour le web, une version HTML, pour l'impression, une version PDF (LaTeX), etc.

Bien entendu, travailler à un bas niveau de mise en page, comme le veut l'usage du Markdown, nécessite de faire des choix, en particulier savoir à l'avance vers quel(s) format(s) vous voulez convertir vos documents. Si vous êtes amené à intégrer des éléments de HTML ou de LaTeX dans vos documents Markdown, faites-le en connaissance de cause.

En revanche les options de Pandoc étant nombreuses, et une fois affinées avec des fichiers de style, vos documents seront non seulement rapides à produire mais ils bénéficieront de multiples manières de les compiler pour des résultats différents.

De manière générale, le Markdown permet d'écrire très vite des contenus avec un minimum de commandes. À certains aspects, c'est sans doute le seul format qui permet d'écrire au kilomètre, avec un minimum d'informations de mise en page, ce qui permet de se concentrer exclusivement sur le contenu. Pandoc et l'art de créer des styles feront le reste.
