
---
title: "Je n'ai pas voté non plus"
date: 2015-12-09
author: "Christophe Masutti"
image: "/images/postimages/typewriter.png"
description: "Signe des temps, il n'y a pas de corrélation entre la démocratie et le droit de vote."
tags: ["Libres propos", "Politique", "Libertés", "Démocratie"]
categories:
- Libres propos
---

Bizarre comme titre, n'est-ce pas ? En fait, c'est en écho à [cet article du copain Gee](http://grisebouille.net/le-deuil-de-la-democratie-representative/), auteur de Grisebouille, qui a su exprimer de manière assez claire les raisons pour lesquelles il ne vote pas (plus). La démocratie représentative serait-elle morte&nbsp;? Oui. Nous avons besoin d'un reboot de la démocratie et c'est chez Jean-Jacques Rousseau qu'il faut en trouver l'amorce.



## Détournements de votes

Comme disait Coluche : «&nbsp;dire qu'il suffirait que les gens n'en n'achètent plus pour que cela ne se vende pas&nbsp;». Je n'ai jamais trop aimé Coluche &ndash;&nbsp;ou plutôt son humour de bistrot dont se sont emparés la plupart des réacs aigris&nbsp;&ndash;, mais au moins il avait le sens de la formule. Il suffirait que plus personne n'aille voter pour que la classe politique actuelle s'en aille pour de bon. Imaginons un instant une grève du vote&nbsp;: tous les vieux dinosaures des partis qu'on voit depuis 40 ans verraient soudainement leur légitimité en prendre un coup. Quant aux plus jeunes, persuadés de faire partie d'une «&nbsp;élite&nbsp;» républicaine (autrement dit, une aristocratie politique), ils pourraient retourner sur les bancs d'école, histoire de bien comprendre qu'on ne s'implique pas dans la politique par carriérisme. Oui, mais voilà&nbsp;: de fait, nous sommes des millions à ne pas aller voter. Et plutôt que de se remettre en cause, les politiques multiplient les interventions en se targuant de vouloir «&nbsp;barrer la route au FN&nbsp;» tout en assurant, comme c'est le cas à chaque élection depuis que je suis né, que «&nbsp;le message a été entendu&nbsp;».

Mais quel message&nbsp;? Pourquoi les gens s'abstiennent de voter, et pourquoi certains votent pour un parti fasciste&nbsp;? Parce que contrairement à ce qu'on m'a toujours dit, le vote n'est pas un choix démocratique. C'est un levier qui légitimise l'exercice du pouvoir par certains. Ainsi, depuis que je vote, je n'ai jamais pu dire que mon vote a servi des causes que je défends&nbsp;:


- quand il m'a fallu voter Chirac pour «&nbsp;barrer la route à Le Pen&nbsp;», en 2002&nbsp;;
- quand j'ai voté «&nbsp;non&nbsp;» au référendum de 2005 sur le traité européen Rome II (parce que c'est d'une autre Europe dont je rêve), remplacé 4 ans plus tard par son clone, le Traité de Lisbonne, cette fois sans demander l'avis aux citoyens&nbsp;;
- quand j'ai voté blanc en 2007 devant les choix consternants qui nous étaient livrés, entre une grande nunuche sans programme et un petit nerveux aux idées brunes&nbsp;;
- quand j'ai voté blanc en 2012 en ayant absolument aucune illusion sur les mensonges de F. Hollande.


## La démocratie représentative a vécu

Quel est mon constat, au-delà de la simple déception&nbsp;? le même que Gee&nbsp;: **la démocratie représentative a vécu**. Et je coupe court tout de suite aux détracteurs qui viendront me dire que «&nbsp;des gens sont morts pour que nous ayons le droit de voter&nbsp;». Non&nbsp;: ils ne sont pas morts pour le droit de vote, ils sont morts pour avoir le droit de participer à la vie politique. Ils ne sont pas morts pour que j'aie le droit de désigner un représentant qui, fondé de pouvoir, fera ce qu'il veut pendant son mandat. Ils sont morts pour que la démocratie s'exerce.

Oui, ma bonne dame, et c'est pas pareil du tout. Et pourtant on nous l'apprend depuis tout petit. Lorsqu'à l'école on n'est pas content des décisions du délégué de classe, on s'empresse de nous dire de fermer notre gueule. C'est vraiment cela, la démocratie&nbsp;? Tiens, voyons ce qu'en disait ce bon vieux Jean-Jacques Rousseau, qu'il serait peut-être temps de relire parce que au pays des Lumières, certains n'ont pas le courant à tous les étages&nbsp;:

> L'attiédissement de l'amour de la patrie, l'activité de l'intérêt privé, l'immensité des états, les conquêtes, l'abus du gouvernement, ont fait imaginer la voie des députés ou représentants du peuple dans les assemblées de la nation. C'est ce qu'en certain pays on ose appeler le tiers état. Ainsi l'intérêt particulier de deux ordres est mis au premier et second rang; l'intérêt public n'est qu'au troisième.
> 
> La souveraineté ne peut être représentée, par la même raison qu'elle peut être aliénée; elle consiste essentiellement dans la volonté générale, et la volonté ne se représente point : elle est la même, ou elle est autre; il n'y a point de milieu. Les députés du peuple ne sont donc ni ne peuvent être ses représentants, ils ne sont que ses commissaires; ils ne peuvent rien conclure définitivement. Toute loi que le peuple en personne n'a pas ratifiée est nulle; ce n'est point une loi. Le peuple Anglais pense être libre, il se trompe fort; il ne l'est que durant l'élection des membres du parlement: sitôt qu'ils sont élus, il est esclave, il n'est rien. Dans les courts moments de sa liberté, l'usage qu'il en fait mérite bien qu'il la perde. [...]
> 
> Quoi qu'il en soit, à l'instant qu'un peuple se donne des représentants, il n'est plus libre; il n'est plus.


&mdash;&nbsp;*Du contrat social ou Principes du droit politique* (1762), Chapitre&nbsp;3.15 &ndash;&nbsp;Des députés ou représentants

<h2>Un peu d'explication de texte</h2>

Au premier paragraphe Rousseau montre que le principe de représentativité est d'abord une affaire de l'Ancien Régime et son administration. Ce qu'on désigne par «&nbsp;tiers état&nbsp;», ce sont les députés, provinciaux pour la plupart, censés être représentatifs des classes sociales (bougeois, commerçants, artisans, etc.) mais qui valident le modèle hiérarchique des trois ordres (noblesse, clergé, bourgeoisie). Lors de la Révolution française, qui interviendra 27 ans après l'ouvrage de ce visionnaire de Rousseau, le principe de souveraineté du peuple sera remis en cause par Sieyès qui, justement, désignera le tiers état comme un élément de la constitution. Sieyès reprendra l'ancien ordre hiérarchique pour montrer que le tiers état, comme les deux autres ordres, doit être représenté de manière proportionelle à sa population. C'était plutôt bien vu, mais en réalité, la représentatitvité de ce tiers état est tellement diversifiée (cela va du paysan au riche industriel) que c'est la haute bourgeoisie, riche et cultivée, qui finira par s'octroyer cette représentativité, pour s'assurer exactement ce que dénonce Rousseau, à savoir la sauvegarde de leurs intérêts privés, face à une noblesse déclinante et une masse «&nbsp;prolétarienne&nbsp;»[^1] qu'il faut absolument écarter du pouvoir. Rousseau note bien, sous l'Ancien Régime, que c'est l'intérêt public (comprendre&nbsp;: du peuple) qui est mis au troisième rang parce que justement les représentants du peuple ne sont là que pour limiter les intérêts des deux autres ordres, ainsi confortés dans le pouvoir. La Révolution ne changera finalement la donne qu'au regard des intérêts d'une bourgeoisie de plus en plus riche face à la noblesse déclinante, c'est tout.

Rousseau est catégorique, sans appel. Dans le second paragraphe, il répond même à Montesquieu qui, dans l'*Esprit des Lois* (Livre XI, 1748) se montrait admiratif de la Constitution Anglaise et écrivait pour sa part&nbsp;:

> Comme, dans un état libre, tout homme qui est censé avoir une âme libre doit être gouverné par lui-même, il faudrait que le peuple en corps e&ucirc;t la puissance législative. Mais comme cela est impossible dans les grands états, et est sujet à beaucoup d'inconvénients dans les petits, il faut que le peuple fasse par ses représentants tout ce qu'il ne peut faire par lui-même. (…) Le grand avantage des représentants, c'est qu'ils sont capables de discuter les affaires. Le peuple n'y est point du tout propre; ce qui forme un des grands inconvénients de la démocratie.


Mais ce qui distingue notre Rousseau, c'est justement la conception des rôles. La conclusion de Rousseau est terrible, je la redonne ici, rien que pour le plaisir&nbsp;:


> (…) Quoi qu'il en soit, à l'instant qu'un peuple se donne des représentants, il n'est plus libre; il n'est plus.

La démocratie ne s'exerce pas tant parce qu'il est possible de rendre la représentativité plus ou moins légitime (de toute façon une impasse pour Rousseau), mais parce qu'il est possible d'y voir s'exercer la **volonté générale** et, en tant que telle, elle ne peut se déléguer. Donc oui, le peuple (la société civile, dirait-on aujourd'hui) est capable de gouverner, c'est à dire exercer et instruire la volonté générale, au lieu de laisser faire des représentants qui aliènent cette volonté durant leurs mandats.

Ces mots de Rousseau il y a plus de 250&nbsp;ans, je les ai entendu lors même de la série de renoncements aux engagements du clan de F. Hollande promulgués lors des élections. Pendant toute la durée du mandat, il allait faloir supporter, impuissants, les féodalités financières dont le point d'orgue fut atteint en insultant le peuple grec qui avait eu le culot, en juillet 2015, de réfuser *par référendum* les pires tentatives de réformes technocratiques pour remédier à un endettement historiquement organisé contre lui. Pour la plupart des électeurs, le fait d'aller voter n'était plus conçu comme l'exercice de la démocratie mais comme le seul moment de liberté avant de s'enchaîner aux grilles infranchissables qu'une classe politique dresse entre elle et le peuple.

Oui, n'en déplaise à ce pédant de Montesquieu qui pensait que le peuple est trop con pour faire de la politique, et tous les coincés du bocal pour qui la politique serait un métier, la société civile est capable à la fois d'expertise, d'expérience et de gouvernement. C'est la raison pour laquelle, sans doute, on a abruti les foules en étouffant leur activisme civique au profit du vote conçu comme une fin en soi, l'acte unique censé être la preuve d'une démocratie vivante. Rien n'est plus faux. Ce n'est pas parce qu'un peuple organise des élections qu'il est sur le chemin de la démocratie. Certains sont présidents à vie, suivez mon regard y compris dans certains fiefs électoraux de notre France.

## D'autres modèles existent déjà

Si l'on regarde honnêtement l'histoire de France, en en particulier de la Cinquième République, les exemples sont nombreux o&ugrave; les moyens de formation du peuple à la démocratie ont été étouffés dans l'oeuf. C'est par exemple toute la tragédie de l'éducation populaire, sujet sur lequel je vous laisse la lecture de l'article de Franck Lepage dans *Le Monde Diplomatique*, [De l'éducation populaire à la domestication par la «&nbsp;culture&nbsp;»](http://www.monde-diplomatique.fr/2009/05/LEPAGE/17113).

La société n'est pas avare de modèles dits «&nbsp;alternatifs&nbsp;», en réalité, des expériences bien souvent probantes de modèles d'organisation politique qui démontrent les limites de la représentation. Tous ces modèles ont ceci de commun qu'ils remettent explicitement en cause le fonctionnement actuel, et sont bien plus exigeants en matière de probité et d'équité. Démocratie directe, démocratie participative, auto-gestion des entreprises ou des collectifs, partage de connaissances, collaborations techniques, j'en passe et des meilleures. Dans le milieu associatif o&ugrave; j'évolue à mes heures pas perdues pour tout le monde, les solutions collaboratives ne manquent pas non plus. Prendre des décisions stratégiques à plusieurs milliers, y compris sous la forme de référendums permanents, tout cela est rendu possible dans le concept de [démocratie liquide](https://fr.wikipedia.org/wiki/D%C3%A9mocratie_liquide), y compris des bases logicielles (plate-formes web, la plupart du temps). La société peut aujourd'hui être conçue comme un gigantesque processeur, une machine à décision ultra rapide, capable de rassembler toutes les voix, en particulier sur des bases de technologies de communication ouvertes. On se réfèrera sur ce point à [cet article de Dominik Schiener](https://medium.com/@DomSchiener/liquid-democracy-true-democracy-for-the-21st-century-7c66f5e53b6f) (trad. fr. [sur le Framablog](http://framablog.org/2015/12/09/democratie-liquide/)).

Évidemment, l'aristocratie électoraliste n'est pas prête à accepter une remise en cause radicale de ses prérogatives. Les exemples, parfois tragiques, des [Zones à Défendre](https://fr.wikipedia.org/wiki/Zone_%C3%A0_d%C3%A9fendre) et du militantisme écologique, sont plus qu'éloquents, à tel point que, profitant des libéralités d'un état d'urgence anti-terrorisme, on en vient à [faire taire les voix trop bruyantes](https://wiki.laquadrature.net/%C3%89tat_urgence/Recensement) en plein [sommet mondial pour le Climat](https://fr.wikipedia.org/wiki/Conf%C3%A9rence_de_Paris_de_2015_sur_le_climat).

Mais bien plus que les fausses excuses du terrorisme, ce qui caractérise la classe politique, c'est sa crainte de voir le peuple se débarrasser d'elle. Si la démocratie représentative est un modèle qui ne fonctionne plus, le pouvoir n'a plus qu'une seule échapatoire&nbsp;: la violence d'état, qui oppose la classe politique et le peuple (qui se défini alors en opposition à l'aristocratie). Si l'on considère que la représentativité est la seule légitimation de l'ordre public, alors la seule réponse à toute volonté de changement de système, c'est d'imposer un vieux modèle contre un nouveau. C'est selon moi tout le principe des lois scélérates votées récemment (comme la [Loi relative au Renseignement](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement)&hellip;) et dont on voit [s'accélérer les tendances](http://www.numerama.com/politique/133795-wi-fi-ouvert-interdit-tor-bloque-les-nouvelles-idees-de-la-police.html), tout spécialement à l'encontre des moyens de communication qui permettent aux citoyens de se faire entendre, d'organiser et planifier l'alternance démocratique.

Après ces dénis de démocratie, les leçons sur le droit de vote sont plus que jamais malvenues. Alors, oui, voilà pourquoi je n'irai pas voter non plus dimanche prochain, et pourquoi je m'emploierai à ma mesure, à l'avènement de solutions vraiment démocratiques.



[^1]: Oui, ici je fais un anachronisme en utilisant ce terme, mais on comprend bien que la paysannerie était exclue de fait, alors même qu'elle représentait alors la grande majorité du peuple. Plus tard, avec la révolution industrielle, ce furent justement les prolétaires qui furent exclus, la voix du peuple étant largement confisquée par les classes bourgeoises capitalistes. Et oui, là, j'ai une lecture marxiste, mais elle en vaut bien une autre.


