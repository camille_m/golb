---
title: "Champ du feu - les cuisses qui piquent"
date: 2017-06-18
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
description: "Un très jolie sortie sur les hauteurs de Barr et du Hohwald"
tags: ["Sport", "VTT", "Entraînement", "Parcours"]
categories:
- Sport
---



Une belle sortie d'entraînement à la montée en VTT vous attend au départ de Barr (67140) jusqu'au Champ du feu (1099&nbsp;m).
Voici le rapide descriptif et la carte correspondante.

Au départ de Barr, vous pouvez garer votre véhicule sur le parking de l'église protestante rue du Kirschberg. Le départ du parcours commence  directmeent par la montée le long du cimetière en direction du chateau du Landsberg par le GR (rectangles rouges). On se dirige ensuite vers le carrefour de la Bloss (6&nbsp;km) via le kiosque Jadelot pour totaliser 491&nbsp;m&nbsp;D+. On amorce aussitôt une descente roulante jusque la Holzplatz (8,7&nbsp;km).

Après avoir traversé la D854, on entamme alors une longue montée (rectangles rouges et blancs) très exigeante (400&nbsp;m&nbsp;D+) avec des passages techniques jusqu'à Welschbruch (13,6&nbsp;km). De là, on poursuit l'ascension jusqu'au Champ du Feu (22,19&nbsp;km) en passant par la Rothlach (282&nbsp;m&nbsp;D+).

Il est alors temps de prendre le chemin du retour. Celui-ci commence par une grande descente jusqu'au Hohwald (28&nbsp;km) en passant par la cascade, puis un petit bout de route avant de longer cette dernière (en l'Andlau) jusqu'à Lilsbach. Pour rejoindre la vallée de Barr, il faut alors remonter jusqu'à Hungerplatz (130&nbsp;m&nbsp;D+) pour redescendre via le château d'Andlau (croix rouges) jusqu'à l'entrée de Barr.

Le dénivelé total du parcours est de 1360&nbsp;m&nbsp;D+ (les dénivelés cités ci-dessus ne sont pas exhaustifs) pour une distance de 42&nbsp;km.

<iframe width="100%" height="300px" frameBorder="0" src="https://framacarte.org/fr/map/circuit-barr-champ-du-feu_10951?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe>

<a href="https://framacarte.org/fr/map/circuit-barr-champ-du-feu_10951">Voir en plein écran</a>


