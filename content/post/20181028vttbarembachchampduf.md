---
title: "Sortie VTT : Barembach - Champ du Feu - Serva"
date: 2018-10-28
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
description: "Un parcours VTT du côté de Schirmeck-Barembach"
tags: ["Sport", "VTT", "Entraînement", "Parcours"]
categories:
- Sport
---

Voici une petite sortie VTT automnale du côté de la Vallée de la Bruche qui vous permettra de faire une sortie courte mais assez sportive.</p>

Pour faciliter l'expérience, vous pouvez télécharger le [topo-guide](/images/topo-guide_barembach-champduf.pdf) que je mets à votre disposition. Il ne remplace pas le GPX que vous pourrez récupérer via la carte ci-dessous. La distance est courte (35 km), le dénivelé est appréciable (1250 m D+) et les terrains sont de difficultés variées (deux points de prudence néanmoins sont signalés dans le topo).

En bref, voici le descriptif du parcours :


- Départ de *Barembach*, se garer près de l'église.
- Monter jusque *Mullerplatz* en suivant le balisage (profitez du sentier qui « coupe » le grand chemin forestier).
- Continuer vers le *Champ du Messin*, puis suivre le GR5.
- À la *Vieille Métairie*, prendre le chemin sur la droite près de la fontaine. Monter au *Champ du feu*.
- Amorcer la descente jusque la route, contourner le Centre de vacances « Les Sapins », pour reprendre le chemin.
- Un grand pré (*Le Haut des Monts*): aller tout droit pour pouvoir amorcer une grande descente jusqu'au Col de la Perheux. Prudence car le terrain n'est pas facile au début de la descente.
- Au *Col de la Perheux*, prendre les cercles jaunes en direction de la *Serva*. Une fois sur un sentier étroit, attention aux lacets avec fort dévers. Descendre vers *Neuviler-la-Roche*, et au croisement de la D130, prendre les triangles rouges.
- Quitter les triangles rouges pour monter la route forestière (sans balisage particulier) jusqu'aux *Roches Blanches*. Puis prendre le sentier (cercles rouges) jusque la *Croix Walter*.
- Descendre jusque *Barembach*.


<iframe src="https://framacarte.org/fr/map/mullerplatzchampdufperheuxserva_33922?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=false&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=true&amp;searchControl=null&amp;tilelayersControl=null&amp;embedControl=null&amp;datalayersControl=true&amp;onLoadPanel=undefined&amp;captionBar=false" width="100%" height="300px"></iframe>

<p><a href="https://framacarte.org/fr/map/mullerplatzchampdufperheuxserva_33922">Voir en plein écran</a></p>


