---
title: "Sentiers et randonnée : le grisbi du Club Vosgien"
date: 2020-01-05
author: "Christophe Masutti"
image: "/images/postimages/vttsentierCM-cc-by-sa.jpg"
description: "En octobre 2019, après un bon repas dans un restaurant montagnard, la Direction du Parc Naturel Régional des Ballons des Vosges et le Club Vosgien ont signé une charte de protection des sentiers pédestres. Ils entendent ainsi définir l'exclusivité des usages et tentent de légitimer une discrimination entre les piétons et les autres usagers, en particulier les VTT. Si cette charte n'a qu'une valeur juridique potentielle, elle n'en demeure pas moins une démarche qui pourra essaimer dans les autres parcs naturels, et c'est toute une activité sportive (et touristique) qui en subira les conséquences. Il faut se mobiliser."
tags: ["Sport", "VTT", "Communs", "Libertés"]
categories:
- Sport
- Libres propos
---


En octobre 2019, après un bon repas dans un restaurant montagnard, la Direction du Parc Naturel Régional des Ballons des Vosges et le Club Vosgien ont signé une charte de protection des sentiers pédestres. Ils entendent ainsi définir l'exclusivité des usages et tentent de légitimer une discrimination entre les piétons et les autres usagers, en particulier les VTT. Si cette charte n'a qu'une valeur juridique potentielle, elle n'en demeure pas moins une démarche qui pourra essaimer dans les autres parcs naturels, et c'est toute une activité sportive (et touristique) qui en subira les conséquences. Il faut se mobiliser.


# Vététiste

Loin des clichés et des vidéos impressionnantes de DownHill qu'on trouve sur les zinternetz, le VTT est d'abord et avant tout une activité à la fois sportive et populaire. Cela va de la petite promenade dominicale en famille dans les sous-bois, à la grande randonnée de 60&nbsp;kilomètres avec 2500&nbsp;m de dénivelé. Cette forme de randonnée sportive a un autre nom, le Cross Country ou All Mountain&hellip; On va appeler cela la Randonnée Sportive en VTT, pour faire court et francophone.

Il s'agit d'une pratique en solo ou en petits groupes. On croisera rarement un peloton de 20 vététistes dans la forêt sauf en cas d'organisation d'un événement dédié. On peut dire aussi que les vététistes sont des amoureux de la nature. Lorsque vous croisez un vététiste sur un sommet, par exemple dans les Vosges, il y a de fortes chances qu'il soit monté depuis le fond de la vallée (pour mieux y redescendre) et il n'a donc pas amené sa voiture sur le parking à 200&nbsp;m de là. Dans son sac à dos, il préfère embarquer quelques outils et non pas la combinaison chips et papiers gras que d'aucuns n'hésitent pas à laisser traîner : il préfère une petite barre de céréales et quelques fruits secs. C'est aussi un contemplatif, il aime les paysages et reste très sensible aux aspects esthétiques du circuit qu'il accompli.

Bref, il ressemble à quoi notre vététiste&nbsp;? à un randonneur. Un pur, un vrai, exactement comme les randonneurs chevronnés du Club Vosgien, cette vénérable institution qui regroupe des mordus de la marche et du bivouac. Sauf que lui, il a juste un vélo en plus.

Cette pratique du VTT rassemble l'écrasante majorité des pratiquants. Ils descendent sur les sentiers ou les chemins plus larges sans se prendre pour des champions du monde (parce que se casser la figure en VTT peut coûter cher). Sourire aux lèvres, sensations dans le guidon, quelques menus frissons&nbsp;: le VTT est une activité cool (avec un peu de transpiration quand même, sinon c'est pas drôle). Et pour les mordus comme moi, quelques degrés vers zéro ou un peu de pluie ne nous font peur (même la neige c'est marrant).

Dans les Vosges, on croise les vététistes soit sur les sentiers soit à la terrasse des fermes-auberges. Quoi de plus normal&nbsp;? Des allemands, des belges, des habitants du coin ou d'un peu plus loin&hellip; exactement comme les randonneurs pédestres vous dis-je.

# Alors c'est quoi le problème ?

J'ai déjà eu l'occasion de pondre [un assez long billet](https://golb.statium.link/post/20181205vttclubvosgien/) sur cette curieuse mentalité de quelques antennes (alsaciennes) du Club Vosgien qui refusent le partage des usages tout en s'octroyant le droit de décider des libertés (en particulier celle d'aller et venir) pour l'ensemble des usagers, y compris les randonneurs qui ne font pas partie du Club Vosgien (une majorité, donc).

Il faut croire que les mêmes ont su réitérer de manière encore plus officielle et cette fois en entraînant dans leur sillage la plus haute responsabilité du Club Vosgien, sa présidence, et en sachant convaincre le président du Parc Naturel Régional des Ballons des Vosges.

À l'initiative de l'antenne CV de Saint Amarin, une charte a été signée le 11 octobre 2019, avec pour titre&nbsp;: «&nbsp;[Charte de protection des sentiers pédestres](http://mbf-france.fr/wp-content/uploads/2019/10/Charte_protection_signee-CV-PNRBV-Oct19.pdf)&nbsp;». Elle concerne tout le massif et engage réciproquement le Parc le Club Vosgien&hellip; Je n'en fais pas l'article, l'antenne de la MBF Vosges a écrit à ce sujet un [communiqué plus que pertinent](http://mbf-france.fr/actionnationaleinter/convention-parc-naturel-regional-ballons-des-vosges-et-federation-du-club-vosgien/).

Cette charte a été rédigée, discutée et signée en totale exclusion des représentants des autres usagers de la montagne et de ses sentiers, en particulier les vététistes. On remarque parmi les présents à la «&nbsp;cérémonie&nbsp;» de la signature&nbsp;: des représentants d'antennes du Club Vosgien (surtout celui de Saint Amarin), des représentants du Parc, le préfet du Haut Rhin (surprenant&nbsp;!), un représentant du Club Alpin Français, un représentant d'Alsace Nature (association de protection de la nature), quelques élus municipaux, départementaux et régionaux&hellip; C'est-à-dire, outre le Club Vosgien, une très faible représentation des usagers de la montagne et du Parc mais une très forte représentation des instances administratives.

{{< figure src="/images/signaturecharteCVPNRBV.png" title="Source : page FB du club Vosgien antenne St. Amarin" >}}

Cette charte engage réciproquement le Club Vosgien et le Parc sur des sujets tels l'entretien des sentiers et leurs préservation, en vertu de leurs valeurs patrimoniale, esthétique, et économique. Fort bien, dirions-nous, puisque dans ce cas, les devoirs ne concernent que ces instances.

Mais que lit-on  au détour  d'un article&nbsp;? que les «&nbsp;sentiers fragiles de montagnes seront réservés en exclusivité à la circulation pédestre&nbsp;».

On ne parle pas ici des sentiers déjà interdits par arrêté (parfois même aux piétons) et qui concernent de très petites parties classée en zone protégée. Non&nbsp;: on parle des sentiers que visiblement le Club Vosgien défini péremptoirement comme des sentiers «&nbsp;fragiles&nbsp;», c'est-à-dire «&nbsp;inférieurs à 1&nbsp;mètre de large&nbsp;», «&nbsp;sinueux, situés dans des pentes raides&nbsp;» et «&nbsp;ne permettent pas le croisement avec d'autres usagers que pédestres&nbsp;» (comme si les VTT ne s'arrêtaient jamais pour laisser passer les marcheurs&hellip;)

En d'autres termes, dans une charte qui ne fait que stipuler des engagements réciproques entre le Parc et le Club Vosgien, c'est-à-dire *entre une association et une autorité administrative*, on voit apparaître comme par enchantement une clause qui rend exclusif et discriminatoire l'usage des sentiers. Faute d'avancer des faits réels et tangibles pour justifier cette exclusivité, la clause en question n'a pas la valeur d'un arrêté mais elle est néanmoins légitimée par une autorité administrative. Tel était l'objectif du Club Vosgien, et il a été atteint.

Il faut encore préciser que la valeur patrimoniale de ces sentiers est essentiellement le fruit du travail du Club Vosgien depuis le début du siècle dernier. On ne peut que saluer l'énorme travail fourni mais on ne peut toutefois s'empêcher de remarquer que le CV se place aujourd'hui en juge et partie&nbsp;: la même valeur patrimoniale est-elle communément partagée&nbsp;? Certains préfèrent peut-être davantage insister sur les aspects environnementaux, sportifs, culturels&hellip;

Le seul argument éventuellement vérifiable qui soit avancé par le Club Vosgien consiste à accuser les VTT de favoriser l'érosion des sentiers. Tout comme on pourrait pointer l'élargissement des sentiers par les randonneurs qui marchent de front. J'ai de mon côté la certitude que le VTT n'est certainement pas un facteur significatif d'érosion. Et si tant est qu'une étude sérieuse porterait sur l'érosion par les usagers du Parc il faudrait alors s'attendre à devoir questionner&nbsp;:

- l'intérêt réel pour le Club Vosgien de baliser près de 20.000 Km de sentiers (sans aucune érosion bien entendu),
- les pratiques des exploitants et la gestion forestière en général (une calamité dans le massif vosgien),
- la gestion du couvert végétal et de la faune,
- questionner le schéma de circulation des véhicules à moteur,
- la pratique du ski,
- etc.

Et il faudrait alors aussi questionner les raisons pour lesquelles, par exemple, une association comme la MBF se voit souvent refuser les autorisations de participer à l'entretien des sentiers. Car de ce côté aussi une certaine exclusivité du Club Vosgien est organisée (puisque valider la participation des vététistes reviendrait à valider aussi leur usage).

Bref, qu'on se le dise : *certains* membres de *certaines* antennes du Club Vosgien ont une dent contre les vététistes. Et profitant de leurs appuis et de leurs relations, ils s'arrangent pour officialiser les choses.

# Conséquences

Il est clair que de telles dispositions dans la charte ont de fait une valeur juridique potentielle. Cela signifie que désormais il sera possible de se référer à cette charte comme un élément justifiant un arrêté futur visant carrément à interdire la pratique du VTT.

Mais cette disposition change aussi la manière de concevoir l'espace *commun*&nbsp;: sous prétexte d'entretenir les sentiers balisés, et donc d'exercer le monopole des itinéraires de randonnée, le Club Vosgien s'approprie cet espace commun, ses valeurs patrimoniales et esthétiques, et s'arroge le droit de choisir qui peut en jouir et comment.

Comme le [dit Ludovic](https://www.vtt-alsace.fr/viewtopic.php?p=139603&sid=4dae79582ab92a911e957a5abf491a61#p139603), membre de la MBF :

> Le problème dans les documents signés par le PNRBV et la Fédération du Club Vosgien, c'est qu'on inverse totalement la réalité des usages et la loi. Légalement, la règle c’est la libre circulation partout et les limitations par voie d’exception (avec des motifs réels et sérieux). Ici l’exception est partout.

Et quitte à l'interdire, toujours selon cette charte, cela risquerait de concerner énormément d'endroits dans le massif des Vosges&nbsp;:

- «&nbsp;Sentiers fragiles&nbsp;»&nbsp;: concept très vagues et donc pouvant être élargi toujours davantage (montées, descentes&hellip;),
- chemins de moins d'1&nbsp;mètre de large,
- «&nbsp;Sentiers pittoresques à haute valeur esthétique en sommet de crête, chaumes, forêts d'altitudes&nbsp;».

Bref, autant dire que si on veut passer d'une vallée à l'autre ou atteindre un sommet, cela ne sera plus possible en VTT (sauf éventuellement par la route bitumée et les cols des départementales).

Mais le plus grave dans tout cela, c'est que si de telles dispositions sont prises dans le Parc Naturel Régional des Ballons des Vosges, on peut s'inquiéter légitimement pour ce qu'il en sera dans les autres Parcs Naturels Régionaux&nbsp;! Car en effet, on sait combien les «&nbsp;expérimentations&nbsp;» ont tendance à devenir très vite des règles universelles.

# Et pourtant

La solution pour plus de sérénité dans le massif réside autant dans le partage des usages que dans le partage des responsabilités. Les mains n'ont cessé de se tendre vers le Club Vosgien.

Les vététistes sont toujours prêts à accepter que dans certains cas, sur des zones particulières et rares, des sentiers dédiés peuvent être imaginés, voire même des itinéraires de délestage, comme des contournements, mais laissant aux uns comme aux autres la liberté de circuler. Des propositions peuvent être élaborées, il reste à les faire valoir, y compris pour leurs intérêts touristiques.

Toute forme d'interdiction portant sur un usage global et dont les conditions seraient définies par un seul acteur, aussi engagé soit-il et exerçant un monopole, reviendrait à l'échec. Non seulement parce que le Club Vosgien ne représente pas tous les usagers, mais aussi parce qu'il serait impossible de faire respecter de tels interdits à moins de placer un agent du Parc Régional chaque jour à chaque coin de la forêt. Il ne pourrait en résulter autre chose que des tensions inutiles entre pratiquants. Au contraire, proposer de manière ouverte et démocratique des solutions alternatives serait profitable à deux points de vue&nbsp;:

- éviter les clivages là où le respect des usages et de l'environnement est d'abord un combat à mener *en commun*,
- améliorer les flux de circulation et enrichir les itinéraires selon les pratiques (randonneurs, VTT, VTT électriques, niveaux de difficulté, etc.).

Pour cela, il faut associer systématiquement tous les usagers aux décisions, ce qui est est une démarche démocratique et égalitaire. Associer les vététistes reviendrait aussi à leur confier les responsabilités qu'ils réclament de leur côté, y compris pour entretenir les sentiers.

Pour éviter que les seules réponses aux difficultés ne soient que l'exclusion et l'ignorance, il est important pour les pratiquants de VTT se rassemblent pour former un front commun de discussion. Seule la MBF et la Fédération Française de Cyclisme (dont le silence est parfois assourdissant) peuvent être des interlocuteurs valables dans un monde où peinent à être reconnus les collectifs informels. Alors, si ce n'est déjà fait, [adhérez à la MBF](http://mbf-france.fr/adherer/)&nbsp;!

![](/images/mbfvosges.png)
