---
title: "Occupations"
date: 2012-01-01
lastmod: 2019-08-01
author: "Christophe Masutti"
tags: ["Occupations"]
description: "Quelques éléments biographiques"
categories:
- Libres propos
---



{{< figure src="/images/Christophe_Masutti-By-Y_Kervran-CC-By_1000x679px.jpg" title="Christophe Masutti, Juin 2019. Crédits: Y. Kervran. CC-By." >}}


Voici quelques éléments biographiques à propos de Christophe Masutti. Ils ne sont ni exhaustifs ni définitifs.


|  Loisirs et occupations  |                                                        |
|---------------------------|-----------------------------------------------|
|  **Résumé :** (H)ac(k)tiviste, libriste, administrateur de [Framasoft](https://framasoft.org). Sport à haute dose (VTT, natation, trail). Histoire, sociologie et philosophie des sciences.    |  ![](/images/Mesoccupations.png)   |


Trucs officiels&hellip;


| Rôle                    |   Instit.                                |
|--------------------------|----------------------------------------|
| **Attaché aux affaires Européennes et transfrontalières**, [Hôpitaux Universitaires de Strasbourg](http://www.chru-strasbourg.fr/), dep. fév. 2008.   —  *Coopération, coordination de projets, recherche clinique, évaluation, gestion scientifique et technique, développement de l'activité* | ![](/images/Logo_HUS.png) | 
| **Co-président, membre administrateur**, [Association Framasoft](https://framasoft.org/), Dep. janv. 2010   — *Membre du comité de direction, direction éditoriale de Framabook, coordinateur de projets, administration de l'association (RH, stratégie)*  | ![](/images/Framasoft_Logo.png) |
| **Chercheur associé**, [SAGE, UMR 7363](https://sage.unistra.fr/), Dep. sept. 2012   — (Sociétés, acteurs, Gouvernement en Europe) | ![](/images/logosage.png) |
| **Post-doctorat**, [INSERM](https://www.inserm.fr/), Institut national de la santé et de la recherche médicale, janv. 2008 — janv. 2009  — Histoire et politiques de santé, France / Allemagne | ![](/images/logoinserm.png) |
| **Post-doctorat**, [La Charité Universitätsmedizin Berlin](https://www.charite.de), févr. 2007 – janv. 2008  — Développement réseau DRUGS (histoire de l'industrie pharmaceutique) | ![](/images/Logo_Charite.png) |
| **Attaché d'enseignement et de recherches**, [Faculté des sciences économiques](https://ecogestion.unistra.fr/), Université de Strasbourg, 2004 — 2006.   — Histoire des sciences et des technologies, Histoire économique | ![](/images/logounistra.png) |
| **Doctorat** (allocataire-moniteur de recherches), [IRIST](http://irist.unistra.fr/), Université de Strasbourg, 2001 — 2004.   — Histoire des sciences et des technologies (72), Sciences politiques (04)  | ![](/images/logounistra.png) |
| **DEA**, [IRIST](http://irist.unistra.fr/), Université de Strasbourg, 2000 — 2001.   — Sciences, Technologies et Société (STS) | ![](/images/logounistra.png) |
| **Maîtrise/master**, [Université de Strasbourg](http://www.unistra.fr), 1999.   — Discipline : Philosophie | ![](/images/logounistra.png) |

**Une petite citation pour donner le ton**&nbsp;:

> Le défaut de jugement est proprement ce que l’on nomme stupidité et c’est là un vice auquel il n’y a pas de remède. Une tête obtuse ou bornée à laquelle il ne manque que le degré d’entendement convenable et des concepts qui lui soient propres, est susceptible de beaucoup d’instruction et même d’érudition. Mais, comme le jugement manque aussi ordinairement, en pareil cas, il n’est pas rare de rencontrer des hommes fort instruits, qui laissent fréquemment éclater, dans l’usage qu’ils font de leur science, cet irréparable défaut.
> -- <cite>(E. Kant, *CRP -- Analytique Transc.*, note)</cite>


