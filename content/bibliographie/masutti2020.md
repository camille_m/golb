---
title: "Affaires Privées"
subtitle: "Aux sources du capitalisme de surveillance"
date: 2020-03-20
image: "/images/biblio/christophe-masutti.jpg"
author: "Christophe Masutti"
description: "Masutti, Christophe. Affaires Privées. Aux sources du capitalisme de surveillance. C&F éditions, 2020."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Masutti"]
---

Au quotidien, nos échanges numériques et nos comportements de consommateurs sont enregistrés, mesurés, calculés afin de construire des profils qui s'achètent et se vendent. Des débuts de la cybernétique aux big data, la surveillance a constitué un levier économique autant qu'idéologique.

Dans *Affaires privées*, Christophe Masutti retrace l'histoire technique et culturelle de soixante années de controverses, de discours, de réalisations ou d'échecs. Ce retour aux sources offre un éclairage passionnant sur le capitalisme de surveillance et sur le rôle joué par le marketing dans l'informatisation de la société.Il décrit la part prise par les révolutions informatiques et le marché des données dans les transformations sociales et politiques.

La surveillance est utilisée par les administrations à des fins de contrôle, et par les entreprises pour renforcer leurs capacités commerciales. Si les pratiques de renseignement des États ont souvent été dénoncées, la surveillance venue du monde des affaires n'a longtemps suscité qu'indifférence. Le business des données en a profité pour bousculer les cadres juridiques et réglementaires de la vie privée.

Comment développer une économie numérique qui respecterait la vie privée des individus ? Comment permettre à la vie privée d'échapper au pouvoir des affaires ? Christophe Masutti propose une réflexion historique et politique sur les conditions d'émancipation face à l'économie de la surveillance.

----

Masutti, Christophe. *Affaires Privées. Aux sources du capitalisme de surveillance*. C&F éditions, 2020.





**Lien vers le site de l'éditeur :** https://cfeditions.com/masutti/

----
