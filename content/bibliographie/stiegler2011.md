---
title: "Réseaux sociaux"
subtitle: "Culture politique et ingénierie des réseaux sociaux"
date: 2011-01-21
image: "/images/biblio/Bernard-Stiegler-reseaux-sociaux.jpg"
author: "Christophe Masutti"
description: "Stiegler, Bernard, éditeur. Réseaux sociaux. Culture politique et ingénierie des réseaux sociaux. FYP éditions, 2011."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Stiegler"]
---

La dissémination des technologies numériques dans toutes les couches sociales de tous les pays industrialisés transforme inexorablement les relations entre les individus, les groupes, les générations et les nations. La croissance spectaculaire des réseaux sociaux affecte tous les milieux, et vient transformer les règles du jeu socio-économique dans son ensemble, tant pour les individus que pour les entreprises et organisations, et dans tous les domaines de la vie. Or – en première analyse – ces nouveaux réseaux peuvent sembler des réseaux non sociaux, voire même antisociaux. Ils sont en effet généralement coupés de ce qui caractérisait jusqu’alors le social : lié à un territoire, à une langue, à un héritage (religieux, politique ou culturel au sens le plus large), légué par des générations d’ascendants, et qui précède en principe le social comme son passé, comme un sol commun.

Cet ouvrage, dirigé par Bernard Stiegler, propose les meilleures contributions aux Entretiens du Nouveau Monde Industriel sur les réseaux sociaux. Il montre comment ces technologies relationnelles bouleversent non seulement les règles traditionnelles de l’économie et de l’industrie, mais également, et plus profondément, le processus d’individuation psychique et collective. Il propose une analyse approfondie des conditions sociologiques et psychologiques qui président à la constitution de ces réseaux sociaux.

Il étudie leurs conséquences économiques et organisationnelles, et identifie les opportunités d’innovation sociale, les enjeux politiques et les menaces afférents à cette émergence du « social engineering ».

Enfin, il explore les règles de constitution et de développement des réseaux sociaux du web 3.0 (alliance du web sémantique et du web social), et également les conditions économiques et éthiques d’administration de ces nouveaux milieux, c’est-à-dire les questions de la gestion, du contrôle, de la transparence et de l’e-démocratie, ainsi que les technologies et les stratégies industrielles déjà mises en oeuvre ou à venir.

Avec les contributions de : Bernard Stiegler, Alexander R. Galloway, Yann Moulier-Boutang, Annie Gentès, François Huguet, Christian Fauré, Richard Harper, Antoine Masson, Elizabeth Rossé, Kieron O’Hara, Aristea M. Zafeiropoulou, David E. Millard et Craig Webber, Alain Mille, Olivier Auber

----

Stiegler, Bernard, éditeur. Réseaux sociaux. Culture politique et ingénierie des réseaux sociaux. FYP éditions, 2011.




**Lien vers le site de l'éditeur :**  https://www.fypeditions.com/bernard-stiegler-et-al-reseaux-sociaux/


----
