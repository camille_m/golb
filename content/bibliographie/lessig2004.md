---
title: "Culture libre"
subtitle: "Comment les médias utilisent la technologie et la loi pour vérouiller la culture et contrôler la créativité"
date: 2004-02-01
image: "/images/biblio/Lawrence-Lessig.jpg"
author: "Christophe Masutti"
description: "Lessig, Lawrence. Culture libre. Comment les médias utilisent la technologie et la loi pour vérouiller la culture et contrôler la créativité. Freeculture.cc, 2004"
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Lessig"]
---

Dans la préface de *Culture Libre*, Lessig compare ce livre avec un de ses précédents, Code et autres lois du cyberespace, qui avançait alors l'idée que le logiciel fait effet de loi. Le message de Culture Libre est différent car « son sujet n’est pas Internet en soi. Le sujet en est plutôt ses effets sur l’une de nos traditions, ce qui est bien plus fondamental et plus difficile à comprendre, que ce soit pour les passionnés d’informatique et de technologies que pour les autres, car bien plus importants. »

Lawrence Lessig analyse la tension qui existe entre les concepts de piratage et de propriété intellectuelle, dans ce contexte qu'il appelle « le système législatif désespérément corrompu », et qui évolue dans chaque pays grâce à des entreprises plus intéressées par l'accumulation d'un capital que par le libre échange des idées.

Le livre rend aussi compte du procès qui opposa Éric Eldred (éditeur de livre appartenant au domaine public) à John Ashcroft, qui allongea la durée du copyright de certaines œuvres de 20 ans, et la tentative de Lessig de développer à ce moment-là une loi Eldred, aussi connue sous le nom de Loi d'amélioration du domaine public (*Public Domain Enhancement Act* ou *Copyright Deregulation Act*).

Lessig conclut son livre en écrivant que maintenant que notre société évolue en une société de l'information, il faut décider si la nature de celle-ci doit être libre ou féodale. Dans la postface, il suggère que le pionnier du logiciel libre Richard Stallman et le modèle de la Free Software Foundation de créer des contenus disponibles ne sont pas dirigés contre le capitalisme (qui permet à des entreprises comme LexisNexis de faire payer les utilisateurs pour des contenus étant principalement dans le domaine public), mais qu'il faut proposer des licences telles que celles créées par son organisation Creative Commons.

Il argumente également en faveur de la création d'une période plus courte pour le renouvellement du copyright et une limitation de ses dérives, telles que le fait de stopper, pour un éditeur, la publication de l'ouvrage d'un auteur sur Internet pour un but non commercial, ou de créer un régime de licence obligatoire pour assurer aux créateurs d'obtenir des royalties sur leurs œuvres directement en fonction de leurs usages.

(Source : [Wikipédia](https://fr.wikipedia.org/wiki/Culture_libre_(livre))) 

----

Lessig, Lawrence. *Culture libre. Comment les médias utilisent la technologie et la loi pour vérouiller la culture et contrôler la créativité*. Freeculture.cc, 2004.


**Lien vers le site de l'éditeur :** http://www.free-culture.cc/

**Téléchargement :** https://www.ebooksgratuits.com/details.php?book=2198

----
