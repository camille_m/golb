---
title: "Histoires et cultures du Libre"
subtitle: "Des logiciels partagés aux licences échangées"
date: 2013-08-03
image: "/images/biblio/Christophe-Masutti-Camille-Paloque.jpg"
author: "Christophe Masutti"
description: "Paloque-Berges, Camille, et Christophe Masutti, (éds.). Histoires et cultures du Libre. Des logiciels partagés aux licences échangées. Lyon, Framasoft, 2013."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Masutti", "Paloque-Berges"]
---

Fruit de la collaboration inédite d’auteurs provenant d’horizons disciplinaires différents, par des approches thématiques et des études de cas, cet ouvrage propose une histoire culturelle du Libre non seulement à travers l’histoire de l’informatique, mais aussi par les représentations sociales, philosophiques, juridiques et économiques qu’a cristallisé le mouvement du logiciel libre jusqu’à nos jours.

À l’aide de multiples clés d’analyse, et sans conception partisane, ce livre dresse un tableau des bouleversements des connaissances et des techniques que ce mouvement a engendré. Le lecteur saura trouver dans cette approche ambitieuse et prospective autant d’outils pour mieux comprendre les enjeux de l’informatique, des réseaux, des libertés numériques, ainsi que l’impact de leurs trajectoires politiques dans la société d’aujourd’hui.


----

Paloque-Berges, Camille, et Christophe Masutti, (éds.). *Histoires et cultures du Libre. Des logiciels partagés aux licences échangées*. Lyon, Framasoft, 2013.



**Lien vers le site de l'éditeur :** https://framabook.org/histoiresetculturesdulibre/

----
