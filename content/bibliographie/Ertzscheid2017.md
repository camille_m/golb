---
title: "L’appétit des géants"
subtitle: "Pouvoir des algorithmes, ambitions des plateformes"
date: 2017-04-20
image: "/images/biblio/Olivier-ertzscheid.jpg"
author: "Christophe Masutti"
description: "Ertzscheid, Olivier. L’appétit des géants: pouvoir des algorithmes, ambitions des plateformes. C&F éditions, 2017."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Ertzscheid"]
---

Il fallait un amoureux du web et des médias sociaux pour décrypter les enjeux culturels, relationnels et démocratiques de nos usages numériques. Olivier Ertzscheid met en lumière les effets d'échelle, l'émergence de géants aux appétits insatiables. En concentrant toutes nos activités numériques sur quelques plateformes, nous avons fait naître des acteurs mondiaux qui s'épanouissent sans contrôle. Nos échanges, nos relations, notre sociabilité vont nourrir des algorithmes pour classer, organiser et finalement décider pour nous de ce qu'il nous faut voir.

Quelle loyauté attendre des algorithmes qui se nourrissent de nos traces pour mieux alimenter l'influence publicitaire ou politique ? Comment construire des médias sociaux et un accès indépendant à l'information qui ne seraient pas soumis aux ambitions des grands acteurs économiques du web ? Pourquoi n'y a-t-il pas de bouton « sauver le monde » ? 

----

Ertzscheid, Olivier. L’appétit des géants: pouvoir des algorithmes, ambitions des plateformes. C&F éditions, 2017.


**Lien vers le site de l'éditeur :** https://cfeditions.com/geants/

----
