---
title: "Numérique : reprendre le contrôle"
date: 2016-08-02
image: "/images/biblio/Tristan-Nitot-Nina-cercy.jpg"
author: "Christophe Masutti"
description: "Nitot, Tristan, et Nina Cercy. Numérique : reprendre le contrôle. Lyon, Framasoft, 2016."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Nitot", "Cercy"]
---

L’extraction et l’analyse des données massives représente l’Eldorado des modèles économiques d’Internet. Pour une grande partie, ces données proviennent de chacun d’entre nous. Profilage, surveillance, contrôle : en utilisant une foule de services et de produits, gratuits ou payants, nous dévoilons toujours un peu plus nos intimités numériques.

Aujourd’hui, pouvons-nous encore contrôler nos données ? Est-ce à l’État ou aux citoyens de construire et défendre leurs souverainetés numériques ? Le logiciel libre permet-il de nous rendre autonomes et d’utiliser des outils respectueux de nos données personnelles ? Les réponses à ces questions sont parfois contradictoires.

Dans cet ouvrage, Tristan NITOT et Nina CERCY donnent la parole à plusieurs acteurs impliqués aujourd’hui dans le paysage numérique. Leurs arguments sont forts, issus de plusieurs points de vue. Une préoccupation principale : la confiance.

----

Nitot, Tristan, et Nina Cercy. *Numérique : reprendre le contrôle*. Lyon, Framasoft, 2016.



**Lien vers le site de l'éditeur :** https://framabook.org/numerique-reprendre-le-controle/


----
