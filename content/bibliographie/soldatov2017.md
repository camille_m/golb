---
title: "The Red Web"
subtitle: "The Struggle Between Russia's Digital Dictators and the New Online Revolutionaries "
date: 2017-04-21
image: "/images/biblio/andrei-soldatov.jpg"
author: "Christophe Masutti"
description: "Soldatov, Andrei, et Irina Borogan. The Red Web. The Struggle Between Russia's Digital Dictators and the New Online Revolutionaries. Public Affairs, 2017."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Soldatov", "Borogan"]
---

After the Moscow protests in 2011-2012, Vladimir Putin became terrified of the internet as a dangerous means for political mobilization and uncensored public debate. Only four years later, the Kremlin used that same platform to disrupt the 2016 presidential election in the United States. How did this transformation happen?

The Red Web is a groundbreaking history of the Kremlin’s massive online-surveillance state that exposes just how easily the internet can become the means for repression, control, and geopolitical warfare. In this bold, updated edition, Andrei Soldatov and Irina Borogan offer a perspective from Moscow with new and previously unreported details of the 2016 hacking operation, telling the story of how Russia came to embrace the disruptive potential of the web and interfere with democracy around the world.

----

Soldatov, Andrei, et Irina Borogan. The Red Web. The Struggle Between Russia's Digital Dictators and the New Online Revolutionaries. Public Affairs, 2017.


**Lien vers le site de l'éditeur :** https://www.publicaffairsbooks.com/titles/andrei-soldatov/the-red-web/9781610395748/

----
