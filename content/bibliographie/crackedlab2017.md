---
title: "Corporate Surveillance in Everyday Life"
subtitle: " How Companies Collect, Combine, Analyze, Trade, and Use Personal Data on Billions"
date: 2017-06-12
image: "/images/biblio/Wolfie-Christl.jpg"
author: "Christophe Masutti"
description: "Christl, Wolfie, et CrackedLab. Corporate Surveillance in Everyday Life. How Companies Collect, Combine, Analyze, Trade, and Use Personal Data on Billions. Cracked Labs, 2017."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Christl", "CrackedLab"]
---

How thousands of companies monitor, analyze, and influence the lives of billions. Who are the main players in today’s digital tracking? What can they infer from our purchases, phone calls, web searches, and Facebook likes? How do online platforms, tech companies, and data brokers collect, trade, and make use of personal data?

In recent years, a wide range of companies has started to monitor, track and follow people in virtually every aspect of their lives. The behaviors, movements, social relationships, interests, weaknesses and most private moments of billions are now constantly recorded, evaluated and analyzed in real-time. The exploitation of personal information has become a multi-billion industry. Yet only the tip of the iceberg of today’s pervasive digital tracking is visible; much of it occurs in the background and remains opaque to most of us.

This report by Cracked Labs examines the actual practices and inner workings of this personal data industry. Based on years of research and a previous 2016 report, the investigation shines light on the hidden data flows between companies. It maps the structure and scope of today’s digital tracking and profiling ecosystems and explores relevant technologies, platforms and devices, as well as key recent developments.

----

Christl, Wolfie, et CrackedLab. *Corporate Surveillance in Everyday Life. How Companies Collect, Combine, Analyze, Trade, and Use Personal Data on Billions*. Cracked Labs, 2017.

**Lien vers le site :** http://crackedlabs.org/en/corporate-surveillance.


----
