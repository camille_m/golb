---
title: "L’éthique des hackers"
date: 2013-02-01
image: "/images/biblio/Steven-Levy.jpg"
author: "Christophe Masutti"
description: "Levy, Steven. L’éthique des hackers, Globe, 2013."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Levy"]
---

Qui aurait cru qu’une poignée de hackers binoclards seraient à l’origine de la plus grande révolution du XX<sup>e</sup> siècle ? Le livre culte de Steven Levy, histoire vraie de l’équipe de geeks qui ont changé le monde.

Précision : un « hacker » n’est pas un vulgaire « pirate informatique ». Un hacker est un « bricoleur de code ». Son truc : plonger dans les entrailles de la machine.

Bill Gates, Steve Jobs, Steve Wozniak, Mark Zuckerberg ont commencé leurs brillantes carrières comme hackers…
La plupart ne paient pas de mine mais tous partagent une même philosophie, une idée simple et élégante comme la logique qui gouverne l’informatique : l’ouverture, le partage, le refus de l’autorité et la nécessité d’agir par soi-même, quoi qu’il en coûte, pour changer le monde.

C’est ce que Steven Levy appelle l’Éthique des hackers, une morale qui ne s’est pas exprimée dans un pesant manifeste, mais que les hackers authentiques ont mise en pratique dans leur vie quotidienne. Ce sont eux qui ont œuvré, dans l’obscurité, à la mise en marche de la révolution informatique.

Depuis les laboratoires d’intelligence artificielle du MIT dans les années 1950 jusqu’aux gamers des années 1980, en passant par toutes les chambres de bonne où de jeunes surdoués ont consacré leurs nuits blanches à l’informatique, Steven Levy les a presque tous rencontrés. Voici leur histoire.

----

Levy, Steven. *L’éthique des hackers*, Globe, 2013.


**Lien vers le site de l'éditeur :** https://www.editions-globe.com/lethique-des-hackers/


----
